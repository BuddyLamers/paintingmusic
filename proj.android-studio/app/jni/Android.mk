LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)
$(call import-add-path,$(LOCAL_PATH))

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame


LOCAL_SRC_FILES := hellocpp/main.cpp \
../../../Classes/AppDelegate.cpp \
../../../Classes/SplashScreen.cpp \
../../../Classes/Home.cpp \
../../../Classes/CustomMenuItemImage.cpp \
../../../Classes/Modes.cpp \
../../../Classes/MenuScreen.cpp \
../../../Classes/TouchableLayer.cpp \
../../../Classes/LoadPaintingScene.cpp \
../../../Classes/UILoadSong.cpp \
../../../Classes/LoadSong.cpp \
../../../Classes/SelectKeybord.cpp \
../../../Classes/UIPianoColor.cpp \
../../../Classes/UIGameScene.cpp \
../../../Classes/UIKeyboardLayer.cpp \
../../../Classes/ColorPanel.cpp \
../../../Classes/GameData.cpp \
../../../Classes/SoundController.cpp \
../../../Classes/PianoSong.cpp \
../../../Classes/UISavePainting.cpp \
../../../Classes/SavePainting.cpp \
../../../Classes/SaveSong.cpp \
../../../Classes/UIStore.cpp \
../../../Classes/LoadMusic.cpp \
../../../Classes/MusicNodes.cpp \
../../../Classes/Store.cpp \
../../../Classes/GameScene.cpp \
../../../Classes/UISetting.cpp \
../../../Classes/UIRecordingPanel.cpp \
../../../Classes/UIRecordSong.cpp \
../../../Classes/IOSongController.cpp \
../../../Classes/Achievement.cpp

LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_LDLIBS := -landroid \
-llog
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END

LOCAL_WHOLE_STATIC_LIBRARIES += PluginAdMob
LOCAL_WHOLE_STATIC_LIBRARIES += sdkbox
LOCAL_WHOLE_STATIC_LIBRARIES += PluginIAP
LOCAL_WHOLE_STATIC_LIBRARIES += PluginSdkboxPlay

LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module, ./sdkbox)
$(call import-module, ./pluginadmob)
$(call import-module, ./pluginiap)
$(call import-module, ./pluginsdkboxplay)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
