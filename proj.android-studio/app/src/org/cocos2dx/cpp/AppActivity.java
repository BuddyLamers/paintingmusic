/****************************************************************************
Copyright (c) 2015-2017 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import org.cocos2dx.lib.Cocos2dxActivity;
import java.io.File;
import java.io.IOException;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.net.rtp.AudioStream;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AppActivity extends Cocos2dxActivity {
    static MediaRecorder recorder;
    static  AudioStream as;
    static MediaPlayer mediaPlayer;
    static  boolean isRecordingStart = false;
    static  String audioPath = "";

    static int pauseRecordingLength = 0;

   static File audiofile = null;
    static final String TAG = "MediaRecording";
    Button startButton,stopButton;

    public static   AppActivity   currentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        startButton = (Button) findViewById(R.id.button1);
//        stopButton = (Button) findViewById(R.id.button2);
        currentActivity             =       this;
        mediaPlayer = new MediaPlayer();
    }

    static  public  void onStartRecording() throws IOException {
//        startButton.setEnabled(false);
//        stopButton.setEnabled(true);
        //Creating file
        File dir = Environment.getExternalStorageDirectory();
        try {
            audiofile = File.createTempFile("sound", ".ogg", dir);
        } catch (IOException e) {
            Log.e(TAG, "external storage access error");
            return;
        }
        //Creating MediaRecorder and specifying audio source, output format, encoder & output format
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(audiofile.getAbsolutePath());
        recorder.prepare();
        recorder.start();


        System.out.println("path rocording2 "+audiofile.getAbsolutePath());

        isRecordingStart = true;
    }




    static public String  onStopRecording(final String fileName) {
        System.out.println("onStopRecording call");
        currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                System.out.println("save rocording 0");
                recorder.stop();
                System.out.println("save rocording 1.0");
                recorder.release();
                currentActivity.onSaveRecording(fileName);

            }
        });




        File dir = Environment.getExternalStorageDirectory();



        System.out.println("android directory path "+dir.getAbsolutePath());
        return dir.getAbsolutePath();

    }

    static  public void onSaveRecording(String fileName){
        currentActivity.stopRecording(fileName);
    }

    void stopRecording(String fileName){
        //stopping recorder

//        as.
        //after stopping the recorder, create the sound file and add it to media library.
        addRecordingToMediaLibrary(fileName);
    }

    void addRecordingToMediaLibrary(String fileName) {
        //creating content values of size 4



        File dir = Environment.getExternalStorageDirectory();



        File savePath = new File(dir,fileName+".ogg");

        String path = dir.getAbsolutePath()+"/"+fileName+".ogg";
        System.out.println("save rocording root path "+path);

        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
//        values.put(MediaStore.Audio.Media.TITLE, "audio" + audiofile.getName());
        values.put(MediaStore.Audio.Media.TITLE, fileName);
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
//        values.put(MediaStore.Audio.Media.DATE_ADDED, fileName);
        values.put(MediaStore.Audio.Media.DISPLAY_NAME, fileName);
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/ogg");
        values.put(MediaStore.Audio.Media.DATA, audiofile.getAbsolutePath());
//        System.out.println("save rocording2 "+audiofile.getAbsolutePath());






        //creating content resolver and storing it in the external content uri
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);
        System.out.println("save rocording 3");
        //sending broadcast message to scan the media file so that it can be available
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
//        Toast.makeText(this, "Recording has saved successfully", Toast.LENGTH_LONG).show();
        System.out.println("save rocording 4");

        audiofile.renameTo(savePath);


        saveaudioSuccessfully(fileName);
















    }


    public static void onPlayRecording(String path){

//        String file = "/storage/emulated/0/"+sound+".ogg";
        System.out.println("play audio path "+path);
        try {

                mediaPlayer.stop();
                mediaPlayer.reset();

            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        }catch (IOException e){
            System.out.println("file not found exp"+e.getMessage());
        }
    }


    public static void onPauseRecording(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            pauseRecordingLength = mediaPlayer.getCurrentPosition();
        }
    }

    public static void onResumeRecording(){
        mediaPlayer.seekTo(pauseRecordingLength);
        mediaPlayer.start();
    }



    public static void playSound(String sound){
        MediaPlayer mediaPlayer = new MediaPlayer();
        String file = "/storage/emulated/0/"+sound+".ogg";
        try {
            mediaPlayer.setDataSource(audioPath);
            mediaPlayer.prepare();
            mediaPlayer.start();
        }catch (IOException e){
            System.out.println("file not found exp");
        }
    }

    public static int isRecordingAudio(){

        if(isRecordingStart){
            return  1;
        }else{
            return 0;
        }
    }



    public static native void saveaudioSuccessfully(String fileName);


}
