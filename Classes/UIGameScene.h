

#ifndef PROJ_ANDROID_STUDIO_UIGAMESCENE_H
#define PROJ_ANDROID_STUDIO_UIGAMESCENE_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "UIKeyboardLayer.h"
#include "UIPianoColor.h"
#include "MenuScreen.h"
#include "CustomMenuItemImage.h"
USING_NS_CC;
class UIGameScene : public cocos2d::Layer{




public:

    Size winSize = Director::getInstance()->getWinSize();


    int currentTileCount = 0;
    LayerColor *topLayer;
    Label *title;
    CustomMenuItemImage *btnMenu;
    CustomMenuItemImage *btnRecord;
    CustomMenuItemImage *btnPlay;
    CustomMenuItemImage *btnStop;

    ScrollView *scrollView;


    //    UIPianoColor *pianoColor;
    Vector<UIPianoColor*> vecpianoColor;
    UIKeyboardLayer *keyboardLayer;


    Label *lblTitle;
    Sprite *imgHeader;

    Sprite *imgColorPanel;




    UIGameScene();
    ~UIGameScene();
//    static Scene* createScene();
    virtual  bool init();
//    CREATE_FUNC(UIGameScene);

    void addFooterPanel();
    void addHeaderPanel();

    void addColorPanel();

    virtual void recordCall(cocos2d::Ref *sender) = 0;
    virtual void stopMusicCall(cocos2d::Ref *sender) = 0;
    virtual void playMusicCall(cocos2d::Ref * sender) = 0;
    virtual void saveCall() = 0;
    virtual void backCall() = 0;
    virtual void eraseCall() = 0;

    // auto scroll in tile animation
    virtual void autoMoveTile();


    virtual void valueChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt);
    void pianoKeyOnEnter();
    void pianoKeyOnExit();

};


#endif //PROJ_ANDROID_STUDIO_UIGAMESCENE_H
