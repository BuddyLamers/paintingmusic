
#include "UIGameScene.h"

#include "cocos-ext.h"
#include "PianoSong.h"
//using namespace cocos2d::ui;
using namespace cocos2d::extension;
using namespace std;
UIGameScene::UIGameScene() {

}

UIGameScene::~UIGameScene() {

}


// Scene* UIGameScene::createScene() {
//
//     Scene *scene = Scene::create();
//     auto layer = UIGameScene::create();
//     scene->addChild(layer);
//     return scene;
//
// }


bool UIGameScene::init() {

    if(!Layer::init()){
        return  false;
    }




    Sprite *imgBg   =   Sprite::create("game screen/BG.png");
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);

    addHeaderPanel();
    addFooterPanel();

    addColorPanel();
    keyboardLayer = UIKeyboardLayer::create();
    keyboardLayer->setPosition(Vec2(winSize.width*0.0,winSize.height-imgHeader->getContentSize().height -keyboardLayer->getContentSize().height));
    this->addChild(keyboardLayer);


    keyboardLayer->slider1->addTargetWithActionForControlEvents(this, cccontrol_selector(UIGameScene::valueChangedCallback), cocos2d::extension::Control::EventType::VALUE_CHANGED);

//    for (int i = 0; i < keyboardLayer->vecKeyButton.size(); i++) {
//
//        KeyButton *btnKey   =   keyboardLayer->vecKeyButton.at(i);
////        CallFunc * keyClick     =   CallFunc::create(UI)
//
//        btnKey->btnKey->setOnEnterCallback(CC_CALLBACK_0(UIGameScene::pianoKeyOnEnter,this));
//        btnKey->btnKey->setOnExitCallback(CC_CALLBACK_0(UIGameScene::pianoKeyOnExit,this));
//
//
//    }
   
   
    return  true;
}



void UIGameScene::addHeaderPanel() {
    LayerColor *layerHeader = LayerColor::create(Color4B::BLACK);
    this->addChild(layerHeader);
    imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height-layerHeader->getContentSize().height));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
    layerHeader->addChild(imgHeader);

    lblTitle = Label::createWithTTF("UNTITLED","fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);
//    lblTitle->setColor(Color3B::BLACK);

    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("game screen/menu bar.png","game screen/menu bar.png",CC_CALLBACK_0(UIGameScene::backCall,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.92,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);

}

void UIGameScene::addFooterPanel(){

    LayerColor *layerFooter = LayerColor::create(Color4B::BLUE);

    layerFooter->setContentSize(Size(winSize.width,winSize.height*0.1));
    layerFooter->setPosition(Vec2(winSize.width*0.0,winSize.height*0.0));
    this->addChild(layerFooter);

    Sprite *imgbtnBg = Sprite::create("game screen/lower button bg.png");
    imgbtnBg->setPosition(layerFooter->getContentSize().width/2,layerFooter->getContentSize().height/2);
    layerFooter->addChild(imgbtnBg);


    Sprite * imgMusicAnim   =   Sprite::create("game screen/music animation.png");
    imgMusicAnim->setPosition(Vec2(imgbtnBg->getContentSize().width*0.3,imgbtnBg->getContentSize().height/2));
    imgbtnBg->addChild(imgMusicAnim);

//    MenuItemImage *btnRecord = MenuItemImage::create("game screen/mic button.png","game screen/mic button.png",CC_CALLBACK_0(UIGameScene::recordCall,this));
//    btnRecord->setPosition(Vec2(layerFooter->getContentSize().width*0.15,layerFooter->getContentSize().height*0.5));

    btnPlay = CustomMenuItemImage::create("game screen/play button lower.png","game screen/play button lower.png",CC_CALLBACK_1(UIGameScene::playMusicCall,this));
    btnPlay->setPosition(Vec2(layerFooter->getContentSize().width*0.6,layerFooter->getContentSize().height*0.5));

    btnPlay->setTag(100);
    btnStop = CustomMenuItemImage::create("game screen/stop button lower.png","game screen/stop button lower press.png",CC_CALLBACK_1(UIGameScene::stopMusicCall,this));
    btnStop->setPosition(Vec2(layerFooter->getContentSize().width*0.75,layerFooter->getContentSize().height*0.5));
    btnPlay->setTag(101);
    btnRecord = CustomMenuItemImage::create("game screen/mike btn.png","game screen/mike btn press.png",CC_CALLBACK_1(UIGameScene::recordCall,this));
    btnRecord->setPosition(Vec2(layerFooter->getContentSize().width*0.9,layerFooter->getContentSize().height*0.5));
    btnPlay->setTag(102);



    Sprite *imgSliderBg = Sprite::create("game screen/lower button bg.png");
    imgSliderBg->setPosition(layerFooter->getContentSize().width/2,layerFooter->getContentSize().height-(imgSliderBg->getContentSize().width/2));
    layerFooter->addChild(imgSliderBg);



    auto Bg_sprite = Sprite::create("game screen/3 slide button lower.png");
    auto Prog_sprite = Sprite::create("game screen/3 slide button lower.png");
    auto Thumb_sprite = Sprite::create("game screen/3 slide button upper.png");

    Menu *menu = Menu::create();

    menu->addChild(btnStop);
    menu->addChild(btnPlay);
//    menu->addChild(btnRecord);
    menu->addChild(btnRecord);

    menu->setPosition(Vec2::ZERO);
    layerFooter->addChild(menu);

}

void UIGameScene::valueChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt)
{
//    log("valuse change");
//    ControlSlider *slider = static_cast<cocos2d::extension::ControlSlider*>(sender);
//    int tag = slider->getTag();
//
//
//    float value = static_cast<cocos2d::extension::ControlSlider*>(sender)->getValue();
//
//    float valuebetween = value/100;
//
//
//
//    float x1 =  slider->getBoundingBox().getMinX()+slider->getContentSize().width*valuebetween;
//
//
//
//    keyboardLayer->stencil->setPosition(Vec2(x1,keyboardLayer->stencil->getPosition().y));
//
//    float x2 = (winSize.width-keyboardLayer->scrollView->getContentSize().width+100)*valuebetween;
//
//    keyboardLayer->scrollView->setContentOffset(Vec2(x2,0),false);
}

void UIGameScene::recordCall(cocos2d::Ref *sender){

   // Director::getInstance()->replaceScene(UIGameScene::createScene());
}
void UIGameScene::stopMusicCall(cocos2d::Ref *sender){



}
// for background music
void UIGameScene::playMusicCall(cocos2d::Ref * sender){
//    log("pause call");
//    Vector<FiniteTimeAction*> vecAction;
//    for (int i = 0; i < vecpianoColor.size(); i++) {
//        UIPianoColor *pianoColor1 = vecpianoColor.at(i);
//        CallFunc *callFunc = CallFunc::create(CC_CALLBACK_0(UIPianoColor::startAnimation,pianoColor1));
//        DelayTime *delayTime    =   DelayTime::create(0.5);
//
//
//        CallFunc *callFunc1 =   CallFunc::create(CC_CALLBACK_0(UIGameScene::autoMoveTile,this));
//
//        vecAction.pushBack(callFunc);
//        vecAction.pushBack(callFunc1);
//        vecAction.pushBack(delayTime);
//
//    }
//
//    Sequence *sequence  =   Sequence::create(vecAction);
//    this->runAction(sequence);
}
void UIGameScene::saveCall(){

}


void UIGameScene::addColorPanel() {

    imgColorPanel = Sprite::create("game screen/color page.png");
    imgColorPanel->setTag(1001);
    imgColorPanel->setPosition(Vec2(winSize.width/2,winSize.height*0.37));
    this->addChild(imgColorPanel);


    LayerColor *scrollLayer  =   LayerColor::create(Color4B(0,0,0,0));
    scrollView  =   ScrollView::create(Size(imgColorPanel->getContentSize().width*0.94,imgColorPanel->getContentSize().height*0.94),scrollLayer);

    scrollView->setDirection(ScrollView::Direction::VERTICAL);
    scrollView->setPosition(Vec2(imgColorPanel->getContentSize().width*0.03,imgColorPanel->getContentSize().height*0.03));
    imgColorPanel->addChild(scrollView);

   scrollView->setContentSize(scrollView->getViewSize());

    scrollLayer->setTag(101);
    scrollView->setContentOffset(Vec2(0,-(scrollLayer->getContentSize().height-scrollView->getViewSize().height)),true);

//    int pWidth  =   5;
//    int pHeight =   scrollLayer->getContentSize().height;
//
//    log("inside loop color00");
//    PianoSong *pianoSong = new PianoSong();
//
//    Vector<__String * > vecMusicSeq;
//    __String      *strObj     =       StringMake(pianoSong->songNode);
//    __Array     *tokensArray    =   strObj->componentsSeparatedByString(",");
//    for(int i=0;i<tokensArray->count();i++){
//        __String		*singleMusic	=		(__String *)tokensArray->getObjectAtIndex(i);
//        vecMusicSeq.pushBack(singleMusic);
//    }
//
//
//
//
//
//    for (int i = 0; i < vecMusicSeq.size(); i++) {
//        log("inside loop color");
//        UIPianoColor *pianoColor    =   UIPianoColor::create();
//        pianoColor->setPosition(Vec2(pWidth,pHeight-pianoColor->getContentSize().height*1.05));
//        pWidth  +=  pianoColor->w*1.05;
//        if(pWidth>imgColorPanel->getContentSize().width*0.9){
//            pHeight-= pianoColor->h*1.05;
//            pWidth  =   5;
//        }
//
//
//        log("inside loop color2");
//
//
//
//
//
//        __String *strSound = vecMusicSeq.at(i);
//
//
//
//        if(strSound->_string == "a3"){
//            pianoColor->setColor(Color3B(3,14,127));
//        }else if(strSound->_string == "b3"){
//            pianoColor->setColor(Color3B(42,140,201));
//        }else if(strSound->_string == "c3"){
//            pianoColor->setColor(Color3B(26,204,189));
//        }else if(strSound->_string == "d3"){
//            pianoColor->setColor(Color3B(61,226,11));
//        }else if(strSound->_string == "e3"){
//            pianoColor->setColor(Color3B(226,134,13));
//        }else if(strSound->_string == "f3"){
//            pianoColor->setColor(Color3B(237,51,37));
//        }else if(strSound->_string == "g3"){
//            pianoColor->setColor(Color3B(3,14,127));
//        }else {
//            pianoColor->setColor(Color3B(0,0,0));
//        }
//
//
//
//
//        scrollLayer->addChild(pianoColor);
//        vecpianoColor.pushBack(pianoColor);
//    }
    log("inside loop color end");



//    scrollLayer->setContentSize(Size(imgColorPanel->getContentSize().width,pHeight));






}



void UIGameScene::autoMoveTile(){




    currentTileCount++;
}

void UIGameScene::pianoKeyOnEnter() {

    log("piano key on enter call");
}
void UIGameScene::pianoKeyOnExit() {
    log("piano key on exit call");
}


void UIGameScene::backCall() {

}