//

//

#ifndef PROJ_ANDROID_STUDIO_MENUSCREEN_H
#define PROJ_ANDROID_STUDIO_MENUSCREEN_H


#include "cocos2d.h"
#include "TouchableLayer.h"
class MenuScreen: public cocos2d::Layer
{
public:

    Size winSize = Director::getInstance()->getWinSize();
    static cocos2d::Scene* createScene();

    virtual bool init();

    // a selector callback
    //void menuCloseCallback(cocos2d::Ref* pSender);

    void fun1();
    void fun2();
    void fun3();
    void fun4();
    void fun5();
    void fun6();
    void fun7();
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode,cocos2d::Event*event);
    void back();
    TouchableLayer *layer1;
    void menuPopup();
    bool btnBack= false;

    // implement the "static create()" method manually
    CREATE_FUNC(MenuScreen);
};

#endif //PROJ_ANDROID_STUDIO_MENUSCREEN_H
