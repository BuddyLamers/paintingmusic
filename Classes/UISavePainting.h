//
// Created by assertinfotech on 21/11/17.
//

#ifndef PROJ_ANDROID_STUDIO_UISAVEPAINTING_H
#define PROJ_ANDROID_STUDIO_UISAVEPAINTING_H


#include "cocos2d.h"
#include "cocos-ext.h"
#include "CustomMenuItemImage.h"
#include "TouchableLayer.h"


  USING_NS_CC;
USING_NS_CC_EXT;

class UISavePainting : public  cocos2d::Layer{

public:




//    TouchableLayer *layerRewrite;
//    MenuItemImage *btnRewriteCancel;
//    MenuItemImage *btnRewriteYes;

    UISavePainting();
    ~UISavePainting();

    CustomMenuItemImage *btnSave;
    CustomMenuItemImage *btnBack;
    // *txtName;
//    ui::TextField *txtName;


    virtual  bool init();
    CREATE_FUNC(UISavePainting);

    virtual void initComponents();
    virtual void backCall();
    virtual void savePaintingCall();


//    void rewritePopup();
//    void rewriteCancelCall();
//    void rewriteYesCall();


};


#endif //PROJ_ANDROID_STUDIO_UISAVEPAINTING_H
