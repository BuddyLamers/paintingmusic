//
//

#include "UIRecordingPanel.h"
#include "GameData.h"
#include "LoadMusic.h"
//#include "SoundController.h"




UIRecordingPanel::UIRecordingPanel() {

}


UIRecordingPanel::~UIRecordingPanel() {

}




Scene* UIRecordingPanel::createScene() {
    Scene* scene = Scene::create();
    auto layer =  UIRecordingPanel::create();
    scene->addChild(layer);

    return  scene;
}
bool UIRecordingPanel::init() {
    if(!Layer::init()){
        return false;
    }

    isRecordingStart = false;
    initComponents();
    addHeaderPanel();
    return true;
}


void UIRecordingPanel::initComponents() {

    Sprite *imgBg = Sprite::create("recording/record/page 24_BG.png");
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);


    Sprite *imgWav = Sprite::create("recording/record/sound wav.png");

    imgWav->setPosition(Vec2(winSize.width/2,winSize.height*0.8));
    this->addChild(imgWav);


    CustomMenuItemImage *btn1 = CustomMenuItemImage::create("recording/record/play button.png","recording/record/play button.png",CC_CALLBACK_1(UIRecordingPanel::btnClick,this));

    btn1->setPosition(Vec2(winSize.width*0.35,winSize.height*0.45));
    btn1->setTag(100);
    btn2 = CustomMenuItemImage::create("recording/record/record button.png","recording/record/record button.png",CC_CALLBACK_1(UIRecordingPanel::btnClick,this));

    btn2->setPosition(Vec2(winSize.width*0.65,winSize.height*0.45));
    btn2->setTag(200);


    Label *lblpaly =  Label::createWithTTF("PLAY","fonts/arial.ttf",35);
    lblpaly->setPosition(Vec2(winSize.width*0.35,winSize.height*0.35));
    lblpaly->setColor(Color3B::BLACK);
    this->addChild(lblpaly);

    Label *lblRecord =  Label::createWithTTF("RECORD","fonts/arial.ttf",35);
    lblRecord->setPosition(Vec2(winSize.width*0.65,winSize.height*0.35));
    lblRecord->setColor(Color3B::BLACK);
    this->addChild(lblRecord);







    CustomMenuItemImage *btn3 = CustomMenuItemImage::create("recording/record/done button.png","recording/record/done button.png",CC_CALLBACK_1(UIRecordingPanel::btnClick,this));
    btn3->setPosition(Vec2(winSize.width*0.5,winSize.height*0.225));
    btn3->setTag(300);

    Label *lblDone =  Label::createWithTTF("DONE","fonts/arial.ttf",30);
    lblDone->setPosition(Vec2(btn3->getContentSize().width/2,btn3->getContentSize().height/2));
    btn3->addChild(lblDone);
    lblDone->enableOutline(Color4B::BLACK,1);

    Menu *menu = Menu::create();
    menu->addChild(btn1);
    menu->addChild(btn2);
    menu->addChild(btn3);

    menu->setPosition(Vec2::ZERO);
    this->addChild(menu);


}


void UIRecordingPanel::addHeaderPanel() {
    LayerColor *layerHeader = LayerColor::create(Color4B::BLACK);
    this->addChild(layerHeader);
    Sprite *imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height-layerHeader->getContentSize().height));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
    layerHeader->addChild(imgHeader);

    Label* lblTitle = Label::createWithTTF("RECORDING SONG","fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);
    lblTitle->enableOutline(Color4B::BLACK,2);
    lblTitle->setColor(Color3B::BLACK);

    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(UIRecordingPanel::backCall,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.1,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);


}


void UIRecordingPanel::backCall() {
    Director::getInstance()->popScene();
}

void UIRecordingPanel::btnClick(Ref *sender) {

    std::string path = CCFileUtils::sharedFileUtils()->getWritablePath();
    std::string path2 = CCFileUtils::sharedFileUtils()->getDefaultResourceRootPath();


    log("cocos search path %s",path2.c_str());

    log("cocos search path %s",path.c_str());
    int tag = ((CustomMenuItemImage*) sender)->getTag();
    if(tag==100){

//        GameData::playRecording();
    }else if(tag==200){
        isRecordingStart = true;
        btn2->setColor(Color3B::GRAY);
        btn2->setEnabled(false);
        GameData::startRecording();

//        GameData::stopRecording();
//        SoundController::resumeBackgroundMusic();
    }else if(tag==300){

        if(isRecordingStart) {

            savePopup();
//            GameData::stopRecording();
//            MessageBox("Recording has been saved", "");
//            isRecordingStart = false;
//
//
//            UserDefault::getInstance()->setIntegerForKey("KEY_RECORD_SONG",1);
//            Director::getInstance()->popScene();
        }
//        GameData::saveSong("","newSound");
//        SoundController::playSoundEffectByName("/storage/emulated/0/sound-1098662029.ogg");
//        ((MenuItemImage*)sender)->setNormalImage(Sprite::create("recording/stop button.png"));
//        ((MenuItemImage*)sender)->setSelectedImage(Sprite::create("recording/stop button.png"));
//        GameData::playRecording();
//        schedule(schedule_selector(UIRecordingPanel::update));


    }

}








/**
     * create mediaplayer for music
     * @param path the path relative to assets
     * @return
     */
//void  UIRecordingPanel::createMediaplayerFromAssets(){
//   String path;
//    MediaPlayer mediaPlayer = null;
//
//    try{
//        mediaPlayer = new MediaPlayer();
//        mediaPlayer.reset();
//
//        if(path.contains("/sdcard")) {
//            mediaPlayer.setDataSource(path);
//        }
//        else {
//            AssetFileDescriptor assetFileDescritor = mContext.getAssets().openFd(path);
//
//            mediaPlayer.setDataSource(assetFileDescritor.getFileDescriptor(),
//                                      assetFileDescritor.getStartOffset(), assetFileDescritor.getLength());
//        }
//
//        mediaPlayer.prepare();
//
//        mediaPlayer.setVolume(mLeftVolume, mRightVolume);
//    }catch (Exception e) {
//        mediaPlayer = null;
//        Log.e(TAG, "error: " + e.getMessage(), e);
//    }
//
//    return mediaPlayer;
//}










void UIRecordingPanel::savePopup() {




    TouchableLayer *layer = TouchableLayer::createLayer(Color4B(0,0,0,150));

    this->addChild(layer);
    Size winSize = Director::getInstance()->getWinSize();
    Sprite *imgBg   =   Sprite::create("save painting/BG frame.png");

    imgBg->setTag(100);
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    layer->addChild(imgBg);

    Sprite *imgTxt  =   Sprite::create("save painting/text BG.png");
    imgTxt->setTag(101);
    imgTxt->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.55));
    imgBg->addChild(imgTxt);

    Label *lblSongName =   Label::createWithTTF("Name of song","fonts/arial.ttf",30);
    lblSongName->setColor(Color3B::BLACK);
    lblSongName->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.7));
    imgBg->addChild(lblSongName);



    txtName =   ui::TextField::create("FIle name","fonts/arial.ttf",30);
    txtName->setPosition(Vec2(imgTxt->getContentSize().width/2,imgTxt->getContentSize().height*0.5));
    txtName->setTag(102);

    txtName->setColor(Color3B(10,20,10));
    txtName->setTextHorizontalAlignment(TextHAlignment::LEFT);
    txtName->setTextVerticalAlignment(TextVAlignment::CENTER);
    txtName->setTouchAreaEnabled(true);
    txtName->setTouchSize(Size(imgTxt->getContentSize().width, imgTxt->getContentSize().height));
    imgTxt->addChild(txtName);



    Label *lblTitle =   Label::createWithTTF("SAVE SONG","fonts/arial.ttf",40);
    lblTitle->setColor(Color3B::BLACK);
    lblTitle->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.9));
    imgBg->addChild(lblTitle);

    CustomMenuItemImage *btnBack =   CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(Node::removeFromParent,layer));
    btnBack->setPosition(Vec2(imgBg->getContentSize().width*0.1,imgBg->getContentSize().height*0.9));



    CustomMenuItemImage *btnSave =   CustomMenuItemImage::create("save painting/save.png","save painting/save.png",CC_CALLBACK_0(UIRecordingPanel::saveCall,this));
    btnSave->setPosition(Vec2(imgBg->getContentSize().width*0.5,imgBg->getContentSize().height*0.3));



    Menu *menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnSave);
    menu->addChild(btnBack);

    imgBg->addChild(menu);







}





void UIRecordingPanel::saveCall() {
    std::string fileName = txtName->getString();
    GameData::stopRecording(fileName);
    MessageBox("Recording has been saved", "");
    isRecordingStart = false;


    UserDefault::getInstance()->setIntegerForKey("KEY_RECORD_SONG",1);
    Director::getInstance()->popScene();

}


void UIRecordingPanel::update(float dt) {
    bool isRecording = GameData::isRecordingAudio();
    if(!isRecording){
        btn2->setNormalImage(Sprite::create("recording/record button.png"));
        btn2->setSelectedImage(Sprite::create("recording/record button.png"));

        unschedule(schedule_selector(UIRecordingPanel::update));
    }
}