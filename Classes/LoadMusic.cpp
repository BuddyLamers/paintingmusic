//
// Created by assertinfotech on 28/11/17.
//

#include "LoadMusic.h"

#include "UIStore.h"

//
//

#include "LoadMusic.h"
#include "GameData.h"
#include "Home.h"
#include "GameScene.h"
#include "SoundController.h"


LoadMusic::LoadMusic() {

}

LoadMusic::~LoadMusic() {

}


Scene* LoadMusic::createScene() {

    Scene *scene = Scene::create();
    auto layer = LoadMusic::create();
    scene->addChild(layer);
    return  scene;

}

bool LoadMusic::init() {
    if(!Layer::init()){
        return false;
    }
    createComponents();

    return  true;
}





void LoadMusic::createComponents() {



    Sprite *imgBg   =   Sprite::create("menu screen/main BG.png");
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);


    addCenterComponents();
    addHeaderPanel();


}


void LoadMusic::addHeaderPanel() {
    LayerColor *layerHeader = LayerColor::create(Color4B(0,0,0,0));
    this->addChild(layerHeader);
    Sprite *imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height*0.82));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
//    layerHeader->addChild(imgHeader);

    Label* lblTitle = Label::createWithTTF("LOAD SONG","fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);
    lblTitle->setColor(Color3B(80,0,0));

    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(LoadMusic::backCall,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.12,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);




}


void LoadMusic::addCenterComponents() {


    GameData::getInstance()->listAllSong.clear();
    Sprite *imgPage   =   Sprite::create("load song/BG frame.png");
    imgPage->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgPage);


    LayerColor *scrollLayer = LayerColor::create(Color4B(10,10,10,0));

    ScrollView *scrollView = ScrollView::create(Size(imgPage->getContentSize().width*0.88,imgPage->getContentSize().height*0.71),scrollLayer);
    imgPage->addChild(scrollView);
    scrollView->setPosition(Vec2(imgPage->getContentSize().width*0.075,imgPage->getContentSize().height*0.1));
    scrollView->setDirection(ScrollView::Direction::VERTICAL);


    int numOfSong = 0;
    log("num of song %d",numOfSong);
    int counter = 0;
    int n = 0;
    float topMargin = 0;

//    if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG || GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
//        numOfSong = 1;
       n+=5;
//    }


    vecLock.push_back("0");
    vecLock.push_back("0");
    vecLock.push_back("0");
    vecLock.push_back("1");
    vecLock.push_back("1");
    GameData::getInstance()->listAllSong.pushBack(StringMake("jingle bell"));
    GameData::getInstance()->listAllSong.pushBack(StringMake("Moon Light"));
    GameData::getInstance()->listAllSong.pushBack(StringMake("Twinkal twinkal"));
    GameData::getInstance()->listAllSong.pushBack(StringMake("Happy Birthday"));
    GameData::getInstance()->listAllSong.pushBack(StringMake("jingle bell"));
//    std::string arrSongName[5] = {"jingle bell","Moon Light","Twinkal twinkal","Happy Birthday","jingle bell"};


    if(GameData::getInstance()->game_type == GAME_TYPE_EXPERIMENTAL_PLAY){
        numOfSong = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
       int size = UserDefault::getInstance()->getIntegerForKey(KEY_NUM_OF_RECORDING,0);
        for (int i = 0; i < size; ++i) {
            std::string key = KEY_RECORDING_NAME;
            key = key+String::createWithFormat("%d",(i+1))->getCString();
            std::string name = UserDefault::getInstance()->getStringForKey(key.c_str());
            GameData::getInstance()->listAllSong.pushBack(StringMake(name));
            vecLock.push_back("0");
        }

        n += size;
    }


    for (int i = 0; i < n; i++) {
//        std::string key = KEY_RECORDING_NAME;
//        key = key+String::createWithFormat("%d",(i+1))->getCString();
//        std::string name = UserDefault::getInstance()->getStringForKey(key.c_str());
//        if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG|| GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
         std::string   name = GameData::getInstance()->listAllSong.at(i)->getCString();
//        }
        log("song name : %s",name.c_str());

        if(name!=""){
            LayerColor *layerBox = LayerColor::create(Color4B(255,255,255,0));
            Sprite *imgBottomLine = Sprite::create("menu screen/menu btn selected.png");
            layerBox->setContentSize(Size(scrollLayer->getContentSize().width,imgPage->getContentSize().height*0.12));



            vecSongName.pushBack(StringMake(name));
            Label *lblName = Label::createWithTTF(name,"fonts/arial.ttf",35,Size(imgPage->getContentSize().width*0.4,imgPage->getContentSize().height*0.15),TextHAlignment::LEFT,TextVAlignment::CENTER);
            lblName->setPosition(Vec2(layerBox->getContentSize().width*0.3,layerBox->getContentSize().height*0.65));
            lblName->setColor(Color3B(50,0,0));
            Label *lblSubTitle = Label::createWithTTF("1 songs","fonts/arial.ttf",25,Size(imgPage->getContentSize().width*0.4,imgPage->getContentSize().height*0.15),TextHAlignment::LEFT,TextVAlignment::CENTER);
            lblSubTitle->setPosition(Vec2(layerBox->getContentSize().width*0.3,layerBox->getContentSize().height*0.3));
            lblSubTitle->setColor(Color3B(50,0,0));
            CustomMenuItemImage *btnListen = CustomMenuItemImage::create("load song/button 02.png","load song/button 02.png",CC_CALLBACK_1(LoadMusic::listenSongCall,this));
            btnListen->setPosition(Vec2(layerBox->getContentSize().width*0.75,layerBox->getContentSize().height*0.72));
            btnListen->setTag(counter);
            Label *lblListen = Label::createWithTTF("LISTEN","fonts/arial.ttf",35);
            lblListen->setPosition(Vec2(btnListen->getContentSize().width*0.5,btnListen->getContentSize().height*0.5));
            btnListen->addChild(lblListen);
            lblListen->setTag(101);
            lblListen->setColor(Color3B(100,0,0));
            CustomMenuItemImage *btnLoad = CustomMenuItemImage::create("load song/button 03.png","load song/button 03.png",CC_CALLBACK_1(LoadMusic::loadSongCall,this));
            btnLoad->setPosition(Vec2(layerBox->getContentSize().width*0.75,layerBox->getContentSize().height*0.3));
            btnLoad->setTag(counter);
            Label *lbLoad = Label::createWithTTF("LOAD","fonts/arial.ttf",35);
            lbLoad->setPosition(Vec2(btnLoad->getContentSize().width*0.5,btnLoad->getContentSize().height*0.5));
            btnLoad->addChild(lbLoad);
            lbLoad->setColor(Color3B(100,0,0));


            bool isLock = UserDefault::getInstance()->getBoolForKey(KEY_INAPP_VIP,false);
            if(vecLock.at(i)=="0" && !isLock ){
//                lbLoad->setString("LOCK");
                Sprite *imgLock = Sprite::create("store/lock.png");
                btnLoad->addChild(imgLock);
                imgLock->setPosition(Vec2(btnLoad->getContentSize().width*0.1,btnLoad->getContentSize().height/2));
                imgLock->setScale(0.35);
            }

            Menu *menu = Menu::create();
            menu->setPosition(Vec2::ZERO);
            menu->addChild(btnListen);
            menu->addChild(btnLoad);

            vecListenBtn.pushBack(btnListen);
            counter++;

            imgBottomLine->setScaleX(1.1);
            imgBottomLine->setPosition(Vec2(layerBox->getContentSize().width*0.46,layerBox->getContentSize().height*0.5));
            layerBox->addChild(imgBottomLine);
            Sprite *imgSideLine = Sprite::create("load song/line 02.png");
            imgSideLine->setPosition(Vec2(scrollLayer->getContentSize().width*0.55,layerBox->getContentSize().height*0.5));
//            layerBox->addChild(imgSideLine);
            layerBox->setPosition(Vec2(0,topMargin));
            topMargin = topMargin + layerBox->getContentSize().height;
            scrollLayer->addChild(layerBox);
            layerBox->addChild(lblName);
            layerBox->addChild(lblSubTitle);
            layerBox->addChild(menu);
        }

    }

    scrollLayer->setContentSize(Size(scrollView->getContentSize().width,topMargin));
    scrollView->setContentOffset(Vec2(0,-(scrollLayer->getContentSize().height-scrollView->getViewSize().height)),true);


}

void LoadMusic::backCall() {
//    Director::getInstance()->popScene();
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    if(GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR || GameData::getInstance()->game_type==GAME_TYPE_REPLICATE_SONG) {

        SoundController::stopEffectSound(listenAudioID);
        this->removeFromParent();
    }else{
        GameData::pauseRecording();
        Director::getInstance()->popScene();
    }

}

void LoadMusic::listenSongCall(cocos2d::Ref *sender) {

    int tag = ((MenuItemImage *) sender)->getTag();
    log("listen tah %d",tag);
    MenuItemImage *btn = ((MenuItemImage *) sender);

    Label *lblName = (Label *) btn->getChildByTag(101);

   int   n = UserDefault::getInstance()->getIntegerForKey(KEY_NUM_OF_RECORDING,0);
    bool flag = true;
    int size = GameData::getInstance()->listAllSong.size();
    int diff = size-n;
    if(diff<=tag){
        flag = false;
    }

    log("listen tah-- %d  n  %d  total %d",tag,n,size);

    if(flag || GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR || GameData::getInstance()->game_type==GAME_TYPE_REPLICATE_SONG) {
        if (lblName->getString() == "LISTEN") {
            restListenBtn();
            lblName->setString("STOP");
            SoundController::stopAllEffectSound();
            listenAudioID = SoundController::playMusicNode("jingle bell song.mp3",0.5);

        } else {
            lblName->setString("LISTEN");
           SoundController::stopEffectSound(listenAudioID);
        }

    }else{

        if (lblName->getString() == "LISTEN") {
            restListenBtn();
            lblName->setString("STOP");

            SoundController::stopEffectSound(listenAudioID);

            std::string name = GameData::getInstance()->listAllSong.at(tag)->getCString();
//            log("")
            GameData::playRecording(name);
        } else {
            lblName->setString("LISTEN");
            GameData::pauseRecording();
        }
    }

}


void LoadMusic::shopCall() {
//    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,UIStore::createScene());
//    Director::getInstance()->pushScene(effect);
    UIStore *store = UIStore::create();
    store->screenName= "loadMusic";
    this->addChild(store);
}


void LoadMusic::loadSongCall(cocos2d::Ref *sender) {
    int tag  = ((CustomMenuItemImage*)sender)->getTag();
    bool isLock = UserDefault::getInstance()->getBoolForKey(KEY_INAPP_VIP,false);
    if(vecLock.at(tag)=="0" && !isLock){
        shopCall();
        return;
    }


    int   n = UserDefault::getInstance()->getIntegerForKey(KEY_NUM_OF_RECORDING,0);
    bool flag = true;
    int size = GameData::getInstance()->listAllSong.size();
    int diff = size-n;
    if(diff<=tag){
        flag = false;
    }



    std::string key = KEY_RECORDING_NAME;
    key = key+String::createWithFormat("%d",tag)->getCString();
    std::string name = UserDefault::getInstance()->getStringForKey(key.c_str());
    GameData::getInstance()->load_recording_name = "#NULL";
    GameData::getInstance()->load_replicated_song = "#NULL";
    std::string fileName =GameData::getInstance()->listAllSong.at(tag)->getCString();
    log("recording name %s",fileName.c_str());

    if(!flag && (GameData::getInstance()->game_type!=GAME_TYPE_FILL_IN_COLOR && GameData::getInstance()->game_type!=GAME_TYPE_REPLICATE_SONG) ){
        GameData::getInstance()->load_recording_name = fileName;
        GameData::playRecording(fileName);
        GameData::pauseRecording();
        Director::getInstance()->popScene();
//        this->removeFromParent();
    }else {
        GameScene *gameScene = (GameScene*)this->getParent();
        gameScene->isReplicateAudioStart = false;
        GameData::getInstance()->load_replicated_song = fileName;

        SoundController::stopEffectSound(listenAudioID);
        if(GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR || GameData::getInstance()->game_type==GAME_TYPE_REPLICATE_SONG) {
            this->removeFromParent();

        }else{
            Director::getInstance()->popScene();

        }


    }
//    Director::getInstance()->popScene();
//    int tag = ((MenuItemImage*)sender)->getTag();
//    std::string songName = vecSongName.at(tag)->getCString();
//    log("song name %s",songName.c_str());
//    GameData::getInstance()->game_type = 1;
//    GameData::getInstance()->load_song_name = songName;
//    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,GameScene::createScene());
//    Director::getInstance()->replaceScene(effect);
}
void LoadMusic::restListenBtn() {
    for (int i = 0; i < vecListenBtn.size();i++) {
        CustomMenuItemImage *btn = vecListenBtn.at(i);
        Label *lblName  = (Label*) btn->getChildByTag(101);
        lblName->setString("LISTEN");
    }
}