//
// Created by assertinfotech on 25/11/17.
//

#ifndef PROJ_ANDROID_STUDIO_UILOADSONG_H
#define PROJ_ANDROID_STUDIO_UILOADSONG_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "PianoSong.h"
#include "CustomMenuItemImage.h"
USING_NS_CC;
USING_NS_CC_EXT;

class UILoadSong : public :: cocos2d::Layer{

public:
    Size winSize = Director::getInstance()->getWinSize();

    Vector<String *> vecSongName;

    Vector<CustomMenuItemImage*> vecListenBtn;
    bool isSongPlay;
    UILoadSong();
    ~UILoadSong();

    int soungNodeCounter = 0;
    String *colorNods;

    PianoSong *pianoSong;

    virtual bool init();
    static Scene* createScene();

    CREATE_FUNC(UILoadSong);

    void addHeaderPanel();

    void createComponents();

    void addCenterComponents();

    void backCall();


    void restListenBtn();

    void loadSongCall(cocos2d::Ref *sender);
    void listenSongCall(cocos2d::Ref *sender);



};


#endif //PROJ_ANDROID_STUDIO_UILOADSONG_H
