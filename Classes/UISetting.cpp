//
//

#include "UISetting.h"

#include "TouchableLayer.h"
#include "GameScene.h"
#include "GameData.h"
#include "ColorPanel.h"
#include "UIStore.h"
#include "CustomMenuItemImage.h"

UISetting::UISetting() {

}

UISetting::~UISetting() {

}

bool UISetting::init() {



    if(!Layer::init()){
        return  false;
    }

    createComponents();

    return true;
}


void UISetting::createComponents() {


    TouchableLayer *touchableLayer = TouchableLayer::createLayer(Color4B(0,0,0,150));
    this->addChild(touchableLayer);
    imgBG = Sprite::create("setting/BG game setings.png");
    imgBG->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBG);

    Label *lblTitle = Label::createWithTTF("GAME SETTINGS","fonts/arial.ttf",40);
    lblTitle->setPosition(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.92);
    imgBG->addChild(lblTitle);

    lblTitle->setColor(Color3B(50,50,50));
    lblTitle->enableGlow(Color4B(0,0,0,150));




    CustomMenuItemImage *btnPainting  =   CustomMenuItemImage::create("setting/painting setings.png","setting/painting setings.png",CC_CALLBACK_0(UISetting::paintingSettingCall,this));
    btnPainting->setPosition(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.6);


    Label *lblPainting = Label::createWithTTF("PAINTING SETTINGS","fonts/arial.ttf",40);
    lblPainting->setPosition(btnPainting->getContentSize().width/2,btnPainting->getContentSize().height*0.5);
    btnPainting->addChild(lblPainting);


    lblPainting->enableGlow(Color4B(0,0,0,150));





    CustomMenuItemImage *btnKeyboard  =   CustomMenuItemImage::create("setting/painting setings.png","setting/painting setings.png",CC_CALLBACK_0(UISetting::keyboardSettingCall,this));
    btnKeyboard->setPosition(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.4);


    Label *lblKeyboard = Label::createWithTTF("KEYBOARD SETTINGS","fonts/arial.ttf",40);
    lblKeyboard->setPosition(btnKeyboard->getContentSize().width/2,btnKeyboard->getContentSize().height*0.5);
    btnKeyboard->addChild(lblKeyboard);

    lblKeyboard->enableGlow(Color4B(0,0,0,150));

    CustomMenuItemImage *btnBeck  =   CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(Node::removeFromParent,this));
    btnBeck->setPosition(imgBG->getContentSize().width*0.1,imgBG->getContentSize().height*0.92);



    Menu *menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnKeyboard);
    menu->addChild(btnPainting);
    menu->addChild(btnBeck);
    imgBG->addChild(menu);







}

void UISetting::paintingSettingCall() {
    createPaintingSetting();
}

 void UISetting::keyboardSettingCall() {
     KeyboardSelection *keyboardSelection = KeyboardSelection::create();
     this->addChild(keyboardSelection);

 }

void UISetting::backCall() {

    log("save scaleX %f, Y: %f",scaleX,scaleY);
    UserDefault::getInstance()->setFloatForKey(KEY_GRID_HEIGHT,scaleY);
    UserDefault::getInstance()->setFloatForKey(KEY_GRID_WIDTH,scaleX);

    GameScene *gameScene = static_cast<GameScene*>(this->getParent());
    gameScene->resetGridItem();
    gameScene->scrollView->setContentOffset(Vec2(0,-(gameScene->scrollView->getChildByTag(101)->getContentSize().height-gameScene->scrollView->getViewSize().height)),true);

    this->removeFromParent();
}



/** painting setting create*/

void UISetting::createPaintingSetting() {

    TouchableLayer *touchableLayer = TouchableLayer::createLayer(Color4B(0,0,0,0));
    this->addChild(touchableLayer);
    imgPaintingSetting = Sprite::create("setting/BG painting setings.png");
    imgPaintingSetting->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgPaintingSetting);

    Label *lblTitle = Label::createWithTTF("PAINTING SETTINGS","fonts/arial.ttf",40);
    lblTitle->setPosition(imgPaintingSetting->getContentSize().width/2,imgPaintingSetting->getContentSize().height*0.93);
    imgPaintingSetting->addChild(lblTitle);

    lblTitle->setColor(Color3B(50,50,50));
    lblTitle->enableGlow(Color4B(0,0,0,150));

    CustomMenuItemImage *btnBeck  =   CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(Node::removeFromParent,this));
    btnBeck->setPosition(imgPaintingSetting->getContentSize().width*0.1,imgPaintingSetting->getContentSize().height*0.93);

    Menu *menu = Menu::create();
    menu->setPosition(Vec2::ZERO);

    menu->addChild(btnBeck);
    imgPaintingSetting->addChild(menu);

    lblHeight = Label::createWithTTF("Notes per column","fonts/arial.ttf",30);
    lblHeight->setPosition(imgPaintingSetting->getContentSize().width*0.3,imgPaintingSetting->getContentSize().height*0.86);
    imgPaintingSetting->addChild(lblHeight);

    lblHeight->setColor(Color3B(50,50,50));
    lblHeight->enableGlow(Color4B(0,0,0,150));

    Sprite *imgBox = Sprite::create("setting/box.png");
    imgBox->setPosition(Vec2(imgPaintingSetting->getContentSize().width/2,imgPaintingSetting->getContentSize().height*0.4));
    imgPaintingSetting->addChild(imgBox);

    Sprite *imgGridBg = Sprite::create("setting/box03.png");
    imgPaintingGrid = Sprite::create("setting/big box.png");
    imgPaintingGrid->setAnchorPoint(Vec2(0,1));
    imgGridBg->setAnchorPoint(Vec2(0,1));

    LayerColor *scrollLayer = LayerColor::create(Color4B(0,0,0,0));
    ScrollView *scrollView = ScrollView::create(imgPaintingGrid->getContentSize()/2,scrollLayer);
    scrollView->setPosition(Vec2((imgBox->getContentSize().width-scrollView->getViewSize().width)/2,(imgBox->getContentSize().height-scrollView->getViewSize().height)/2));
    imgBox->addChild(scrollView);
    scrollView->setContentSize(scrollView->getViewSize());
    imgPaintingGrid->setPosition(Vec2(0,scrollView->getContentSize().height));
    imgGridBg->setPosition(Vec2(scrollLayer->getContentSize().width*0.0,scrollLayer->getContentSize().height));


     scaleX = UserDefault::getInstance()->getFloatForKey(KEY_GRID_WIDTH);
     scaleY = UserDefault::getInstance()->getFloatForKey(KEY_GRID_HEIGHT);

    log("scale x %f y: %f",scaleX,scaleY);
    imgPaintingGrid->setScaleX((0.5+scaleX/100));
    imgPaintingGrid->setScaleY((0.5+scaleY/100));

//    scrollLayer->addChild(imgGridBg);
    scrollLayer->addChild(imgPaintingGrid);

    scrollView->setTouchEnabled(false);

//    imgBox->addChild(imgPaintingGrid);


    auto Bg_sprite = Sprite::create("setting/red bar.png");
    auto Prog_sprite = Sprite::create("setting/pink bar upper.png");
    auto Thumb_sprite = Sprite::create("setting/icon bar.png");
    ControlSlider *slider1 = ControlSlider::create(Bg_sprite, Prog_sprite, Thumb_sprite);



    slider1->setMinimumValue(1);
    slider1->setMaximumValue(100);
    slider1->setValue(scaleY);
    slider1->addTargetWithActionForControlEvents(this, cccontrol_selector(UISetting::heightChangedCallback), cocos2d::extension::Control::EventType::VALUE_CHANGED);
    imgPaintingSetting->addChild(slider1);

    slider1->setPosition(imgPaintingSetting->getContentSize().width*0.5,imgPaintingSetting->getContentSize().height*0.8);

    Sprite *imgSliderBG1 = Sprite::create("setting/BG bar.png");
    imgSliderBG1->setPosition(imgPaintingSetting->getContentSize().width*0.5,imgPaintingSetting->getContentSize().height*0.8);
    imgPaintingSetting->addChild(imgSliderBG1);





//    slider1->setValue()
    lblWidth = Label::createWithTTF("Notes per row","fonts/arial.ttf",30);
    lblWidth->setPosition(imgPaintingSetting->getContentSize().width*0.3,imgPaintingSetting->getContentSize().height*0.72);
    imgPaintingSetting->addChild(lblWidth);

    lblWidth->setColor(Color3B(50,50,50));
    lblWidth->enableGlow(Color4B(0,0,0,150));

    auto Bg_sprite2 = Sprite::create("setting/red bar.png");

    auto Prog_sprite2= Sprite::create("setting/pink bar upper.png");

    auto Thumb_sprite2 = Sprite::create("setting/icon bar.png");
    ControlSlider *slider2 = ControlSlider::create(Bg_sprite2, Prog_sprite2, Thumb_sprite2);

    float xw = 8/(1.5-scaleX/100);
    float xh = 8/(1.5-scaleY/100);
    lblWidth->setString(String::createWithFormat("Notes per row(%d)",(int)xw)->getCString());
    lblHeight->setString(String::createWithFormat("Notes per column(%d)",(int)xh)->getCString());
    slider2->setMaximumValue(100);
    slider2->setMinimumValue(1);
    slider2->setValue(scaleX);
    slider2->addTargetWithActionForControlEvents(this, cccontrol_selector(UISetting::widthChangedCallback), cocos2d::extension::Control::EventType::VALUE_CHANGED);
    imgPaintingSetting->addChild(slider2);
    slider2->setPosition(imgPaintingSetting->getContentSize().width*0.5,imgPaintingSetting->getContentSize().height*0.66);


    Sprite *imgSliderBG2 = Sprite::create("setting/BG bar.png");
    imgSliderBG2->setPosition(imgPaintingSetting->getContentSize().width*0.5,imgPaintingSetting->getContentSize().height*0.66);
    imgPaintingSetting->addChild(imgSliderBG2);

    CustomMenuItemImage *btnApply =   CustomMenuItemImage::create("setting/apply.png","setting/apply.png",CC_CALLBACK_0(UISetting::backCall,this));
    btnApply->setPosition(imgPaintingSetting->getContentSize().width*0.5,imgPaintingSetting->getContentSize().height*0.15);

    Label *lblApply = Label::createWithTTF("APPLY","fonts/arial.ttf",30);
    lblApply->setPosition(Vec2(btnApply->getContentSize().width/2,btnApply->getContentSize().height/2));
    btnApply->addChild(lblApply);
    lblApply->enableGlow(Color4B(0,0,0,150));
    menu->addChild(btnApply);

}
void UISetting::paintingBackCall() {
    imgBG->setVisible(true);
    imgPaintingSetting->removeFromParent();

}
void UISetting::heightChangedCallback(Ref *sender, cocos2d::extension::Control::EventType evnt) {
    log("valuse change");
    ControlSlider *slider = static_cast<cocos2d::extension::ControlSlider*>(sender);
    int tag = slider->getTag();
    float value = static_cast<cocos2d::extension::ControlSlider*>(sender)->getValue();
    log("slider value %f",value);
    scaleY = value;
    imgPaintingGrid->setScaleY((1.5-value/100));
    float n = 8/(1.5-value/100);
    lblHeight->setString(String::createWithFormat("Notes per column(%d)",(int)n)->getCString());
    float scaleFactor = 1.0;
}
void UISetting::widthChangedCallback(Ref *sender, cocos2d::extension::Control::EventType evnt) {
    float value = static_cast<cocos2d::extension::ControlSlider*>(sender)->getValue();
    log("slider value width %f",value);
    scaleX = value;
    imgPaintingGrid->setScaleX((1.5-value/100));
    float n = 8/((1.5-value/100));
    lblWidth->setString(String::createWithFormat("Notes per row(%d)",(int)n)->getCString());

}

KeyboardSelection::KeyboardSelection() {

}
KeyboardSelection::~KeyboardSelection() {

}


bool KeyboardSelection::init() {

    if(!Layer::init()){
        return  false;
    }


    initKeyboardSelection();
    return true;
}



void KeyboardSelection::initKeyboardSelection() {

    log("initKeyboardSelection1");

    log("initKeyboardSelection2");
    TouchableLayer *touchableLayer = TouchableLayer::createLayer(Color4B(0,0,0,0));
    this->addChild(touchableLayer);
    Sprite *imgBG = Sprite::create("setting/BG_22 keyBoard.png");
    imgBG->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBG);



    LayerColor *scrollLayer = LayerColor::create(Color4B(10,10,10,0));

    ScrollView *scrollView = ScrollView::create(Size(winSize.width,winSize.height*0.91),scrollLayer);
    this->addChild(scrollView);
    scrollView->setPosition(Vec2(0,10));
    scrollView->setDirection(ScrollView::Direction::VERTICAL);

    log("initKeyboardSelection3");
    float topMargin = 0;


    std::string arrAccess[4] = {"1","0","0","0"};
    std::string arrName[4] =  {"Classic","Retro","Primary","Custom"};
    for (int i = 0; i < 4; i++) {

        LayerColor *layerBox = LayerColor::create(Color4B(255, 255, 255, 0));
        layerBox->setContentSize(
        Size(winSize.width, winSize.height * 0.23));

        if (i % 2 != 0) {
            Sprite *imgBox = Sprite::create("setting/BG piano.png");
            layerBox->addChild(imgBox);
            imgBox->setPosition(Vec2(layerBox->getContentSize().width / 2,
                                     layerBox->getContentSize().height / 2));
        }


        Sprite *imgPianoBoard = getKeyboard(i);//Sprite::create("setting/key board_normal.png");
        layerBox->addChild(imgPianoBoard);
        imgPianoBoard->setPosition(Vec2(layerBox->getContentSize().width / 2,
                                 layerBox->getContentSize().height / 2));

        Label *lblName = Label::createWithTTF(arrName[i].c_str(),"fonts/arial.ttf",35);
        lblName->setPosition(Vec2(imgPianoBoard->getContentSize().width*0.2,imgPianoBoard->getContentSize().height*0.865));
        imgPianoBoard->addChild(lblName);
        lblName->enableOutline(Color4B(50,0,0,200),2);
        lblName->setColor(Color3B::WHITE);

        CustomMenuItemImage *btnSwipe = CustomMenuItemImage::create("setting/swipe colour button.png","setting/swipe colour button.png",CC_CALLBACK_1(KeyboardSelection::swipeColorCall,this));
        btnSwipe->setPosition(Vec2(layerBox->getContentSize().width*0.85,layerBox->getContentSize().height*0.85));
        btnSwipe->setTag((i+1));


        Menu *menu = Menu::create();
        menu->addChild(btnSwipe);
//        customKeyboardCreate
        if(i==3) {

            btnSwipe->setNormalImage(Sprite::create("setting/select.png"));
            btnSwipe->setSelectedImage(Sprite::create("setting/select.png"));
            CustomMenuItemImage *btnEdit = CustomMenuItemImage::create("setting/edit.png",
                                                           "setting/edit.png",
                                                           CC_CALLBACK_0(
                                                                   KeyboardSelection::customKeyboardCreate,
                                                                   this));
            btnEdit->setPosition(Vec2(layerBox->getContentSize().width * 0.85-btnSwipe->getContentSize().width/2-btnEdit->getContentSize().width*0.5,
                                      layerBox->getContentSize().height * 0.85));
//            btnEdit->setScale(0.8);

            bool isLock = UserDefault::getInstance()->getBoolForKey(KEY_INAPP_VIP,false);
            if(!isLock){
                btnEdit->setEnabled(false);
            }
            menu->addChild(btnEdit);
        }
        menu->setPosition(Vec2::ZERO);
        layerBox->addChild(menu);
        layerBox->setPosition(Vec2(0,topMargin));
        topMargin = topMargin + layerBox->getContentSize().height;
        bool isLock = UserDefault::getInstance()->getBoolForKey(KEY_INAPP_VIP,false);
        if(!isLock) {
            if (arrAccess[i] == "0") {

                btnSwipe->setEnabled(false);
                LayerColor *layerLock = LayerColor::create(Color4B(0, 0, 0, 150));
                layerLock->setContentSize(layerBox->getContentSize());
                layerBox->addChild(layerLock);

                CustomMenuItemImage *btnLock = CustomMenuItemImage::create("store/lock.png", "store/lock.png",
                                                               CC_CALLBACK_0(
                                                                       KeyboardSelection::shopCall,
                                                                       this));

                btnLock->setPosition(Vec2(layerLock->getContentSize().width / 2,
                                          layerLock->getContentSize().height / 2));

                Menu *menu1 = Menu::create(btnLock, NULL);
                menu1->setPosition(Vec2::ZERO);
                layerLock->addChild(menu1);
            }


        }



        scrollLayer->addChild(layerBox);
    }
    scrollLayer->setContentSize(Size(scrollView->getContentSize().width,topMargin));
    scrollView->setContentOffset(Vec2(0,-(scrollLayer->getContentSize().height-scrollView->getViewSize().height)),true);
    addHeaderPanel();
}




void KeyboardSelection::shopCall() {
//    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,UIStore::createScene());
    UIStore *store = UIStore::create();
    store->screenName= "keyboardSelection";
    this->addChild(store);
}

Sprite* KeyboardSelection::getKeyboard(int type) {

    Sprite *imgKeyBoard = Sprite::create("setting/key board_normal.png");
    std::string keyCode = KEY_COLOR_CODE;
    keyCode = keyCode + String::createWithFormat("%d", 0)->getCString();

    int selected = UserDefault::getInstance()->getIntegerForKey(KEY_KEYBOARD_SELECTE,0);
    if(type== selected){
        imgKeyBoard->setTexture("setting/key board_select.png");
    }

    int colorIndex = type*7;
    Sprite* btn1 = Sprite::create("game screen/white key normal.png");
    ColorPanel *colorPanel = new ColorPanel();
    float leftMergin = btn1->getContentSize().width*0.9;
    for (int i = 0; i < 7; i++) {
        Sprite* btn1 = Sprite::create("game screen/white key normal.png");
        btn1->setPosition(Vec2(leftMergin,imgKeyBoard->getContentSize().height*0.45));
        leftMergin+= btn1->getContentSize().width*1.03;
        btn1->setOpacity(0);
        Sprite* colorBox = Sprite::create("game screen/center key color.png");
        colorBox->setPosition(Vec2(btn1->getContentSize().width*0.55,btn1->getContentSize().height*0.25));
        btn1->addChild(colorBox);





        if(type==3){
            std::string customKey = KEY_CUSTOM_KEYBOARD_COLOR;
            customKey  = customKey+String::createWithFormat("%d",i)->getCString();
            int index =  UserDefault::getInstance()->getIntegerForKey(customKey.c_str());


            std::string colorId = colorPanel->arrColor[index][0];
            colorIndex++;
            log(" color id %s",colorId.c_str());
            String *strColorId = String::create(colorId);

            __Array *arrColor =  strColorId->componentsSeparatedByString(",");


            int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
            int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
            int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
            log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);
            colorBox->setColor(Color3B(r,g,b));



        }else{
            std::string colorId = colorPanel->arrColor[colorIndex][0];
            colorIndex++;
            log(" color id %s",colorId.c_str());
            String *strColorId = String::create(colorId);

            __Array *arrColor =  strColorId->componentsSeparatedByString(",");

            int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
            int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
            int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
            log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);
            colorBox->setColor(Color3B(r,g,b));

        }



        imgKeyBoard->addChild(btn1);
        log("getKeyboard3.3");



    }




    return imgKeyBoard;



    }



void KeyboardSelection::addHeaderPanel() {
    LayerColor *layerHeader = LayerColor::create(Color4B::BLACK);
    this->addChild(layerHeader);
    Sprite *imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height-layerHeader->getContentSize().height));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
    layerHeader->addChild(imgHeader);

    Label* lblTitle = Label::createWithTTF("SELECT KEYBOARD COLORS","fonts/arial.ttf",35);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);

    lblTitle->enableOutline(Color4B::BLACK,1);
    lblTitle->setColor(Color3B::BLACK);

    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(Node::removeFromParent,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.08,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);
}
void KeyboardSelection::swipeColorCall(cocos2d::Ref *sender) {

    int tag = ((CustomMenuItemImage*)sender)->getTag();
    log("tag value %d",tag);
    setKeyboardType(tag);
//    Director::getInstance()->replaceScene(GameScene::createScene());
//    customKeyboardCreate();

    GameScene *gameScene = static_cast<GameScene*>(this->getParent()->getParent());
    gameScene->keyboardLayer->setKeyboardKeysColor();
    gameScene->resetGridItem();
    gameScene->resetGridColor();
    this->removeFromParent();
}



void KeyboardSelection::setKeyboardType(int boardNumber) {


    log("keyBoard number %d",boardNumber);
    UserDefault::getInstance()->setIntegerForKey(KEY_KEYBOARD_SELECTE,boardNumber-1);


    if(boardNumber==4){
        for (int i = 0; i < 7; i++) {
            std::string customKey = KEY_CUSTOM_KEYBOARD_COLOR;
            customKey = customKey + String::createWithFormat("%d", i)->getCString();
            int n = UserDefault::getInstance()->getIntegerForKey(customKey.c_str());


            std::string keyCode = KEY_COLOR_CODE;
            keyCode = keyCode + String::createWithFormat("%d", i)->getCString();
            UserDefault::getInstance()->setIntegerForKey(keyCode.c_str(), n);
        }


    }else {

        for (int i = 0; i < 7; i++) {
            std::string keyCode = KEY_COLOR_CODE;
            keyCode = keyCode + String::createWithFormat("%d", i)->getCString();
            UserDefault::getInstance()->setIntegerForKey(keyCode.c_str(), ((boardNumber - 1) * 7) + i);

        }
    }


}


void KeyboardSelection::customKeyboardCreate() {

    layer_customKeyboard = TouchableLayer::createLayer(Color4B(0,0,0,150));
    this->addChild(layer_customKeyboard);



    Menu *menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    Sprite *imgBg = Sprite::create("setting/custom keyboard/bg27.png");
    imgBg->setPosition(Vec2(layer_customKeyboard->getContentSize().width/2,layer_customKeyboard->getContentSize().height/2));
    layer_customKeyboard->addChild(imgBg);

    Label *lblHeader = Label::createWithTTF("SWITCH COLORS","fonts/arial.ttf",40);
    lblHeader->setPosition(Vec2(layer_customKeyboard->getContentSize().width/2,layer_customKeyboard->getContentSize().height*0.9));
    layer_customKeyboard->addChild(lblHeader);
    lblHeader->setColor(Color3B(30,0,1));


    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("setting/back button.png","setting/back button.png",CC_CALLBACK_0(Node::removeFromParent,layer_customKeyboard));
    btnBack->setPosition(Vec2(imgBg->getContentSize().width*0.1,imgBg->getContentSize().height*0.96));

    menu->addChild(btnBack);

    // add keyBoard
    Sprite *imgKeyBoard = Sprite::create("setting/key board_normal.png");

    std::string keyCode = KEY_COLOR_CODE;
    keyCode = keyCode + String::createWithFormat("%d", 0)->getCString();

    int selected = UserDefault::getInstance()->getIntegerForKey(keyCode.c_str(),0);

        imgKeyBoard->setTexture("setting/key board_select.png");

    imgKeyBoard->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.835));
    imgKeyBoard->setScale(0.715);

    imgBg->addChild(imgKeyBoard);
    int colorIndex = 0;
    CustomMenuItemImage* btn1 = CustomMenuItemImage::create("game screen/white key normal.png","game screen/white key normal.png",CC_CALLBACK_1(KeyboardSelection::customKeySelectCall,this));
    ColorPanel *colorPanel = new ColorPanel();
    float leftMergin = btn1->getContentSize().width*0.9;
    for (int i = 0; i < 7; i++) {
        Sprite* btn1 = Sprite::create("game screen/white key normal.png");
        btn1->setPosition(Vec2(leftMergin,imgKeyBoard->getContentSize().height*0.45));
        leftMergin+= btn1->getContentSize().width*1.03;
        btn1->setOpacity(0);
        CustomMenuItemImage* colorBox = CustomMenuItemImage::create("game screen/center key color.png","game screen/center key color.png",CC_CALLBACK_1(KeyboardSelection::customKeySelectCall,this));
        colorBox->setPosition(Vec2(btn1->getContentSize().width*0.55,btn1->getContentSize().height*0.25));
        btn1->addChild(colorBox);
        colorBox->setTag(i);

        Menu *menu1 = Menu::create();
        menu1->setPosition(Vec2::ZERO);
        menu1->addChild(colorBox);

        btn1->addChild(menu1);

        std::string customKey = KEY_CUSTOM_KEYBOARD_COLOR;
        customKey  = customKey+String::createWithFormat("%d",i)->getCString();
        int index =  UserDefault::getInstance()->getIntegerForKey(customKey.c_str());
        log("save color id %d",index);

        arrKeyValue[i] = index;

        std::string colorId = colorPanel->arrColor[index][0];
        colorIndex++;
        log(" color id %s",colorId.c_str());
        String *strColorId = String::create(colorId);

        __Array *arrColor =  strColorId->componentsSeparatedByString(",");



        int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
        int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
        int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());

        colorBox->setColor(Color3B(r,g,b));



        Sprite *imgSelected = Sprite::create("setting/custom keyboard/right sign.png");
        imgSelected->setPosition(Vec2(colorBox->getContentSize().width/2,colorBox->getContentSize().height/2));

        colorBox->addChild(imgSelected);
        imgSelected->setTag(101);

        imgSelected->setVisible(false);


        vecCustomKeys.pushBack(colorBox);


        imgKeyBoard->addChild(btn1);
        log("getKeyboard3.3");
    }


    Label *lblRecentColor = Label::createWithTTF("RECENT COLOR","fonts/arial.ttf",18);
    lblRecentColor->setPosition(Vec2(imgBg->getContentSize().width*0.25,imgBg->getContentSize().height*0.73));
    imgBg->addChild(lblRecentColor);
    lblRecentColor->setColor(Color3B(30,0,1));


    Label *lblSwipeColor = Label::createWithTTF("SWITCH COLOR","fonts/arial.ttf",18);
    lblSwipeColor->setPosition(Vec2(imgBg->getContentSize().width*0.75,imgBg->getContentSize().height*0.73));
    imgBg->addChild(lblSwipeColor);
    lblSwipeColor->setColor(Color3B(30,0,1));

    imgRecentColor = Sprite::create("setting/custom keyboard/color box large.png");
    imgRecentColor->setPosition(Vec2(imgBg->getContentSize().width*0.25,imgBg->getContentSize().height*0.67));
    imgBg->addChild(imgRecentColor);



    imgSwipeColor = Sprite::create("setting/custom keyboard/color box large.png");
    imgSwipeColor->setPosition(Vec2(imgBg->getContentSize().width*0.75,imgBg->getContentSize().height*0.67));
    imgBg->addChild(imgSwipeColor);



    Sprite *imgSwipeTo = Sprite::create("setting/custom keyboard/to.png");
    imgSwipeTo->setPosition(Vec2(imgBg->getContentSize().width*0.5,imgBg->getContentSize().height*0.675));
    imgBg->addChild(imgSwipeTo);
    imgSwipeTo->setScale(0.75);




    CustomMenuItemImage *btnSwipe = CustomMenuItemImage::create("setting/apply.png","setting/apply.png",CC_CALLBACK_0(KeyboardSelection::customSwipeCall,this));
    btnSwipe->setPosition(Vec2(imgBg->getContentSize().width*0.5,imgBg->getContentSize().height*0.61));
    menu->addChild(btnSwipe);
    btnSwipe->setScale(0.7);
    Label *lblSwipe = Label::createWithTTF("SWITCH","fonts/arial.ttf",30);
    lblSwipe->setPosition(Vec2(btnSwipe->getContentSize().width/2,btnSwipe->getContentSize().height/2));
    btnSwipe->addChild(lblSwipe);

    lblSwipe->enableOutline(Color4B::BLACK,2);

    Label *lblSelectAnyColor = Label::createWithTTF("SELECT ANY COLOR","fonts/arial.ttf",20);
    lblSelectAnyColor->setPosition(Vec2(imgBg->getContentSize().width*0.3,imgBg->getContentSize().height*0.56));
    imgBg->addChild(lblSelectAnyColor);
    lblSelectAnyColor->setColor(Color3B(30,0,1));


    LayerColor *scrollLayer = LayerColor::create(Color4B(30,50,50,0));
    ScrollView *scrollView  = ScrollView::create(Size(imgBg->getContentSize().width,imgBg->getContentSize().height*0.29),scrollLayer);

    scrollView->setDirection(ScrollView::Direction::VERTICAL);

    scrollView->setPosition(Vec2(imgBg->getContentSize().width*0.0,imgBg->getContentSize().height*0.25));


    float leftMargin = 0;
    float topMargin = 0;
    for (int i = 36; i >0; i--) {
    Sprite *imgBox = Sprite::create("setting/custom keyboard/color box.png");

        vecColorBox.pushBack(imgBox);
        addColorBoxTouchEvents(imgBox);

        imgBox->setTag(i-1);
        imgBox->setColor(Color3B::GRAY);




        Sprite *imgSelected = Sprite::create("setting/custom keyboard/blue frame.png");
        imgSelected->setPosition(Vec2(imgBox->getContentSize().width/2,imgBox->getContentSize().height/2));
        imgBox->addChild(imgSelected);
        imgSelected->setTag(101);

        imgSelected->setVisible(false);






        if(i%6==0) {
            topMargin = topMargin + imgBox->getContentSize().height;
            leftMargin = imgBox->getContentSize().width*7;
        }else{
            leftMargin = leftMargin - imgBox->getContentSize().width*1.15;
        }

        imgBox->setPosition(Vec2(leftMargin,topMargin));



        std::string colorId = colorPanel->arrColor[i-1][0];
        log(" color id %s",colorId.c_str());
        String *strColorId = String::create(colorId);

        __Array *arrColor =  strColorId->componentsSeparatedByString(",");


        int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
        int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
        int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
        log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);



        imgBox->setColor(Color3B(r,g,b));

        scrollLayer->addChild(imgBox);
    }

//    scrollLayer->addChild(menu);

    scrollView->setContentSize(Size(scrollView->getContentSize().width,topMargin+40));
    scrollView->setContentOffset(Vec2(0,-(scrollView->getContentSize().height-scrollView->getViewSize().height)),false);



    imgBg->addChild(scrollView);







    Label *lblRestColor = Label::createWithTTF("REST COLOR","fonts/arial.ttf",20);
    lblRestColor->setPosition(Vec2(imgBg->getContentSize().width*0.2,imgBg->getContentSize().height*0.22));
    imgBg->addChild(lblRestColor);
    lblRestColor->setColor(Color3B(30,0,1));




    imgRestColor = Sprite::create("setting/custom keyboard/color box large.png");
    imgRestColor->setPosition(Vec2(imgBg->getContentSize().width*0.2,imgBg->getContentSize().height*0.14));
    imgBg->addChild(imgRestColor);


    float leftMaring = imgBg->getContentSize().width*0.4;
    float topHeight  = imgBg->getContentSize().height*0.17;
    for (int i = 0; i < 8; i++) {
        CustomMenuItemImage *btnRestColor = CustomMenuItemImage::create("setting/custom keyboard/color box.png","setting/custom keyboard/color box.png",CC_CALLBACK_1(KeyboardSelection::restColorCall,this));

        btnRestColor->setTag(i);


        Sprite *imgHigilite = Sprite::create("setting/custom keyboard/blue frame.png");
        imgHigilite->setPosition(Vec2(btnRestColor->getContentSize().width/2,btnRestColor->getContentSize().height/2));
        btnRestColor->addChild(imgHigilite);
        imgHigilite->setTag(101);

        Sprite *imgRight = Sprite::create("setting/custom keyboard/right sign.png");
        imgRight->setPosition(Vec2(btnRestColor->getContentSize().width/2,btnRestColor->getContentSize().height/2));
        btnRestColor->addChild(imgRight);
        imgRight->setTag(102);

        vecRectColor.pushBack(btnRestColor);
        restSelected = UserDefault::getInstance()->getIntegerForKey(KEY_COLOR_CODE_REST);
        imgHigilite->setVisible(false);
        imgRight->setVisible(false);

        if(restSelected==i) {
            imgHigilite->setVisible(true);
            imgRight->setVisible(true);
            ColorPanel *colorPanel = new ColorPanel();
            std::string colorId = colorPanel->arrRestColor[i];
            log(" color id %s",colorId.c_str());
            String *strColorId = String::create(colorId);
            __Array *arrColor =  strColorId->componentsSeparatedByString(",");
            int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
            int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
            int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
            log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);

            imgRestColor->setColor(Color3B(r,g,b));
        } else{


        }
        std::string colorId = colorPanel->arrRestColor[i];
        log(" color id %s",colorId.c_str());
        String *strColorId = String::create(colorId);
        __Array *arrColor =  strColorId->componentsSeparatedByString(",");
        int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
        int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
        int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
        log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);
        btnRestColor->setColor(Color3B(r,g,b));


        btnRestColor->setPosition(Vec2(leftMaring,topHeight));

        if(i==3){
            leftMaring = imgBg->getContentSize().width*0.4;
            topHeight  = imgBg->getContentSize().height*0.15-imgRestColor->getContentSize().height*0.5;

            log("if part");
        }else{
            log("if else call");
            leftMaring += btnRestColor->getContentSize().width*1.2;
        }




        Menu *menu1 = Menu::create();
        menu1->addChild(btnRestColor);
        menu1->setPosition(Vec2::ZERO);
        imgBg->addChild(menu1);


    }





    CustomMenuItemImage *btnDone = CustomMenuItemImage::create("setting/custom keyboard/done.png","setting/custom keyboard/done.png",CC_CALLBACK_0(KeyboardSelection::doneCall,this));
    btnDone->setPosition(Vec2(imgBg->getContentSize().width*0.5,imgBg->getContentSize().height*0.04));
    menu->addChild(btnDone);



    imgBg->addChild(menu);


}





void KeyboardSelection::customKeySelectCall(cocos2d::Ref * sender){


    ColorPanel* colorPanel = new ColorPanel();


    CustomMenuItemImage * btn = (CustomMenuItemImage*)sender;
    int tag = btn->getTag();
    resetCustomKeys();
    btn->getChildByTag(101)->setVisible(true);


    std::string customKey = KEY_CUSTOM_KEYBOARD_COLOR;
    customKey  = customKey+String::createWithFormat("%d",tag)->getCString();
    int index =  UserDefault::getInstance()->getIntegerForKey(customKey.c_str());

    std::string colorId = colorPanel->arrColor[index][0];
    String *strColorId = String::create(colorId);

    __Array *arrColor =  strColorId->componentsSeparatedByString(",");

    int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
    int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
    int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());

    selectedKeyColor = tag;
    imgRecentColor->setColor(Color3B(r,g,b));


}


void KeyboardSelection::resetCustomKeys() {
    for (int i = 0; i < vecCustomKeys.size(); i++) {
        vecCustomKeys.at(i)->getChildByTag(101)->setVisible(false);
    }
}


void KeyboardSelection::customSwipeCall() {

    ColorPanel *colorPanel = new ColorPanel();

    for (int i = 0; i < 7; i++) {
        if(arrKeyValue[i] == selectedBoxColor){
            MessageBox("This color already in keyboard please choose other color.","");
            return;
        }
    }

    std::string colorId = colorPanel->arrColor[selectedBoxColor][0];
    String *strColorId = String::create(colorId);

    __Array *arrColor =  strColorId->componentsSeparatedByString(",");

    int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
    int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
    int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());





    imgRecentColor->setColor(Color3B(r,g,b));

    log("selected key color %d size %d",selectedKeyColor,vecCustomKeys.size());
    vecCustomKeys.at(selectedKeyColor)->setColor(Color3B(r,g,b));



    arrKeyValue[selectedKeyColor] = selectedBoxColor;
}



void KeyboardSelection::doneCall() {

    for (int i = 0; i < 7; i++) {
        std::string customKey = KEY_CUSTOM_KEYBOARD_COLOR;
    customKey  = customKey+String::createWithFormat("%d",i)->getCString();


    log("key for keyboard %s value %d",customKey.c_str(),arrKeyValue[i]);

        UserDefault::getInstance()->setIntegerForKey(customKey.c_str(),arrKeyValue[i]);


        std::string keyCode = KEY_COLOR_CODE;
        keyCode = keyCode + String::createWithFormat("%d", i)->getCString();
        UserDefault::getInstance()->setIntegerForKey(keyCode.c_str(), arrKeyValue[i]);



    }
    UserDefault::getInstance()->setIntegerForKey(KEY_KEYBOARD_SELECTE,3);

     UserDefault::getInstance()->setIntegerForKey(KEY_COLOR_CODE_REST,restSelected);

//    Director::getInstance()->replaceScene(GameScene::createScene());

    GameScene *gameScene = static_cast<GameScene*>(this->getParent()->getParent());
    gameScene->keyboardLayer->setKeyboardKeysColor();
    gameScene->resetGridItem();
    gameScene->resetGridColor();
    this->removeFromParent();

//    Director::getInstance()->popScene();

}

void KeyboardSelection::addColorBoxTouchEvents(Sprite *item){
    auto listener = EventListenerTouchOneByOne::create();

    listener->onTouchBegan= [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        Vec2         p              =       touch->getLocation();
        Sprite *touchSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
        Vec2         localPoint     =       touchSprite->getParent()->convertToNodeSpace(p);
        cocos2d::Rect rect          =       touchSprite->getBoundingBox();
        if(!rect.containsPoint(localPoint))
        {
            return false; // we did not consume this event, pass thru.
        }

        for (int i = 0; i < vecColorBox.size(); ++i) {
            vecColorBox.at(i)->getChildByTag(101)->setVisible(false);
        }

        touchSprite->getChildByTag(101)->setVisible(true);
        log("before");
        selectedBoxColor =  touchSprite->getTag();
log("after");
        ColorPanel *colorPanel  = new ColorPanel();
        std::string colorId = colorPanel->arrColor[selectedBoxColor][0];
        log(" color id %s",colorId.c_str());
        String *strColorId = String::create(colorId);
        __Array *arrColor =  strColorId->componentsSeparatedByString(",");


        int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
        int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
        int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
        log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);

        imgSwipeColor->setColor(Color3B(r,g,b));


        return true;// to indicate that we have consumed it.
    };


    listener->onTouchMoved= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {

        Vec2 vecStart = touch->getStartLocation();
        Vec2 vecMove = touch->getLocation();

        float dif = ccpDistance(vecStart,vecMove);
        if(dif>winSize.width*0.05){
            isTouchable = false;
//            listener->setSwallowTouches(false);
        }

    };
    listener->onTouchEnded= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
//        listener->setSwallowTouches(false);
        if(isTouchable){
//            collectAchievementPopup();
        }
        isTouchable = true;
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, item);
    listener->setSwallowTouches(false);
}


void KeyboardSelection::restColorCall(cocos2d::Ref *sender) {

    int tag = ((CustomMenuItemImage*)sender)->getTag();

    for (int i = 0; i < vecRectColor.size(); i++) {
        vecRectColor.at(i)->getChildByTag(101)->setVisible(false);
        vecRectColor.at(i)->getChildByTag(102)->setVisible(false);

    }


    ColorPanel *colorPanel = new ColorPanel();
    std::string colorId = colorPanel->arrRestColor[tag];
    log(" color id %s",colorId.c_str());
    String *strColorId = String::create(colorId);
    __Array *arrColor =  strColorId->componentsSeparatedByString(",");
    int r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
    int g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
    int b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
    log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);

    imgRestColor->setColor(Color3B(r,g,b));

    vecRectColor.at(tag)->getChildByTag(102)->setVisible(true);
    vecRectColor.at(tag)->getChildByTag(101)->setVisible(true);

    restSelected = tag;



}
