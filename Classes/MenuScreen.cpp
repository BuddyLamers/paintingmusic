//

//

#include "MenuScreen.h"
#include "Modes.h"
#include "LoadPaintingScene.h"
#include "SelectKeybord.h"
#include "Achievement.h"
#include "SavePainting.h"
#include "GameScene.h"
#include "GameData.h"
#include "LoadMusic.h"
#include "UIStore.h"
#include "CustomMenuItemImage.h"
#include "SoundController.h"
#include "Home.h"

#include "UILoadSong.h"
#include "Home.h"

USING_NS_CC;

Scene* MenuScreen::createScene()
{
    auto scene = Scene::create();
    auto layer = MenuScreen::create();
    scene->addChild(layer);
    return  scene;
}

// on "init" you need to initialize your instance
bool MenuScreen::init() {
    //////////////////////////////
    // 1. super init first
    if (!Layer::init()) {
        return false;
    }

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    this->setKeypadEnabled(true);
#endif




    layer1=TouchableLayer::createLayer();
    //layer1->setPosition(Vec2(origin.x + 0, origin.y + 0));
    this->addChild(layer1);
    layer1->setOpacity(0);



    Sprite *imgBG   =   Sprite::create("menu screen/main BG.png");

    imgBG->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBG);



    /** menu header*/
    Sprite *imgHeader   =   Sprite::create("menu screen/menu header.png");

    imgHeader->setPosition(Vec2(winSize.width/2,winSize.height-imgHeader->getContentSize().height/2));
    this->addChild(imgHeader);



    Label *lblTitle = Label::createWithTTF("MENU","fonts/arial.ttf",30);
    lblTitle->setPosition(Vec2(imgHeader->getContentSize().width/2,imgHeader->getContentSize().height/2));
    imgHeader->addChild(lblTitle);










    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object



    auto btn = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(MenuScreen::back, this));

    btn->setPosition(Vec2(imgHeader->getContentSize().width*0.08,imgHeader->getContentSize().height/2));

    auto menu = Menu::create(btn, NULL);
    menu->setPosition(Vec2::ZERO);
    imgHeader->addChild(menu);

    menuPopup();

    return true;
}

void MenuScreen::menuPopup()
{
    btnBack=true;
auto visibleSize = Director::getInstance()->getVisibleSize();
Vec2 origin = Director::getInstance()->getVisibleOrigin();





    /** popup bg image*/
    Sprite *imgPopup   =   Sprite::create("menu screen/page_05.png");
    imgPopup->setPosition(Vec2(winSize.width/2,winSize.height*0.47));
    this->addChild(imgPopup);



    auto btn1 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun1, this));

    btn1->setPosition(Vec2(imgPopup->getContentSize().width/2, imgPopup->getContentSize().height * .85f));

    auto label1 = Label::createWithTTF("Menu", "fonts/Marker Felt.ttf", 40);
    label1->setPosition(Vec2(btn1->getContentSize().width/2, btn1->getContentSize().height*.5));
    btn1->addChild(label1, 1);
    label1->setColor(Color3B::BLACK);




    auto btn2 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun2, this));

    btn2->setPosition(Vec2(imgPopup->getContentSize().width / 2,  imgPopup->getContentSize().height * .85f));





    auto btn3 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun3, this));

    btn3->setPosition(Vec2(imgPopup->getContentSize().width / 2, btn2->getPositionY()-btn2->getContentSize().height*1.02));

    auto btn4 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun4, this));

    btn4->setPosition(Vec2(imgPopup->getContentSize().width / 2, btn3->getPositionY()-btn3->getContentSize().height*1.02));


    auto btn5 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun5, this));

    btn5->setPosition(Vec2(imgPopup->getContentSize().width / 2, btn4->getPositionY()-btn4->getContentSize().height*1.02));

    auto btn6 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun6, this));

    btn6->setPosition(Vec2(imgPopup->getContentSize().width / 2, btn5->getPositionY()-btn5->getContentSize().height*1.02));


    auto btn7 = CustomMenuItemImage::create(
            "menu screen/menu btn.png",
            "menu screen/menu btn selected.png",
            CC_CALLBACK_0(MenuScreen::fun7, this));

    btn7->setPosition(Vec2(imgPopup->getContentSize().width / 2, btn6->getPositionY()-btn6->getContentSize().height*1.02));


    auto backButton = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(MenuScreen::back, this));

    backButton->setPosition(Vec2(layer1->getContentSize().width*.2f, layer1->getContentSize().height*.9f));



    auto menu = Menu::create(btn2,btn3,btn4,btn5,btn6,btn7, NULL);
    menu->setPosition(Vec2::ZERO);
    imgPopup->addChild(menu, 1);




    auto label2 = Label::createWithTTF("New Game: Choose game mode", "fonts/Marker Felt.ttf", 40);
    label2->setPosition(Vec2(btn2->getContentSize().width/2,  btn2->getContentSize().height * .5));
    btn2->addChild(label2, 1);


    auto label3 = Label::createWithTTF("Save Game", "fonts/Marker Felt.ttf", 40);
    label3->setPosition(Vec2(btn3->getContentSize().width/2, btn3->getContentSize().height*.5));
    btn3->addChild(label3, 1);

    auto label4 = Label::createWithTTF("Load Game", "fonts/Marker Felt.ttf", 40);
    label4->setPosition(Vec2(btn4->getContentSize().width/2, btn4->getContentSize().height*.5));
    btn4->addChild(label4, 1);

    auto label5 = Label::createWithTTF("Unlock all features ", "fonts/Marker Felt.ttf", 40);
    label5->setPosition(Vec2(btn5->getContentSize().width/2, btn5->getContentSize().height*.5));
    btn5->addChild(label5, 1);

    auto label6 = Label::createWithTTF("Achievements", "fonts/Marker Felt.ttf", 40);
    label6->setPosition(Vec2(btn6->getContentSize().width/2, btn6->getContentSize().height*.5));
    btn6->addChild(label6, 1);

    auto label7 = Label::createWithTTF("Exit Game", "fonts/Marker Felt.ttf", 40);
    label7->setPosition(Vec2(btn7->getContentSize().width/2, btn7->getContentSize().height*.5));
    btn7->addChild(label7, 1);

    label2->setColor(Color3B::BLACK);
    label3->setColor(Color3B::BLACK);
    label4->setColor(Color3B::BLACK);
    label5->setColor(Color3B::BLACK);
    label6->setColor(Color3B::BLACK);
    label7->setColor(Color3B::BLACK);


}

void MenuScreen::fun1()
{

}

void MenuScreen::fun2()
{
    log("func2 call");

    Director::getInstance()->replaceScene(Modes::createScene());


}

void MenuScreen::fun3()
{

    if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR || GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG){
        return;
    }
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    SavePainting *savePainting = SavePainting::create();
    this->addChild(savePainting);


}

void MenuScreen::fun4()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    TransitionFadeTR *effect = TransitionFadeTR::create(0.5,UILoadSong::createScene());
    Director::getInstance()->pushScene(effect);
}

void MenuScreen::fun5()
{
//    TransitionFadeTR *effect = TransitionFadeTR::create(0.5,UIStore::createScene());
//    Director::getInstance()->pushScene(effect);
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    this->addChild(UIStore::create());



}

void MenuScreen::fun6()
{

    SoundController::playEffectSound(SOUND_BTN_CLICK);
    TransitionFadeTR *effect = TransitionFadeTR::create(0.5,Achievement::createScene());
    Director::getInstance()->pushScene(effect);
}

void MenuScreen::fun7()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
//    GameData::getInstance()->game_type = 0;
    Director::getInstance()->replaceScene(Home::createScene());


}


void MenuScreen::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode,cocos2d::Event*event)
{

    if(btnBack) {
        log("back Button");
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK) {
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#endif

            back();
        }
    }
    }

void MenuScreen::back()
{

    log("back call--");
    btnBack=false;
    SoundController::playEffectSound(SOUND_BTN_CLICK);
   this->removeFromParent();
}