//
//

#ifndef PROJ_ANDROID_STUDIO_UISTORE_H
#define PROJ_ANDROID_STUDIO_UISTORE_H

#include "cocos2d.h"
#include "TouchableLayer.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
class UIStore : public  cocos2d::Layer{

public:
    Size winSize = Director::getInstance()->getWinSize();


    std::string screenName;
    TouchableLayer *layerCunform;
    UIStore();
    ~UIStore();
    virtual  bool init();
    CREATE_FUNC(UIStore);
    static Scene* createScene();


    void addHeaderPanel();

    void createComponents();

    void addCenterComponents();

    void backCall();

    void buySongCall(cocos2d::Ref *sender);
    void listenSongCall(cocos2d::Ref *sender);




    void homeCall();

    void buySuccessPopup();

    void cunformBuyPopup();

    void buyCunform();


};


#endif //PROJ_ANDROID_STUDIO_UISTORE_H
