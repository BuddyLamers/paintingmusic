//

//

#include "Achievement.h"
#include "MenuScreen.h"
#include "TouchableLayer.h"
#include "Home.h"
#include "SoundController.h"



USING_NS_CC;

Scene* Achievement::createScene()

{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = Achievement::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Achievement::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

    isTouchable = true;
    initComponents();
    return true;
}

void Achievement::initComponents() {

    TouchableLayer *layer = TouchableLayer::createLayer(Color4B(0,0,0,150));
    this->addChild(layer);

    Sprite *imgBg = Sprite::create("game screen/BG.png");

    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);



    Sprite *imgPopup = Sprite::create("achievement/achievement_BG.png");

    imgPopup->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgPopup);

    MenuItemImage *btnBack = MenuItemImage::create("achievement/back button.png","achievement/back button.png",CC_CALLBACK_0(Achievement::back,this));
    btnBack->setPosition(Vec2(imgPopup->getContentSize().width*0.13,imgPopup->getContentSize().height*0.92));


    Menu *backMenu = Menu::create();
    backMenu->addChild(btnBack);
    backMenu->setPosition(Vec2::ZERO);
    imgPopup->addChild(backMenu);



    Label *lblTitle = Label::createWithTTF("ACHIEVEMENT","fonts/arial.ttf",35);
    lblTitle->setPosition(Vec2(imgPopup->getContentSize().width*0.5,imgPopup->getContentSize().height*0.92));
    imgPopup->addChild(lblTitle);
    lblTitle->setColor(Color3B::BLACK);


    Label *lblMsg1 = Label::createWithTTF(" ","fonts/arial.ttf",25);
    lblMsg1->setPosition(Vec2(imgPopup->getContentSize().width*0.3,imgPopup->getContentSize().height*0.82));
    imgPopup->addChild(lblMsg1);
    lblMsg1->setColor(Color3B::RED);




    Label *lblMsg2 = Label::createWithTTF(" ","fonts/arial.ttf",35);
    lblMsg2->setPosition(Vec2(imgPopup->getContentSize().width*0.65,imgPopup->getContentSize().height*0.82));
    imgPopup->addChild(lblMsg2);
    lblMsg2->setColor(Color3B::WHITE);
    lblMsg2->enableOutline(Color4B(50,0,0,255),2);




    LayerColor * scrollLayer = LayerColor::create(Color4B(0,0,0,0));

    ScrollView *scrollView = ScrollView::create(Size(imgPopup->getContentSize().width*0.9,imgPopup->getContentSize().height*0.65),scrollLayer);
    scrollView->setPosition(Vec2(imgPopup->getContentSize().width*0.1,imgPopup->getContentSize().height*0.12));


    scrollView->setDirection(ScrollView::Direction::VERTICAL);

    float topMargin = 0;
    float leftMargin = 0;


//    Menu *menu = Menu::create();
//    menu->setPosition(Vec2::ZERO);
    for (int i = 24; i >0; i--) {
//        MenuItemImage *btn = MenuItemImage::create("achievement/unlock normal.png","achievement/unlock normal.png",CC_CALLBACK_1(Achievement::achievementCall,this));

        log("inside loop");
        Sprite *btn = Sprite::create("achievement/unlock normal.png");



        addColorBoxTouchEvents(btn);

        if(i%4==0) {
            topMargin = topMargin + btn->getContentSize().height;
            leftMargin = btn->getContentSize().width*3.385;
        }else{
            leftMargin = leftMargin - btn->getContentSize().width*0.98;
        }

        btn->setPosition(Vec2(leftMargin,topMargin));

        String *str = String::createWithFormat("%d",i);

        Label *lblNum = Label::createWithTTF(str->getCString(),"fonts/arial.ttf",25);
        lblNum->setPosition(Vec2(btn->getContentSize().width*0.19,btn->getContentSize().height*0.8));
        btn->addChild(lblNum);

//topMargin = topMargin-btn->getContentSize().width*0.7;


        scrollLayer->addChild(btn);
    }

//    scrollLayer->addChild(menu);

    scrollView->setContentSize(Size(scrollView->getContentSize().width,topMargin+70));
    scrollView->setContentOffset(Vec2(0,-(scrollView->getContentSize().height-scrollView->getViewSize().height)),false);



    imgPopup->addChild(scrollView);




}


void Achievement::achievementCall(cocos2d::Ref *sender) {
    collectAchievementPopup();

}

void Achievement::start()
{
   // Director::getInstance()->replaceScene(GamePlay::createScene());
}
void Achievement::back()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    Director::getInstance()->popScene();
}


void Achievement::collectAchievementPopup() {


    TouchableLayer *layer = TouchableLayer::createLayer(Color4B(0,0,0,150));
    this->addChild(layer);

    Sprite *imgBG = Sprite::create("achievement/page_17_BG.png");
    imgBG->setPosition(Vec2(winSize.width/2,winSize.height/2));
    layer->addChild(imgBG);


    MenuItemImage *btnBack = MenuItemImage::create("achievement/back button.png","achievement/back button.png",CC_CALLBACK_0(Node::removeFromParent,layer));
    btnBack->setPosition(Vec2(imgBG->getContentSize().width*0.13,imgBG->getContentSize().height*0.94));
    Menu *menu = Menu::create();
    menu->addChild(btnBack);
    menu->setPosition(Vec2::ZERO);
    imgBG->addChild(menu);



    Label *lblTitle = Label::createWithTTF("ACHIEVEMENT","fonts/arial.ttf",35);
    lblTitle->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.94));
    imgBG->addChild(lblTitle);
    lblTitle->setColor(Color3B(50,0,0));


    Sprite *imgRedBanner = Sprite::create("achievement/red banner.png");
    imgRedBanner->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.75));
    imgBG->addChild(imgRedBanner);




    Label *lblMsg = Label::createWithTTF("You Have Used ","fonts/arial.ttf",30);
    lblMsg->setPosition(Vec2(imgRedBanner->getContentSize().width/2,imgRedBanner->getContentSize().height*0.8));
    imgRedBanner->addChild(lblMsg);
    lblMsg->setColor(Color3B::WHITE);

    Label *lblMsg2 = Label::createWithTTF("100 C Notes","fonts/arial.ttf",45);
    lblMsg2->setPosition(Vec2(imgRedBanner->getContentSize().width/2,imgRedBanner->getContentSize().height*0.4));
    imgRedBanner->addChild(lblMsg2);
    lblMsg2->setColor(Color3B::WHITE);
    lblMsg2->enableOutline(Color4B(50,0,0,255),1);





}





void Achievement::addColorBoxTouchEvents(Sprite *item){
    auto listener = EventListenerTouchOneByOne::create();

    listener->onTouchBegan= [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {

//


        Vec2         p              =       touch->getLocation();
        Layer *touchLayer  =       static_cast<Layer*>(event->getCurrentTarget());
        Vec2         localPoint     =       touchLayer->getParent()->convertToNodeSpace(p);
        cocos2d::Rect rect          =       touchLayer->getBoundingBox();


        if(!rect.containsPoint(localPoint))
        {
            return false; // we did not consume this event, pass thru.
        }




        return true;// to indicate that we have consumed it.
    };


    listener->onTouchMoved= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {

        Vec2 vecStart = touch->getStartLocation();
        Vec2 vecMove = touch->getLocation();

        float dif = ccpDistance(vecStart,vecMove);
        if(dif>winSize.width*0.05){
            isTouchable = false;
//            listener->setSwallowTouches(false);
        }

    };
    listener->onTouchEnded= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
//        listener->setSwallowTouches(false);
        if(isTouchable){
            collectAchievementPopup();

        }

        isTouchable = true;
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, item);
    listener->setSwallowTouches(false);
}





