//
//

#include "UIRecordSong.h"
#include "GameData.h"
#include "UIRecordingPanel.h"
#include "LoadMusic.h"
#include "UIStore.h"
#include "CustomMenuItemImage.h"

UIRecordSong::UIRecordSong() {

}


UIRecordSong::~UIRecordSong() {

}


Scene* UIRecordSong::createScene() {
    Scene *scene = Scene::create();
    auto layer = UIRecordSong::create();
    scene->addChild(layer);
    return  scene;
}
bool UIRecordSong::init() {
    if(!Layer::init()){
        return  false;
    }

    initComponents();
    addHeaderPanel();
    return  true;
}

void UIRecordSong::initComponents() {


    Sprite *imgBg = Sprite::create("recording/record/page 24_BG.png");
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);


    Sprite *imgWav = Sprite::create("recording/record/sound wav.png");
    imgWav->setPosition(Vec2(winSize.width/2,winSize.height*0.8));
    this->addChild(imgWav);



    Sprite *imgBtnBG1 = Sprite::create("recording/button back BG.png");
    imgBtnBG1->setPosition(Vec2(winSize.width*0.5,winSize.height*0.5));
    this->addChild(imgBtnBG1);
    MenuItemImage *btn1 = MenuItemImage::create("recording/recore new song.png","recording/recore new song.png",CC_CALLBACK_1(UIRecordSong::btnClick,this));
    btn1->setPosition(Vec2(winSize.width/2,winSize.height*0.5));
    btn1->setTag(100);




    Sprite *imgBtnBG2 = Sprite::create("recording/button back BG.png");
    imgBtnBG2->setPosition(Vec2(winSize.width*0.5,winSize.height*0.35));
    this->addChild(imgBtnBG2);

    CustomMenuItemImage *btn2 = CustomMenuItemImage::create("recording/load song.png","recording/load song.png",CC_CALLBACK_1(UIRecordSong::btnClick,this));
    btn2->setPosition(Vec2(winSize.width/2,winSize.height*0.35));
    btn2->setTag(200);

    Sprite *imgBtnBG3 = Sprite::create("recording/button back BG.png");
    imgBtnBG3->setPosition(Vec2(winSize.width*0.5,winSize.height*0.2));
    this->addChild(imgBtnBG3);
    CustomMenuItemImage *btn3 = CustomMenuItemImage::create("recording/buy  song.png","recording/buy  song.png",CC_CALLBACK_1(UIRecordSong::btnClick,this));
    btn3->setPosition(Vec2(winSize.width/2,winSize.height*0.2));
    btn3->setTag(300);


    Menu *menu = Menu::create();
    menu->addChild(btn1);
    menu->addChild(btn2);
    menu->addChild(btn3);

    menu->setPosition(Vec2::ZERO);
    this->addChild(menu);



}



void UIRecordSong::addHeaderPanel() {
    LayerColor *layerHeader = LayerColor::create(Color4B::BLACK);
    this->addChild(layerHeader);
    Sprite *imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height-layerHeader->getContentSize().height));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
    layerHeader->addChild(imgHeader);

    Label* lblTitle = Label::createWithTTF("RECORDING SONG","fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);
    lblTitle->enableOutline(Color4B::BLACK,2);
    lblTitle->setColor(Color3B::BLACK);

    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(UIRecordSong::backCall,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.1,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);

}


void UIRecordSong::backCall() {
    Director::getInstance()->popScene();
}

void UIRecordSong::btnClick(Ref *sender) {
    int tag = ((CustomMenuItemImage*) sender)->getTag();
    if(tag==100){
        Director::getInstance()->pushScene(UIRecordingPanel::createScene());
    }else if(tag==200){
        TransitionFadeTR *effect = TransitionFadeTR::create(0.5,LoadMusic::createScene());
        Director::getInstance()->pushScene(effect);
    }else if(tag==300){
//        TransitionFadeTR *effect = TransitionFadeTR::create(0.5,UIStore::createScene());
//        Director::getInstance()->pushScene(effect);
        UIStore *store = UIStore::create();
        store->screenName= "recordSong";
        this->addChild(store);

    }

}
