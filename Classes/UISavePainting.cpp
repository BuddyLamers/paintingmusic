//
// Created by assertinfotech on 21/11/17.
//

#include "UISavePainting.h"
#include "GameData.h"
//#include "TouchableLayer.h"
#include "ui/CocosGUI.h"
//#include "GameScene.h"


UISavePainting::UISavePainting() {

}

UISavePainting::~UISavePainting() {

}

bool UISavePainting::init() {

    if(!Layer::init()){
        return false;
    }
    initComponents();
    return true;
}

void UISavePainting::initComponents() {


    TouchableLayer *layer = TouchableLayer::createLayer(Color4B(0,0,0,150));

    this->addChild(layer);
    Size winSize = Director::getInstance()->getWinSize();
    Sprite *imgBg   =   Sprite::create("save painting/BG frame.png");

    imgBg->setTag(100);
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);

    Sprite *imgTxt  =   Sprite::create("save painting/text BG.png");
    imgTxt->setTag(101);
    imgTxt->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.55));
    imgBg->addChild(imgTxt);

    Label *lblSongName =   Label::createWithTTF("Name of game","fonts/arial.ttf",30);
    lblSongName->setColor(Color3B::BLACK);
    lblSongName->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.7));
    imgBg->addChild(lblSongName);

    std::string placeholderText = "File Name";

//    GameScene *gameScene = dynamic_cast<GameScene*>(this->getParent()->getParent());
//    if(gameScene->lblTitle->getString()!="UNTITLED"){
//        placeholderText = gameScene->lblTitle->getString();
//    }
    if(GameData::getInstance()->game_type==GAME_TYPE_LOADGAME){
        placeholderText = GameData::getInstance()->load_song_name;
    }else if(GameData::getInstance()->currentSongName!=""){
        placeholderText = GameData::getInstance()->currentSongName;
    }


    ui::TextField * txtName =   ui::TextField::create(placeholderText.c_str(),"fonts/arial.ttf",30);
    txtName->setPosition(Vec2(imgTxt->getContentSize().width/2,imgTxt->getContentSize().height*0.5));
    txtName->setTag(102);

    txtName->setColor(Color3B(10,20,10));
    txtName->setTextHorizontalAlignment(TextHAlignment::LEFT);
    txtName->setTextVerticalAlignment(TextVAlignment::CENTER);
    txtName->setTouchAreaEnabled(true);
    txtName->setTouchSize(Size(imgTxt->getContentSize().width, imgTxt->getContentSize().height));
    imgTxt->addChild(txtName);



    Label *lblTitle =   Label::createWithTTF("Save Game","fonts/arial.ttf",40);
    lblTitle->setColor(Color3B::BLACK);
    lblTitle->setPosition(Vec2(imgBg->getContentSize().width/2,imgBg->getContentSize().height*0.9));
    imgBg->addChild(lblTitle);

    btnBack =   CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(UISavePainting::backCall,this));
    btnBack->setPosition(Vec2(imgBg->getContentSize().width*0.1,imgBg->getContentSize().height*0.9));



    btnSave =   CustomMenuItemImage::create("save painting/save.png","save painting/save.png",CC_CALLBACK_0(UISavePainting::savePaintingCall,this));
    btnSave->setPosition(Vec2(imgBg->getContentSize().width*0.5,imgBg->getContentSize().height*0.3));



    Menu *menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnSave);
    menu->addChild(btnBack);

    imgBg->addChild(menu);






}


void UISavePainting::backCall() {

}


void UISavePainting::savePaintingCall() {


}


//void UISavePainting::rewriteCancelCall() {
//
//}
//
//void UISavePainting::rewriteYesCall() {
//
//}

//void UISavePainting::rewritePopup() {
//
//
//    layerRewrite = TouchableLayer::createLayer(Color4B(0,0,0,150));
//
//    this->addChild(layerRewrite);
//
//    Sprite *imgPopupBG = Sprite::create("popup/box 25 BG.png");
//    imgPopupBG->setPosition(Vec2(layerRewrite->getContentSize().width/2,layerRewrite->getContentSize().height/2));
//    layerRewrite->addChild(imgPopupBG);
//
//    Label *lblTitle = Label::createWithTTF("REWRITE?","fonts/arial.ttf",40);
//    lblTitle->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.7));
//    imgPopupBG->addChild(lblTitle);
//
//
//
//    Label *lblMsg = Label::createWithTTF("It will replace your current song press yes to continue...","fonts/arial.ttf",40,Size(imgPopupBG->getContentSize().width*0.7,imgPopupBG->getContentSize().height*0.4),TextHAlignment::CENTER,TextVAlignment::CENTER);
//    lblMsg->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.5));
//    imgPopupBG->addChild(lblMsg);
//
//
//
//    btnRewriteCancel = MenuItemImage::create("popup/cancel.png","popup/cancel.png",CC_CALLBACK_0(UISavePainting::rewriteCancelCall,this));
//    btnRewriteCancel->setPosition(Vec2(imgPopupBG->getContentSize().width*0.3,imgPopupBG->getContentSize().height*0.3));
//
//
//    btnRewriteYes = MenuItemImage::create("popup/yes.png","popup/yes.png",CC_CALLBACK_0(UISavePainting::rewriteYesCall,this));
//    btnRewriteYes->setPosition(Vec2(imgPopupBG->getContentSize().width*0.3,imgPopupBG->getContentSize().height*0.3));
//
//
//    Menu *menu  = Menu::create();
//    menu->addChild(btnRewriteYes);
//    menu->addChild(btnRewriteCancel);
//
//
//
//
//
//
//
//}