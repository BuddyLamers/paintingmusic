//
//

#include "SoundController.h"

#include "SoundController.h"
#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;

int soundId[25];
bool    SoundController::isEffectOn;
bool    SoundController::isBackgroundOn;
bool    SoundController::bSound;
bool    SoundController::eSound;

int SoundController::background_id = -1;

//SimpleAudioEngine *SoundController::audioEngine = SimpleAudioEngine::getInstance();

SoundController::SoundController() {

    //audioEngine	=	audioEngine;
}

SoundController::~SoundController() {
    // TODO Auto-generated destructor stub
}

void SoundController::setAutoSelectSound() {

    log("soundddddddddddddddddddddd playyyy");
    UserDefault *defaultSound = UserDefault::getInstance();
    bSound = defaultSound->getBoolForKey(EFFECT_SOUND_KEY, true);
    eSound = defaultSound->getBoolForKey(BACKGROUND_SOUND_KEY, true);

    if (bSound) {
        isEffectOn = true;
    } else {
        isEffectOn = false;
    }
    if (eSound) {
        isBackgroundOn = true;
    } else {
        isBackgroundOn = false;
    }
}


//method to play effect sound
void SoundController::playEffectSound(int sound) {
//    UserDefault *defaultSound = UserDefault::getInstance();
//    defaultSound->setBoolForKey(EFFECT_SOUND_KEY, true);
//    audioEngine = SimpleAudioEngine::getInstance();
//    log("sound effect play 00");
//    if (SoundController::isEffectOn) {

        log("%s", String::createWithFormat("sound effect play----%d", sound)->getCString());
        switch (sound) {
            case SOUND_BTN_CLICK: {
               AudioEngine::play2d("sound/button_click.mp3", false, 0.5f, nullptr);
                break;
            }

        }
//    }
}


//method to play background music
void SoundController::playBackGroundMusic(int sound) {



    SoundController::isBackgroundOn = UserDefault::getInstance()->getBoolForKey(
            BACKGROUND_SOUND_KEY, true);
    AudioEngine::stopAll();
    if (SoundController::isBackgroundOn) {
        if (background_id != -1) {
            AudioEngine::stop(background_id);
        }
        background_id = AudioEngine::play2d(SOUND_HAVEN, false, 0.5f, nullptr);
    }
}

//method to stop background music
void SoundController::stopBackGroundMusic() {
    AudioEngine::stop(background_id);
}

//method to stop Effect sound
void SoundController::stopEffectSound(int sound) {
    AudioEngine::stop(sound);
}


//method to stop all Effect sound
void SoundController::stopAllEffectSound() {
    AudioEngine::stopAll();
}


// void SoundController::setAutoSelectSound(){
//
// UserDefault *defaultSound=UserDefault::getInstance();
//  bSound=defaultSound->getBoolForKey(EFFECT_SOUND_KEY,true);
//eSound=defaultSound->getBoolForKey(BACKGROUND_SOUND_KEY,true);
//
// if(bSound){
//     isEffectOn          =       true;
// }else{
//     isEffectOn          =       false;
// }
// if(eSound){
//     isBackgroundOn      =       true;
// }else{
//     isBackgroundOn      =       false;
// }
//}



void SoundController::resumeAllEffectSound() {
//    audioEngine = SimpleAudioEngine::getInstance();
//    audioEngine->resumeAllEffects();
}


void SoundController::pauseAllEffectSound() {
//    audioEngine = SimpleAudioEngine::getInstance();
//    audioEngine->pauseAllEffects();
}


void SoundController::pauseBackgroundMusic() {
    AudioEngine::stop(background_id);
}

void SoundController::resumeBackgroundMusic() {
//    audioEngine = SimpleAudioEngine::getInstance();
//    audioEngine->resumeBackgroundMusic();



    AudioEngine::preload("/storage/emulated/0/sound-1773790299.mp3");

}


void SoundController::setBackgroundMusicVolumn(float soundVolumn) {
//    audioEngine = SimpleAudioEngine::getInstance();
//    audioEngine->setBackgroundMusicVolume(soundVolumn);
}

void SoundController::playSoundEffectByName(std::string fileName) {
    SoundController::isEffectOn =  UserDefault::getInstance()->getBoolForKey(EFFECT_SOUND_KEY, true);

    if (SoundController::isEffectOn) {
        log("sound on");
        AudioEngine::play2d(fileName.c_str(), false, 1.0f, nullptr);




    }else{
        log("sound off");
    }
}




int SoundController::playPianoKey(int keyType, int keyId,int octiveType,int keyNumber) {

log("keyType %d keyId %d octive Id %d  keyNumber %d",keyType,keyId,octiveType,keyNumber);
    std::string soundName = "sound/piano";


    switch(keyNumber){
        case 0:
            soundName   =   soundName+"/a1";
            break;
        case 1:
            soundName   =   soundName+"/#a1";
            break;
        case 2:
            soundName   =   soundName+"/b1";
            break;
        case 3:
            soundName   =   soundName+"/#b1";
            break;
        case 4:
            soundName   =   soundName+"/c1";
            break;
        case 5:
            soundName   =   soundName+"/d1";
            break;
        case 6:
            soundName   =   soundName+"/#c1";
            break;
        case 7:
            soundName   =   soundName+"/e1";
            break;
        case 8:
            soundName   =   soundName+"/#d1";
            break;
        case 9:
            soundName   =   soundName+"/f1";
            break;
        case 10:
            soundName   =   soundName+"/#e1";
            break;
        case 11:
            soundName   =   soundName+"/g1";
            break;

        case 12:
            soundName   =   soundName+"/a2";
            break;
        case 13:
            soundName   =   soundName+"/#a2";
            break;
        case 14:
            soundName   =   soundName+"/b2";
            break;
        case 15:
            soundName   =   soundName+"/#b2";
            break;
        case 16:
            soundName   =   soundName+"/c2";
            break;
        case 17:
            soundName   =   soundName+"/d2";
            break;
        case 18:
            soundName   =   soundName+"/#c2";
            break;
        case 19:
            soundName   =   soundName+"/e2";
            break;
        case 20:
            soundName   =   soundName+"/#d2";
            break;
        case 21:
            soundName   =   soundName+"/f2";
            break;
        case 22:
            soundName   =   soundName+"/#e2";
            break;
        case 23:
            soundName   =   soundName+"/g2";
            break;

        case 24:
            soundName   =   soundName+"/a3";
            break;
        case 25:
            soundName   =   soundName+"/#a3";
            break;
        case 26:
            soundName   =   soundName+"/b3";
            break;
        case 27:
            soundName   =   soundName+"/#b3";
            break;
        case 28:
            soundName   =   soundName+"/c3";
            break;
        case 29:
            soundName   =   soundName+"/d3";
            break;
        case 30:
            soundName   =   soundName+"/#c3";
            break;
        case 31:
            soundName   =   soundName+"/e3";
            break;
        case 32:
            soundName   =   soundName+"/#d3";
            break;
        case 33:
            soundName   =   soundName+"/f3";
            break;
        case 34:
            soundName   =   soundName+"/#e3";
            break;
        case 35:
            soundName   =   soundName+"/g3";
            break;
    }



        soundName   =   soundName+".mp3";


    log("sound name----%s",soundName.c_str());
    return AudioEngine::play2d(soundName, false, 1.0f, nullptr);
//    if(rand()%2==0){
//        log("if Call");
        SimpleAudioEngine::getInstance()->playEffect(soundName.c_str(),false,1,1,1);
//    }else{
//        log("if else Call");
//        SimpleAudioEngine::getInstance()->playEffect(soundName.c_str(),false,2,1,1);
//    }


}


void SoundController::stopPianoKey(int soundId) {
    AudioEngine::stop(soundId);
}

int SoundController::playSoundNode(std::string node) {
    std::string soundName = "sound/piano/";
    soundName+=node;
    soundName+=".mp3";
    log("sound name000 %s",soundName.c_str());
   return  AudioEngine::play2d(soundName, false, 1.0f, nullptr);

//    return SimpleAudioEngine::getInstance()->playEffect(soundName.c_str(),false,1,1,1);
}



void SoundController::pauseSound(int soundId) {
    AudioEngine::pause(soundId);
}


void SoundController::resumeSound(int soundId) {
    AudioEngine::resume(soundId);

}

int SoundController::playMusicNode(std::string node,float volum) {
    std::string soundName = "sound/";
    soundName+=node;

    log("sound name %s volum %f",soundName.c_str(),volum);
    return  AudioEngine::play2d(soundName, false, volum, nullptr);
}



