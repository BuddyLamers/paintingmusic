//

//

#ifndef PROJ_ANDROID_STUDIO_ACHIEVEMENT_H
#define PROJ_ANDROID_STUDIO_ACHIEVEMENT_H






#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC_EXT;
USING_NS_CC;
class Achievement : public cocos2d::Layer
{
public:


    bool isTouchable;
    Size winSize = Director::getInstance()->getWinSize();

    MenuItemImage *button;
    Label *level;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();


    void initComponents();

    // a selector callback
    void start();
    void back();
    // implement the "static create()" method manually
    CREATE_FUNC(Achievement);

    void achievementCall(cocos2d::Ref* sender);



    void collectAchievementPopup();

    void addColorBoxTouchEvents(Sprite *item);

};

#endif //PROJ_ANDROID_STUDIO_ACHIEVEMENT_H
