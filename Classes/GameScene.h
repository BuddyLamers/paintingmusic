//

//

#ifndef PROJ_ANDROID_STUDIO_GAMESCENE_H
#define PROJ_ANDROID_STUDIO_GAMESCENE_H

#include "UIGameScene.h"
#include "PianoSong.h"
#include "MusicNodes.h"
class GameScene : public UIGameScene{

public:
    GameScene();
    ~GameScene();



    bool isTileClicked;
    float musicTimer;
    bool isReplicateAudioStart  =   false;
    int replicateAudioId = -1;
    bool isReplicateAudioEnd = true;
    MusicNodes *musicNodes;
    int fillInColorCounter = 0;


    int currentPianoNote  = -1; // use for when tap on keyboard on touch begin and stop on touch end

    int rightNodeCounter;

    Vector<__Integer*> vecReplicateNodesClick;

    // using for check song langth;
    int matchAudioCounter = 0;

    UIPianoColor *startDragColor;

    PianoSong *pianoSong;

    bool isSongPlaying;

   static bool isSongPause;

    bool isBackGroundPlay;
    int soungNodeCounter = 0;


    static Scene* createScene();
     bool init();
    CREATE_FUNC(GameScene);



    void initComponents();
    void createComponents();

    void loadGameData();
    void loadMatchAudio();
    void loadFillInBlanks();
     void recordCall(cocos2d::Ref *sender);
     void pauseCall();
    // for bg music play
     void playMusicCall(cocos2d::Ref * sender);


    void stopMusicCall(cocos2d::Ref *sender);


   void setColorGrid(__Array *arrColorNods);


     void saveCall();
     void backCall();
    void eraseCall();
    void stopCall();

    void deleteCall();

    void createEmptyCell();
    void resetGridItem();

    // when keyboard color change it will reset all grid cell color
    void resetGridColor();


    // for create rest cell
    void restCall();


    void playSong();

    void playUpdate(float dt);

    int getRequireNotePosition(std::string requireNote,std::string currentNote);



    void showMessageDialog(std::string title, std::string msg);

    // auto scroll in tile animation
     void autoMoveTile();


     void valueChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt);

    void backClick();

    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event);

    // when key pressed
    void addSpriteTouchEvents(KeyButton *touchKey, int itemType);


    // type -1 default normal , 0 wrong ,1 right
    void addColorBox(KeyButton *keyBtn , int type = 2);



    void restartLevel();
    void time_update(float dt);


    void addColorBoxTouchEvents(UIPianoColor *pianoColor);

    int getKeyBtnIndex(std::string node);

    void stopAllBoxAnim();

};


#endif //PROJ_ANDROID_STUDIO_GAMESCENE_H
