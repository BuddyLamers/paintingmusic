//
// Created by assertinfotech on 3/12/17.
//

#ifndef PROJ_ANDROID_STUDIO_SAVESONG_H
#define PROJ_ANDROID_STUDIO_SAVESONG_H

#include "UISavePainting.h"
class SaveSong  : public UISavePainting{

    public:

    SaveSong();
    ~SaveSong();
    virtual bool init();
    CREATE_FUNC(SaveSong);

    virtual void backCall();
    virtual void savePaintingCall();
    void startLoading(float dt);
};


#endif //PROJ_ANDROID_STUDIO_SAVESONG_H
