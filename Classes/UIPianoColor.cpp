//

//

#include "UIPianoColor.h"
#include "GameData.h"
#include "GameScene.h"
#include "CustomMenuItemImage.h"

UIPianoColor::UIPianoColor() {

}


UIPianoColor::~UIPianoColor() {

}


bool UIPianoColor::init() {

    if(!LayerColor::initWithColor(Color4B(0,0,0,0))){
        return false;
    }



    float scaleX = UserDefault::getInstance()->getFloatForKey(KEY_GRID_WIDTH);
    float scaleY = UserDefault::getInstance()->getFloatForKey(KEY_GRID_HEIGHT);



    isNoFillColor = false;
    isEmptyCell = false;
    extraSize = 1;
    noteLength = 1;
    w=25+scaleX;
    h=25+scaleY;
    animationTime   =   0.1;
    r = g = b = r1 = g1 = b1 = r2 = g2 = b2 = 0;
    this->setContentSize(Size(w,h));

    selectLayer = LayerColor::create(Color4B(255,255,255,100));
    selectLayer->setContentSize(Size(w,h));
    this->addChild(selectLayer,100);
    selectLayer->setOpacity(0);


    leftLayer  = LayerColor::create(Color4B(0,0,0,0));
    leftLayer->setContentSize(Size(this->getContentSize().width,this->getContentSize().height));
    leftLayer->setPosition(Vec2(0,0));

    imgLeft = Sprite::create("game screen/box_left.png");
    imgLeft->setContentSize(leftLayer->getContentSize());
    imgLeft->setPosition(Vec2(leftLayer->getContentSize().width*0.51,leftLayer->getContentSize().height/2));
    leftLayer->addChild(imgLeft);



    this->addChild(leftLayer);



    rightLayer  = LayerColor::create(Color4B(0,0,0,0));
    rightLayer->setContentSize(Size(this->getContentSize().width,this->getContentSize().height));
    rightLayer->setPosition(Vec2(0,0));
    this->addChild(rightLayer);
    imgRight = Sprite::create("game screen/box_right.png");

    imgRight->setContentSize(rightLayer->getContentSize());
    imgRight->setPosition(Vec2(rightLayer->getContentSize().width/2,rightLayer->getContentSize().height/2));
    rightLayer->addChild(imgRight);


    layerAnim   =   LayerColor::create(Color4B(255,255,255,100));
    layerAnim->setContentSize(Size(w,h));
    layerAnim->setScaleY(0);
    layerAnim->setAnchorPoint(Vec2(0,0));

    this->addChild(layerAnim);



    imgArrow = Sprite::create("game screen/arrow.png");
    imgArrow->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
    this->addChild(imgArrow);

    imgArrow->setOpacity(0);





    arrowRight = Sprite::create("game screen/side arrow.png");
    arrowRight->setPosition(Vec2(this->getContentSize().width*0.7,this->getContentSize().height/2));
    this->addChild(arrowRight);


    arrowLeft = Sprite::create("game screen/side arrow.png");
    arrowLeft->setPosition(Vec2(this->getContentSize().width*0.3,this->getContentSize().height/2));

    arrowLeft->setRotation(180);
    this->addChild(arrowLeft);



    arrowBottom = Sprite::create("game screen/side arrow.png");
    arrowBottom->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height*0.5));
    this->addChild(arrowBottom);
    arrowBottom->setRotation(90);

    arrowTop = Sprite::create("game screen/side arrow.png");
    arrowTop->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height*0.5));
    this->addChild(arrowTop);
    arrowTop->setRotation(270);

    arrowBottom->setVisible(false);
    arrowTop->setVisible(false);
    arrowRight->setVisible(false);
    arrowLeft->setVisible(false);



    outerBox = Scale9Sprite::create("game screen/outer box.png");
//    outerBox->setScale(this->getContentSize().width/outerBox->getContentSize().width,this->getContentSize().height/outerBox->getContentSize().height);

    outerBox->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
    outerBox->setVisible(false);
    this->addChild(outerBox);

    createLoopAnim();

    return true;
}




void UIPianoColor::setCellData() {

    float scaleX = UserDefault::getInstance()->getFloatForKey(KEY_GRID_WIDTH);
    float scaleY = UserDefault::getInstance()->getFloatForKey(KEY_GRID_HEIGHT);

//    w=130-scaleX+extraSize;

//    w=130-scaleX+extraSize;
    float w1 =getSingleCellWidth();


//    w        =       40+80*(100-scaleX)/100.0+extraSize;
    w=  (int)(w1*noteLength+(int)(w1*(noteLength-1)*0.05));
//    w = (w*extraSize)+20;
//    h=130-scaleY;
    h=35+70*(100-scaleY)/100.0;
    animationTime   =   0.1;
    r = g = b = r1 = g1 = b1 = r2 = g2 = b2 = 0;
    this->setContentSize(Size(w,h));


    selectLayer->setContentSize(Size(w,h));
    leftLayer->setContentSize(Size(this->getContentSize().width,this->getContentSize().height));
    leftLayer->setPosition(Vec2(0,0));



    rightLayer->setContentSize(Size(this->getContentSize().width,this->getContentSize().height));
    rightLayer->setPosition(Vec2(0,0));
    layerAnim->setContentSize(Size(w,h));
    layerAnim->setScaleY(0);
    layerAnim->setAnchorPoint(Vec2(0,0));
    imgArrow->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
    imgArrow->setOpacity(0);

    outerBox->setPreferredSize(Size(this->getContentSize().width,this->getContentSize().height));
//    outerBox->setScale(this->getContentSize().width/outerBox->getContentSize().width,this->getContentSize().height/outerBox->getContentSize().height);
    outerBox->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
    outerBox->setVisible(false);
    imgLeft->setContentSize(leftLayer->getContentSize());
    imgLeft->setPosition(Vec2(leftLayer->getContentSize().width*0.51,leftLayer->getContentSize().height/2));
    imgRight->setContentSize(rightLayer->getContentSize());
    imgRight->setPosition(Vec2(rightLayer->getContentSize().width/2,rightLayer->getContentSize().height/2));



}

void UIPianoColor::startAnimation() {


    log("start animation Call");
    if(imgType!= nullptr){
        imgType->removeFromParent();
    }
    log("start animation Call1");
    if(colorType == COLOR_RIGHT){
        imgType = Sprite::create("game screen/right color.png");
        imgType->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
        imgType->setContentSize(Size(this->getContentSize().height*0.5,this->getContentSize().height*0.5));
        this->addChild(imgType);
    }else  if(colorType == COLOR_WRONG){
//        imgType = Sprite::create("game screen/wrong color.png");
//        imgType->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
//        imgType->setContentSize(Size(this->getContentSize().height*0.5,this->getContentSize().height*0.5));
//        this->addChild(imgType);

        arrowTop->setPosition(Vec2(w/2,h*0.7));
        arrowRight->setPosition(Vec2(w*0.7,h/2));
        arrowLeft->setPosition(Vec2(w*0.3,h/2));
        arrowBottom->setPosition(Vec2(w/2,h*0.3));
        if(!isNoFillColor) {

            if(requireLength==-100){
                imgType = Sprite::create("game screen/wrong color.png");
                imgType->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
                imgType->setContentSize(Size(this->getContentSize().height*0.5,this->getContentSize().height*0.5));
                this->addChild(imgType);
            }else {
                if (requireLength > 0) {
                    arrowRight->setVisible(true);
                } else if (requireLength < 0) {
                    arrowLeft->setVisible(true);
                }

                if (requireNotePosition > 0) {
                    arrowTop->setVisible(true);
                } else {
                    arrowBottom->setVisible(true);
                }
            }
        }else{
            imgType = Sprite::create("game screen/wrong color.png");
        imgType->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));
        imgType->setContentSize(Size(this->getContentSize().height*0.5,this->getContentSize().height*0.5));
        this->addChild(imgType);
        }
    }
    MoveTo *move    =   MoveTo::create(animationTime,Vec2(0,0));
    float scaleX = UserDefault::getInstance()->getFloatForKey(KEY_GRID_WIDTH);
    animationTime = 0.1*(noteLength*2);
    ScaleTo *scaleTo    =   ScaleTo::create(animationTime,1.0,1.0);
    CallFunc *callFunc  = CallFunc::create(CC_CALLBACK_0(UIPianoColor::animationOver,this));
    layerAnim->runAction(Sequence::create(scaleTo,callFunc,NULL));
}


void UIPianoColor::animationOver() {
GameScene *gameScene = (GameScene*)this->getParent();
    GameScene::isSongPause = false;
}

void UIPianoColor::resetAnimation() {

    if(imgType!= nullptr){
        imgType->removeFromParent();
        imgType = nullptr;
    }
    arrowBottom->setVisible(false);
    arrowTop->setVisible(false);
    arrowRight->setVisible(false);
    arrowLeft->setVisible(false);
    layerAnim->setScaleY(0);
}

void UIPianoColor::selectBox(bool isSelect) {
    if (isSelect) {
        selectLayer->setOpacity(255);
    }
    else{
        selectLayer->setOpacity(0);

    }
}



void UIPianoColor::setMargeCell(float numOfCells) {
    noteLength      +=      numOfCells;
    extraSize       +=      getSingleCellWidth()*numOfCells*1.05;
    setCellData();
}

float UIPianoColor::getSingleCellWidth() {
    float scaleX    =       UserDefault::getInstance()->getFloatForKey(KEY_GRID_WIDTH);
    float w1        =       36.5+73*(100-scaleX)/100.0;
    return w1;
}

void UIPianoColor::startSelectAnimation() {
    outerBox->setVisible(true);
    imgArrow->setOpacity(255);
}


void UIPianoColor::stopSelectAnimation() {
    imgArrow->setOpacity(0);
    outerBox->setVisible(false);
}

void UIPianoColor::createLoopAnim() {


    ScaleTo *scaleTo1 =  ScaleTo::create(0.5,1.2);
    ScaleTo *scaleTo2 =  ScaleTo::create(0.5,0.8);


    Sequence *sequence = Sequence::create(scaleTo1,scaleTo2,NULL);
    RepeatForever *forever = RepeatForever::create(sequence);
    imgArrow->runAction(forever);


    FadeIn      *fadeIn     =       FadeIn::create(0.5);
    FadeOut     *fadeOut    =       FadeOut::create(0.5);
    Sequence    *fadeSeq    =       Sequence::create(fadeIn,fadeOut,NULL);
    outerBox->runAction(RepeatForever::create(fadeSeq));
}


void UIPianoColor::setCellHeight(float height) {
    h = 30+height;
}

void UIPianoColor::setCellWidth(float width) {

    w=30+width;
}

void UIPianoColor::setEmptyCell() {

    isEmptyCell = true;


}