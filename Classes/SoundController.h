//

//

#ifndef PROJ_ANDROID_STUDIO_SOUNDCONTROLLER_H
#define PROJ_ANDROID_STUDIO_SOUNDCONTROLLER_H






#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;

#define                    SOUND_BACKGROUND_GAMEPLAY        1

// ------------- Effect Sound -------------
#define                    SOUND_BTN_CLICK                    0
#define                    SOUND_Game_Lose                    1
#define                    SOUND_Get_Star                2
#define                    SOUND_Game_Win                    3
#define                    SOUND_GEM_CLICK4                4

#define                 EFFECT_SOUND_KEY                "effect_sound_key"
#define                 BACKGROUND_SOUND_KEY            "background_sound_key"




#define  SOUND_HAVEN                  "sounds/Robobozo1.mp3"
#define SOUND_FIRE                   "sounds/fire.mp3"
#define  SOUND_ANGEL                "sounds/angel sound 1.mp3"
#define  SOUND_SMOKE                 "sounds/Poof Of Smoke Sound Effect.mp3"


static bool soundCall = true;

#include "cocos2d.h"
USING_NS_CC;


class SoundController {
public:






//	 static bool	isSoundOn;		//variable to check for sound
    static bool isEffectOn;     //  Variable to check whether effects are on or not
    static bool isBackgroundOn; //  Variable to check whether background sound is ON or not
    static bool bSound;
    static bool eSound;
    static int background_id;
    SoundController();

    virtual ~SoundController();

//    static SimpleAudioEngine *audioEngine;


    //method to play effect sound
    static void playEffectSound(int fileName);

    //method to play background music
    static void playBackGroundMusic(int sound);

    //method to stop background music
    static void stopBackGroundMusic();

    /** set Sound according to last save */
    static void setAutoSelectSound();

    /** stop a effect sound*/
    static void stopEffectSound(int sound);

    /** stop all effect sound*/
    static void stopAllEffectSound();

    /** pause all sound effects */
    static void pauseAllEffectSound();

    /** resume all sound effects*/
    static void resumeAllEffectSound();

    /** pause Background music*/
    static void pauseBackgroundMusic();

    /** resume Background Music*/
    static void resumeBackgroundMusic();

    /** set Background Music volumn */
    static void setBackgroundMusicVolumn(float soundVolumn);

    /** set sound effect with file name**/
    static void playSoundEffectByName(std::string fileName);


    static int playPianoKey(int keyType,int keyId,int octiveType,int keyNumber);
    static void stopPianoKey(int soundId);

    static int playSoundNode(std::string node);

    static  void pauseSound(int soundId);
    static void resumeSound(int soundId);

    static  int playMusicNode(std::string node,float volum = 1);


};


#endif //PROJ_ANDROID_STUDIO_SOUNDCONTROLLER_H
