//
//

#include "SaveSong.h"

#include <ui/UITextField.h>
#include "GameScene.h"
#include "GameData.h"
#include "TouchableLayer.h"
#include "UIPianoColor.h"
#include "ColorPanel.h"


SaveSong::SaveSong() {

}
SaveSong::~SaveSong() {

}

bool SaveSong::init() {
    if(!UISavePainting::init()){
        return false;
    }


    return true;
}

void SaveSong::backCall() {
    log("virtual back call");
    this->removeFromParent();
}
void SaveSong::savePaintingCall() {
    schedule(schedule_selector(SaveSong::startLoading),0.1);
}

void SaveSong::startLoading(float dt) {
    TouchableLayer * layer = TouchableLayer::createLayer(Color4B(0,0,0,100));
    this->addChild(layer);
    GameScene *gameScene = dynamic_cast<GameScene*>(this->getParent()->getParent());
    if(gameScene == nullptr){
        gameScene = dynamic_cast<GameScene*>(this->getParent());
    }
    std::string str = ((ui::TextField*)this->getChildByTag(100)->getChildByTag(101)->getChildByTag(102))->getString();
    std::string songName = str;
    songName += "$";
    for (int i = 0; i < gameScene->pianoSong->vecSoundNode.size(); i++) {
        songName += gameScene->pianoSong->vecSoundNode.at(i)->getCString();
        songName+=",";
    }
    songName += "$";
    for (int i = 0; i < gameScene->vecpianoColor.size(); i++) {
        UIPianoColor *pianoColor = gameScene->vecpianoColor.at(i);
        String *str = String::createWithFormat("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,:",pianoColor->h,pianoColor->w,pianoColor->r,pianoColor->g,pianoColor->b,pianoColor->r1,pianoColor->g1,pianoColor->b1,pianoColor->r2,pianoColor->g2,pianoColor->b2);
        songName +=  str->getCString();
    }
    GameData::getInstance()->saveSong(str,songName);
    layer->removeFromParent();
    unschedule(schedule_selector(SaveSong::startLoading));
    this->removeFromParent();

}