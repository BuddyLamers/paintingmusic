//

//

#include "UIStore.h"
#include "GameData.h"
#include "Home.h"
#include "UISetting.h"
#include "UIRecordSong.h"
#include "LoadMusic.h"
#include "MenuScreen.h"
#include "SoundController.h"




UIStore::UIStore() {

}

UIStore::~UIStore() {

}

Scene* UIStore::createScene() {

    Scene *scene = Scene::create();
    auto layer = UIStore::create();
    scene->addChild(layer);
    return  scene;

}

bool UIStore::init() {
    if(!Layer::init()){
        return false;
    }


    screenName = "";
    createComponents();
    return  true;
}





void UIStore::createComponents() {



    Sprite *imgBg   =   Sprite::create("store/BG_15.png");
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);




    addHeaderPanel();
    addCenterComponents();



}


void UIStore::addHeaderPanel() {
    LayerColor *layerHeader = LayerColor::create(Color4B::BLACK);
    this->addChild(layerHeader);
    Sprite *imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height-layerHeader->getContentSize().height));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
    layerHeader->addChild(imgHeader);

    Label* lblTitle = Label::createWithTTF("STORE","fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);
    lblTitle->enableOutline(Color4B::BLACK,2);
    lblTitle->setColor(Color3B::BLACK);

    MenuItemImage *btnBack = MenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(UIStore::backCall,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.1,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);


    Sprite *imgHeaderCanopy   =   Sprite::create("store/shop canopy.png");
    imgHeaderCanopy->setPosition(Vec2(winSize.width/2,winSize.height-layerHeader->getContentSize().height-imgHeaderCanopy->getContentSize().height/2));
    this->addChild(imgHeaderCanopy);






}


void UIStore::addCenterComponents() {

    Sprite *imgPage   =   Sprite::create("load song/page.png");
    imgPage->setPosition(Vec2(winSize.width/2,winSize.height*0.42));
    this->addChild(imgPage);
    imgPage->setContentSize(Size(winSize.width,imgPage->getContentSize().height));
    imgPage->setOpacity(0);


    LayerColor *scrollLayer = LayerColor::create(Color4B(10,10,10,0));

    ScrollView *scrollView = ScrollView::create(Size(imgPage->getContentSize().width,imgPage->getContentSize().height*0.88),scrollLayer);
    imgPage->addChild(scrollView);
    scrollView->setPosition(Vec2(imgPage->getContentSize().width*0,imgPage->getContentSize().height*0.06));
    scrollView->setDirection(ScrollView::Direction::VERTICAL);

    int numOfSong = 10;


    log("num of song %d",numOfSong);
    int counter = 0;
    float topMargin = 0;
    for (int i = 0; i < 1; i++) {
        String *songName= String::createWithFormat("SONG_NAME_%d",(i+1));
        std::string name = "SONG NAME";//UserDefault::getInstance()->getStringForKey(songName->getCString());

        log("song name : %s",name.c_str());

        if(name!="") {
            LayerColor *layerBox = LayerColor::create(Color4B(255, 255, 255, 0));
            layerBox->setContentSize(
                    Size(imgPage->getContentSize().width, imgPage->getContentSize().height * 0.16));


            if (i % 2 != 0) {
                Sprite *imgBox = Sprite::create("store/text banner.png");
                layerBox->addChild(imgBox);
                imgBox->setPosition(Vec2(layerBox->getContentSize().width / 2,
                                         layerBox->getContentSize().height / 2));
            }



//            Sprite *imgBottomLine = Sprite::create("load song/line.png");


            Label *lblName = Label::createWithTTF("VIP PURCHASE","fonts/arial.ttf",40,Size(imgPage->getContentSize().width*0.55,imgPage->getContentSize().height*0.15),TextHAlignment::LEFT,TextVAlignment::CENTER);
            lblName->setPosition(Vec2(layerBox->getContentSize().width*0.3,layerBox->getContentSize().height*0.6));
            layerBox->addChild(lblName);
            lblName->enableOutline(Color4B(50,0,0,200),1);
            lblName->setColor(Color3B(50,0,0));




            Label *lblSubTitle = Label::createWithTTF("Unlock all items","fonts/arial.ttf",30,Size(imgPage->getContentSize().width*0.45,imgPage->getContentSize().height*0.15),TextHAlignment::LEFT,TextVAlignment::CENTER);
            lblSubTitle->setPosition(Vec2(layerBox->getContentSize().width*0.25,layerBox->getContentSize().height*0.3));
            layerBox->addChild(lblSubTitle);
            lblSubTitle->setColor(Color3B(50,0,0));



            MenuItemImage *btnListen = MenuItemImage::create("store/listen button.png","store/listen button.png",CC_CALLBACK_1(UIStore::listenSongCall,this));
            btnListen->setPosition(Vec2(layerBox->getContentSize().width*0.7,layerBox->getContentSize().height*0.5));
            btnListen->setTag(counter);



            MenuItemImage *btnBuy = MenuItemImage::create("store/buy button.png","store/buy button.png",CC_CALLBACK_1(UIStore::buySongCall,this));
            btnBuy->setPosition(Vec2(layerBox->getContentSize().width*0.9,layerBox->getContentSize().height*0.5));
            btnBuy->setTag(counter);



            Menu *menu = Menu::create();
            menu->setPosition(Vec2::ZERO);
//            menu->addChild(btnListen);
            menu->addChild(btnBuy);
            layerBox->addChild(menu);

            counter++;


//            imgBottomLine->setPosition(Vec2(layerBox->getContentSize().width/2,layerBox->getContentSize().height*0.01));
//            layerBox->addChild(imgBottomLine);
//            imgBottomLine->setScaleX(0.7);
//
//            Sprite *imgSideLine = Sprite::create("load song/line 02.png");
//            imgSideLine->setPosition(Vec2(layerBox->getContentSize().width*0.55,layerBox->getContentSize().height*0.5));
//            layerBox->addChild(imgSideLine);



            layerBox->setPosition(Vec2(0,topMargin));
            topMargin = topMargin + layerBox->getContentSize().height;
            scrollLayer->addChild(layerBox);



        }


        scrollLayer->setContentSize(Size(scrollView->getContentSize().width,topMargin));

        scrollView->setContentOffset(Vec2(0,-(scrollLayer->getContentSize().height-scrollView->getViewSize().height)),true);

    }



}

void UIStore::backCall() {
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    this->removeFromParent();
}

void UIStore::listenSongCall(cocos2d::Ref *sender) {

}


void UIStore::buySongCall(cocos2d::Ref *sender) {

    SoundController::playEffectSound(SOUND_BTN_CLICK);
    int tag = ((MenuItemImage*)sender)->getTag();
    cunformBuyPopup();

}



void UIStore::homeCall() {
//    Director::getInstance()->replaceScene(Home::createScene());
    SoundController::playEffectSound(SOUND_BTN_CLICK);

    if(screenName== "keyboardSelection"){
        KeyboardSelection *keyboardSelection = (KeyboardSelection*)this->getParent();
        keyboardSelection->initKeyboardSelection();
    } else if(screenName== "recordSong"){
        UIRecordSong *recordSong = (UIRecordSong* )this->getParent();
        recordSong->initComponents();

    }else if(screenName == "loadMusic"){
            LoadMusic * loadMusic = (LoadMusic *)this->getParent();
                loadMusic->createComponents();

    }
    this->removeFromParent();

}


void UIStore::cunformBuyPopup() {
    layerCunform = TouchableLayer::createLayer(Color4B(0,0,0,150));
    this->addChild(layerCunform);

    Sprite *imgBG = Sprite::create("store/page_20_BG.png");
    imgBG->setPosition(Vec2(winSize.width/2,winSize.height/2));
    layerCunform->addChild(imgBG);


    MenuItemImage *btnBack = MenuItemImage::create("achievement/back button.png","achievement/back button.png",CC_CALLBACK_0(UIStore::homeCall,this));
    btnBack->setPosition(Vec2(imgBG->getContentSize().width*0.15,imgBG->getContentSize().height*0.94));
    Menu *menu = Menu::create();
    menu->addChild(btnBack);
    menu->setPosition(Vec2::ZERO);
    imgBG->addChild(menu);



    Label *lblTitle = Label::createWithTTF("VIP PURCHASE","fonts/arial.ttf",35);
    lblTitle->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.94));
    imgBG->addChild(lblTitle);
    lblTitle->setColor(Color3B(50,0,0));




//    Label *lblThank = Label::createWithTTF("THANK YOU","fonts/arial.ttf",70);
//    lblThank->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.40));
//    imgBG->addChild(lblThank);
//    lblThank->enableOutline(Color4B(50,0,0,255),1);
//    lblThank->setColor(Color3B::WHITE);




    Label *lblMusicPack = Label::createWithTTF("Unlock all items","fonts/arial.ttf",45);
    lblMusicPack->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.82));
    imgBG->addChild(lblMusicPack);
    lblMusicPack->setColor(Color3B(50,0,0));


    Label *lblSong = Label::createWithTTF("","fonts/arial.ttf",30);
    lblSong->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.77));
    imgBG->addChild(lblSong);
    lblSong->setColor(Color3B(50,0,0));



    MenuItemImage *btnBuy = MenuItemImage::create("store/BUY for button.png","store/BUY for button.png",CC_CALLBACK_0(UIStore::buyCunform,this));
    btnBuy->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.7));
    menu->addChild(btnBuy);




    MenuItemImage *btnCancle = MenuItemImage::create("store/cancel.png","store/cancel.png",CC_CALLBACK_0(Node::removeFromParent,layerCunform));
    btnCancle->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.55));
    menu->addChild(btnCancle);



//    Label *lblForPurchase = Label::createWithTTF("FOR PURCHASING","fonts/arial.ttf",30);
//    lblForPurchase->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.35));
//    imgBG->addChild(lblForPurchase);
//    lblForPurchase->setColor(Color3B(50,0,0));



}



void UIStore::buySuccessPopup() {

    TouchableLayer *layer = TouchableLayer::createLayer(Color4B(0,0,0,150));
    this->addChild(layer);

    Sprite *imgBG = Sprite::create("store/page_21_BG.png");
    imgBG->setPosition(Vec2(winSize.width/2,winSize.height/2));
    layer->addChild(imgBG);


    MenuItemImage *btnBack = MenuItemImage::create("store/back button.png","store/back button.png",CC_CALLBACK_0(UIStore::homeCall,this));
    btnBack->setPosition(Vec2(imgBG->getContentSize().width*0.2,imgBG->getContentSize().height*0.795));
    Menu *menu = Menu::create();
    menu->addChild(btnBack);
    menu->setPosition(Vec2::ZERO);
    imgBG->addChild(menu);

    Sprite *imgProduct = Sprite::create("store/music pack.png");
    imgProduct->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.55));
    imgBG->addChild(imgProduct);

    Label *lblTitle = Label::createWithTTF("VIP PURCHASE","fonts/arial.ttf",35);
    lblTitle->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.795));
    imgBG->addChild(lblTitle);
    lblTitle->setColor(Color3B(50,0,0));

    Label *lblThank = Label::createWithTTF("THANK YOU","fonts/arial.ttf",70);
    lblThank->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.40));
    imgBG->addChild(lblThank);
    lblThank->enableOutline(Color4B(50,0,0,255),1);
    lblThank->setColor(Color3B::WHITE);

    Label *lblForPurchase = Label::createWithTTF("FOR PURCHASING","fonts/arial.ttf",30);
    lblForPurchase->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.33));
    imgBG->addChild(lblForPurchase);
    lblForPurchase->setColor(Color3B(50,0,0));

    Label *lblMusicPack = Label::createWithTTF("Unlock all items","fonts/arial.ttf",45);
    lblMusicPack->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.28));
    imgBG->addChild(lblMusicPack);
    lblMusicPack->setColor(Color3B(50,0,0));

    Label *lblSong = Label::createWithTTF("","fonts/arial.ttf",30);
    lblSong->setPosition(Vec2(imgBG->getContentSize().width/2,imgBG->getContentSize().height*0.25));
    imgBG->addChild(lblSong);
    lblSong->setColor(Color3B(50,0,0));

}



void UIStore::buyCunform() {
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    UserDefault::getInstance()->setBoolForKey(KEY_INAPP_VIP,true);
    layerCunform->removeFromParent();
    buySuccessPopup();

}


