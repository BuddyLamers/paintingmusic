//
//

#ifndef PROJ_ANDROID_STUDIO_UIRECORDINGPANEL_H
#define PROJ_ANDROID_STUDIO_UIRECORDINGPANEL_H

#include "TouchableLayer.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
//#include "cocos-ext.h"
#include "CustomMenuItemImage.h"
//USING_NS_CC_EXT;

USING_NS_CC;
class UIRecordingPanel : public  cocos2d::Layer{

public:
    Size winSize = Director::getInstance()->getWinSize();


    TouchableLayer *popupLayer;
    bool isRecordingStart;
    CustomMenuItemImage *btn2;
    ui::TextField * txtName;
    UIRecordingPanel();
    ~UIRecordingPanel();

    static Scene* createScene();
    virtual  bool init();
    CREATE_FUNC(UIRecordingPanel);

    void initComponents();
    void addHeaderPanel();

    void btnClick(Ref *sender);
    void backCall();

    void update(float dt);


    void savePopup();

    void saveCall();

//    void createMediaplayerFromAssets();


};


#endif //PROJ_ANDROID_STUDIO_UIRECORDINGPANEL_H
