#include "SplashScreen.h"
#include "SimpleAudioEngine.h"
#include "Home.h"
#include "GameData.h"
#include "ColorPanel.h"
USING_NS_CC;


Scene* SplashScreen::createScene()
{
    return SplashScreen::create();
}

// on "init" you need to initialize your instance
bool SplashScreen::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    Size size = Director::getInstance()->getWinSize();

    Sprite *imgBg = Sprite::create("splash/splash screen.jpg");

    imgBg->setPosition(Vec2(size.width/2,size.height/2));
    this->addChild(imgBg);


    firstTimeLoad();
    CallFunc *callFunc = CallFunc::create(CC_CALLBACK_0(SplashScreen::goHome,this));
    Sequence *sequence = Sequence::create(DelayTime::create(0.5),callFunc,NULL);
    this->runAction(sequence);

//    goHome();
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
//    auto closeItem = MenuItemImage::create(
//                                           "CloseNormal.png",
//                                           "CloseSelected.png",
//                                           CC_CALLBACK_1(SplashScreen::menuCloseCallback, this));
//
//    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
//                                origin.y + closeItem->getContentSize().height/2));
//
//    // create menu, it's an autorelease object
//    auto menu = Menu::create(closeItem, NULL);
//    menu->setPosition(Vec2::ZERO);
//    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
//    auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
//
//    // position the label on the center of the screen
//    label->setPosition(Vec2(origin.x + visibleSize.width/2,
//                            origin.y + visibleSize.height - label->getContentSize().height));
//
//    // add the label as a child to this layer
//    this->addChild(label, 1);
//
//    // add "HelloWorld" splash screen"
//    auto sprite = Sprite::create("HelloWorld.png");
//
//    // position the sprite on the center of the screen
//    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
//
//    // add the sprite as a child to this layer
//    this->addChild(sprite, 0);
    
    return true;
}


void SplashScreen::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}


void SplashScreen::goHome()
{
    DelayTime *delay =DelayTime::create(2);
    CallFunc *change	=	CallFunc::create(CC_CALLBACK_0(SplashScreen::change,this));
    Sequence *seq = Sequence::create(delay,change,NULL);
    this->runAction(seq);
}

void SplashScreen::change()
{
    Director::getInstance()->replaceScene(Home::createScene());
}



void SplashScreen::firstTimeLoad() {
        bool isFirstTime    =   UserDefault::getInstance()->getBoolForKey("key_first_time",true);
        if(isFirstTime){
            UserDefault::getInstance()->setBoolForKey("key_first_time",false);
            UserDefault::getInstance()->setFloatForKey(KEY_GRID_WIDTH,50);
            UserDefault::getInstance()->setFloatForKey(KEY_GRID_HEIGHT,50);
            log("first time call");
            for (int i = 0; i < 7; i++) {
                std::string  keyCode =  KEY_COLOR_CODE;
                keyCode = keyCode + String::createWithFormat("%d",i)->getCString();
                UserDefault::getInstance()->setIntegerForKey(keyCode.c_str(),i);
                // for first time custom keyboard color
                std::string customKey = KEY_CUSTOM_KEYBOARD_COLOR;
                customKey  = customKey+String::createWithFormat("%d",i)->getCString();
                UserDefault::getInstance()->setIntegerForKey(customKey.c_str(),28+i);
            }
            UserDefault::getInstance()->setIntegerForKey(KEY_COLOR_CODE_REST,0);
        }
    }