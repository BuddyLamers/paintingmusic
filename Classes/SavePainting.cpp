//

//

#include <ui/UITextField.h>
#include "SavePainting.h"
#include "GameScene.h"
#include "GameData.h"
#include "TouchableLayer.h"
#include "UIPianoColor.h"
#include "ColorPanel.h"
#include "SoundController.h"


SavePainting::SavePainting() {

}
SavePainting::~SavePainting() {

}

    bool SavePainting::init() {
        if(!UISavePainting::init()){
            return false;
        }


        return true;
    }



void SavePainting::backCall() {
    log("virtual back call");
    this->removeFromParent();
}


void SavePainting::savePaintingCall() {

    int n = UserDefault::getInstance()->getIntegerForKey(KEY_NUM_OF_RECORDING,0);

//    GameScene *gameScene = (GameScene*)this->getParent();
//
//    log("title name %s",gameScene->lblTitle->getString().c_str());


    std::string str = ((ui::TextField*)this->getChildByTag(100)->getChildByTag(101)->getChildByTag(102))->getString();

    int n1 = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
    for (int i = 0; i < n1; i++) {
        String *sName= String::createWithFormat("SONG_NAME_%d",(i+1));
        std::string name = UserDefault::getInstance()->getStringForKey(sName->getCString());

        if(str==name){
            showRewritePopup();
            return;
        }
    }









          schedule(schedule_selector(SavePainting::startLoading),0.1);



}

void SavePainting::startLoading(float dt) {
    TouchableLayer * layer = TouchableLayer::createLayer(Color4B(0,0,0,100));
    this->addChild(layer);

    GameScene *gameScene = dynamic_cast<GameScene*>(this->getParent()->getParent());
    if(gameScene == nullptr){
        gameScene = dynamic_cast<GameScene*>(this->getParent());
    }
    std::string str = ((ui::TextField*)this->getChildByTag(100)->getChildByTag(101)->getChildByTag(102))->getString();
    std::string songName = str;
    songName += "$";
    for (int i = 0; i < gameScene->pianoSong->vecSoundNode.size(); i++) {
        songName += gameScene->pianoSong->vecSoundNode.at(i)->getCString();
        songName+=",";
    }

    songName += "$";


    for (int i = 0; i < gameScene->vecpianoColor.size(); i++) {
        UIPianoColor *pianoColor = gameScene->vecpianoColor.at(i);
        int isEmpty = 0;
        if(pianoColor->isEmptyCell){
            isEmpty = 0;
        }else{
            isEmpty = 1;
        }
        songName += String::createWithFormat("%d:%d",pianoColor->noteLength,isEmpty)->getCString();
        songName+= ",";
    }

    int selectedKeyBoard = UserDefault::getInstance()->getIntegerForKey(KEY_KEYBOARD_SELECTE,0);
    songName+="$";
    songName += Value(selectedKeyBoard).asString();

    log("songName str %s ",songName.c_str());
    Sprite *imgPopupBG = Sprite::create("popup/box 25 BG.png");
    Label *lblMsg = Label::createWithTTF("SAVE SUCCESSFULLY","fonts/arial.ttf",35,Size(imgPopupBG->getContentSize().width*0.75,imgPopupBG->getContentSize().height*0.4),TextHAlignment::CENTER,TextVAlignment::CENTER);
    lblMsg->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.55));


    int n = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
    for (int i = 0; i < n; i++) {
        String *sName= String::createWithFormat("SONG_NAME_%d",(i+1));
        std::string name = UserDefault::getInstance()->getStringForKey(sName->getCString());
        log("song name %s name %s",name.c_str(),str.c_str());
        if(str == name){
            lblMsg->setString("Save file updated");
            break;
        }

    }
    bool isSave = GameData::getInstance()->saveSong(str,songName);
    if(isSave){


        TouchableLayer *layerpopup = TouchableLayer::createLayer(Color4B(0,0,0,100));

        imgPopupBG->setPosition(Vec2(layerpopup->getContentSize().width/2,layerpopup->getContentSize().height/2));
        layerpopup->addChild(imgPopupBG);

        Label *lblTitle = Label::createWithTTF(str.c_str(),"fonts/arial.ttf",40);
        lblTitle->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.75));
        imgPopupBG->addChild(lblTitle);

        lblTitle->setColor(Color3B::BLACK);




        imgPopupBG->addChild(lblMsg);
        lblMsg->setColor(Color3B::BLACK);


        CustomMenuItemImage *btnOkay  = CustomMenuItemImage::create("popup/cancel.png","popup/cancel.png",CC_CALLBACK_0(SavePainting::okayCall,this));
        btnOkay->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.2));

        Label *lblCancel = Label::createWithTTF("Okay","fonts/arial.ttf",30);
        lblCancel->setPosition(Vec2(btnOkay->getContentSize().width/2,btnOkay->getContentSize().height/2));
        btnOkay->addChild(lblCancel);

        Menu *menu  = Menu::create();
        menu->setPosition(Vec2::ZERO);
        menu->addChild(btnOkay);
        imgPopupBG->addChild(menu);

//


        GameData::getInstance()->currentSongName = str;
        gameScene->lblTitle->setString(str);

        this->addChild(layerpopup);
    }else{
           this->removeFromParent();
    }
    log("virtual call-----------%s ",str.c_str());


//    layer->removeFromParent();

    unschedule(schedule_selector(SavePainting::startLoading));


}




void SavePainting::okayCall() {

//    layer->removeFromParent();

//    unschedule(schedule_selector(SavePainting::startLoading));
    this->removeFromParent();
}

void SavePainting::showRewritePopup() {
    layerRewrite = TouchableLayer::createLayer(Color4B(0,0,0,150));

    this->addChild(layerRewrite);

    Sprite *imgPopupBG = Sprite::create("popup/box 25 BG.png");
    imgPopupBG->setPosition(Vec2(layerRewrite->getContentSize().width/2,layerRewrite->getContentSize().height/2));
    layerRewrite->addChild(imgPopupBG);

    Label *lblTitle = Label::createWithTTF("REWRITE?","fonts/arial.ttf",40);
    lblTitle->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.75));
    imgPopupBG->addChild(lblTitle);

    lblTitle->setColor(Color3B::BLACK);


    Label *lblMsg = Label::createWithTTF("It will replace your current song press yes to continue...","fonts/arial.ttf",35,Size(imgPopupBG->getContentSize().width*0.75,imgPopupBG->getContentSize().height*0.4),TextHAlignment::CENTER,TextVAlignment::CENTER);
    lblMsg->setPosition(Vec2(imgPopupBG->getContentSize().width*0.5,imgPopupBG->getContentSize().height*0.55));
    imgPopupBG->addChild(lblMsg);
    lblMsg->setColor(Color3B::BLACK);


    btnRewriteCancel = CustomMenuItemImage::create("popup/cancel.png","popup/cancel.png",CC_CALLBACK_0(SavePainting::rewriteCancel,this));
    btnRewriteCancel->setPosition(Vec2(imgPopupBG->getContentSize().width*0.3,imgPopupBG->getContentSize().height*0.2));

    Label *lblCancel = Label::createWithTTF("CANCEL","fonts/arial.ttf",30);
    lblCancel->setPosition(Vec2(btnRewriteCancel->getContentSize().width/2,btnRewriteCancel->getContentSize().height/2));
    btnRewriteCancel->addChild(lblCancel);

    btnRewriteYes = CustomMenuItemImage::create("popup/yes.png","popup/yes.png",CC_CALLBACK_0(SavePainting::rewriteYes,this));
    btnRewriteYes->setPosition(Vec2(imgPopupBG->getContentSize().width*0.7,imgPopupBG->getContentSize().height*0.2));
    lblCancel->setColor(Color3B::BLACK);

    Label *lblYes = Label::createWithTTF("YES","fonts/arial.ttf",30);
    lblYes->setPosition(Vec2(btnRewriteYes->getContentSize().width/2,btnRewriteYes->getContentSize().height/2));
    btnRewriteYes->addChild(lblYes);
    lblYes->setColor(Color3B::BLACK);

    Menu *menu  = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnRewriteYes);
    menu->addChild(btnRewriteCancel);
    imgPopupBG->addChild(menu);
}

void SavePainting::rewriteYes() {

    SoundController::playEffectSound(SOUND_BTN_CLICK);
    schedule(schedule_selector(SavePainting::startLoading),0.1);

}


void SavePainting::rewriteCancel() {
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    layerRewrite->removeFromParent();
}