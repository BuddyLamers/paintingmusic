//

//

#ifndef PROJ_ANDROID_STUDIO_LOADPAINTINGSCENE_H
#define PROJ_ANDROID_STUDIO_LOADPAINTINGSCENE_H


#include "cocos2d.h"
/*#include "TouchableLayer.h"*/
class LoadPaintingScene: public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    void back();
    void fun1();
    void fun2();
    void fun3();
    void fun5();
    void fun6();
    void fun7();

    // a selector callback
    //void menuCloseCallback(cocos2d::Ref* pSender);



    // implement the "static create()" method manually
    CREATE_FUNC(LoadPaintingScene);
};

#endif //PROJ_ANDROID_STUDIO_LOADPAINTINGSCENE_H
