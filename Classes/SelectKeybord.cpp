//

//

#include "SelectKeybord.h"
#include "MenuScreen.h"
#include "SoundController.h"
USING_NS_CC;

Scene* SelectKeybord::createScene()

{
    // 'scene' is an autorelease object
    auto scene = Scene::create();

    // 'layer' is an autorelease object
    auto layer = SelectKeybord::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SelectKeybord::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    int noOfLevels	=	10;
    int rowHeight =55;
    float scrollSize=(float)(noOfLevels);
    if(noOfLevels/1)
    {
        scrollSize=scrollSize+.5;
    }


    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    //Size scrollFrameSize= Size(visibleSize.width ,visibleSize.height*.8 );


    Layer	*layerScroll	=	LayerColor::create(Color4B(0,0,0,150));

    auto    scrollView	=	ScrollView::create(Size(visibleSize.width, visibleSize.height*.9),layerScroll);
    //	scrollView->setContainer(scrollLayer);
    scrollView->setDirection(ScrollView::Direction::VERTICAL);
    //scrollView->setContentSize(Size(scrollFrameSize));
    //	scrollView->setEnablePaging(false);

    this->addChild(scrollView);
    scrollView->setPosition(Vec2(origin.x+0,origin.y ));

    //scrollView->setContentSize(Size(scrollView->getContentSize().width,scrollView->getContentSize().height*scrollSize));
    scrollView->setContentSize(Size(visibleSize.width,  scrollSize*rowHeight));
    /*auto level = Sprite::create("box.png");

                    level->setPosition(Vec2(origin.x+ level->getContentSize().width/2+50,origin.y+ visibleSize.height/1.5));
                    level->setScale(.8);
                  this->addChild(level, 0);
 */
    auto label = Label::createWithTTF("Select Keybord Color", "fonts/Marker Felt.ttf", 15);

    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    this->addChild(label);


    int a=0;
    int i=0;
    for( j=1;j<=noOfLevels;j++)
    {
        auto buttons=Array::createWithCapacity(3);
        auto levels=Array::createWithCapacity(3);
        //	log("Araay");
        buttons->retain();
        levels->retain();
        //log("Araay3");
        //	for(int i=0;i<3;i++)
        //{
        //log("Araay2");
        //sprite=Sprite::create("box.png");
        auto button = CustomMenuItemImage::create(
                "tutorial_popup.png",
                "tutorial_popup.png",
                CC_CALLBACK_0(SelectKeybord::start, this));
       // level=Label::createWithTTF("","fonts/Marker Felt.ttf",40);
        //button->setPosition(Vec2(origin.x+visibleSize.width/6+i*150,origin.y + visibleSize.height/1.2+a));
        button->setPosition(Vec2(layerScroll->getContentSize().width/2,layerScroll->getContentSize().height*.95 -(button->getContentSize().height/2)+a));
        //const char *con =String::createWithFormat("%d ",count)->getCString();
      //  level->setString(con);
       // level->setPosition(Vec2(button->getContentSize().width/2,button->getContentSize().height*.75));
        //button->setScale(.7);
        layerScroll->addChild(button);
        buttons->addObject(button);
       // button->addChild(level);
        auto menu = Menu::create(button, NULL);
        menu->setPosition(Vec2::ZERO);
        layerScroll->addChild(menu, 1);
        count++;
        i++;



        //}

            a-=50;


            i=0;


    }
    /*const char *key  =   "key_Star";

 int star=		UserDefault::getInstance()->getIntegerForKey(key,0);



 if(star==0)
{

for(int k=0;k<3;k++)
{

            auto spriteStar=Sprite::create("star_gray.png");
            spriteStar->setPosition(Vec2(button->getContentSize().width/4 +(k*25),button->getContentSize().height*.2));
            spriteStar->setScale(.5);
            button->addChild(spriteStar);

}
}
 if(star!=0)
 {
     for(int k=0;k<star;k++)
            {
            auto spriteStar=Sprite::create("star_yellow.png");
            spriteStar->setPosition(Vec2(button->getContentSize().width/4 +(k*25),button->getContentSize().height*.2));
            spriteStar->setScale(.5);
            button->addChild(spriteStar);

            }
     for(int k=star;k<3;k++)

                         {
                         auto spriteStar=Sprite::create("star_grey.png");
                         spriteStar->setPosition(Vec2(button->getContentSize().width/4 +(k*25),button->getContentSize().height*.2));
                         spriteStar->setScale(.5);
                         button->addChild(spriteStar);

                         }

 }
*/
    auto back = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(SelectKeybord::back, this));

    back->setPosition(Vec2(origin.x + visibleSize.width*.1  ,
                           origin.y + visibleSize.height- back->getContentSize().height));
    auto menu = Menu::create(back, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    return true;
}

void SelectKeybord::start()
{
   // Director::getInstance()->replaceScene(GamePlay::createScene());
}
void SelectKeybord::back()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
   Director::getInstance()->replaceScene(MenuScreen::createScene());
}