/*
 * CustomMenuItemImage.h
 *
 *  Created on: 09-Aug-2014
 *      Author: Assert InfoTech
 */

#ifndef CUSTOMMENUITEMIMAGE_H_
#define CUSTOMMENUITEMIMAGE_H_
#include "cocos2d.h"

USING_NS_CC;

class CustomMenuItemImage : public MenuItemImage {

	float  		scaleVal;
	bool		isAutoSelectionEnabled;
	bool		isPressAnimationEnabled;
public:
	Label	*genericLabel;
	CustomMenuItemImage();
	virtual ~CustomMenuItemImage();

	static CustomMenuItemImage* create(const char *normalImage, const char *selectedImage, const ccMenuCallback& callback);

	/** initializes a menu item with a normal, selected  and disabled image with target/selector */
	bool initWithNormalImage(const char *normalImage, const char *selectedImage,const ccMenuCallback& callback);

	// Method to set value whether the selection of menu item is handled automatically or handled by user
	void setAutoSelectionEnabled(bool selection);

	// Method to select and de-select menu Item manually
	void setSelected(bool selection);

	// Method to enable and disable press animation
	void setPressAnimationEnabled(bool isEnabled);

	//add label on menu item image
	void addLabel(const char *string,const char *fontName,float fontSize,const Color4B &color4=Color4B(0, 0, 0,255));

	//add label stroke on menu item image
	void addLabelStroke(const Color4B color);

	//set scale value
	virtual void setScale(float val);

	//super method
	virtual void selected();
	virtual void unselected();
	virtual void activate();
	void onActivate();


};

#endif /* CUSTOMMENUITEMIMAGE_H_ */
