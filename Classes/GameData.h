//

//

#ifndef PROJ_ANDROID_STUDIO_GAMEDATA_H
#define PROJ_ANDROID_STUDIO_GAMEDATA_H


#define KEY_COLOR_CODE      "key_color_code"
#define KEY_KEYBOARD_SELECTE      "key_keyboard_select"
#define KEY_COLOR_CODE_REST      "key_color_code_rest"
#define KEY_NUMBER_OF_SAVE_SONG  "key_number_of_save_song"
#define KEY_GRID_HEIGHT     "key_grid_height"
#define KEY_GRID_WIDTH      "key_grid_width"
#define KEY_RECORD_PATH     "key_record_path"
#define KEY_NUM_OF_RECORDING  "key_num_of_recording"
#define KEY_RECORDING_NAME  "key_recording_name"
#define KEY_CUSTOM_KEYBOARD_COLOR   "key_custon_keyboard_color"
#define KEY_INAPP_VIP       "key_inapp_vip"

#include "cocos2d.h"
#include "PianoSong.h"



enum GAME_TYPE{
    GAME_TYPE_REPLICATE_SONG = 1,
    GAME_TYPE_FILL_IN_COLOR,
    GAME_TYPE_EXPERIMENTAL_PLAY,
    GAME_TYPE_LOADGAME
};

USING_NS_CC;
class GameData {
private:
    GameData();


public:



    int gameMusicID = -1;


    std::string currentSongName;
    std::string load_song_name = "";

    std::string load_recording_name = "";
    std::string load_replicated_song = "";

    int game_type = 0;
//    Vector<PianoSong*> vecSongs;
    Vector<String *> vecSongNames;

    Vector<String *> listAllSong;// first recorded and default song
    static GameData *getInstance();
    void initComponents();

    void loadAllSOngNames();
    void loadAllSong();

    bool saveSong(std::string songName,std::string data);

    std::string readSong(std::string songName);

    std::string getFilePath();


    static void startRecording();
    static void stopRecording(std::string fileName);
    static void playRecording(std::string fileName);

    /**
     * pause audio recording
     */
    static void pauseRecording();
    /**
     * resume audio recording
     */
    static void resumeRecording();


    static  bool isRecordingAudio();
};


#endif //PROJ_ANDROID_STUDIO_GAMEDATA_H
