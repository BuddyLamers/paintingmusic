

#ifndef TOUCHABLELAYER_H_
#define TOUCHABLELAYER_H_

#include "cocos2d.h"

USING_NS_CC;

class TouchableLayer : public LayerColor {
public:
    TouchableLayer();

    virtual ~TouchableLayer();

    static TouchableLayer *createLayer(Color4B color = Color4B(0, 0, 0, 150));

    bool init(Color4B color);

    //Methods to handle touch events
    bool onTouchBegan(Touch *touch, Event *event);

    void onTouchMoved(Touch *touch, Event *event);

    void onTouchEnded(Touch *touch, Event *event);
};

#endif /* TOUCHABLELAYER_H_ */