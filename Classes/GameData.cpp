//

//

#include "GameData.h"
#define COCOS2D_DEBUG 1

#include "cocos2d.h"
#include <stdio.h>

using namespace std;
const char *AppActivityClassName = "org/cocos2dx/cpp/AppActivity";
static  GameData *_gameData = nullptr;
GameData::GameData() {

}

GameData* GameData::getInstance() {


    if(!_gameData){
        _gameData = new GameData();
        _gameData->initComponents();
    }
    return _gameData;

}


void GameData::initComponents() {

    loadAllSOngNames();
}


void GameData::loadAllSong() {
    int numOfSong = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
//    std::string songPath = "piano_song_";
//    for (int i = 0; i < numOfSong; ++i) {
//        String *songPath = String::createWithFormat("piano_song_%d",i);
//
//        std::string path = FileUtils::getInstance()->getWritablePath();
//        file = path + "stagestatus.txt";
//// write
//        FILE *fp = fopen(path.c_str(), "wb");
//        for (int i = 0; i < size; ++i) {
//            fwrite(&stages.at(i), sizeof(StageStat), 1, fp);
//        }
//        fclose(fp);
//// read
//        FILE *fp = fopen(path.c_str(), "rb");
//        for(int i = 0; i < count; i++) {
//            fread(&ss, sizeof(StageStat), 1, fp);
//            stages.push_back(ss);
//        }
//        fclose(fp)
//        return stages;
//    }

}


void GameData::loadAllSOngNames() {
    vecSongNames.clear();
    int numOfSong = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
    for (int i = 0; i < numOfSong; ++i) {
        String *songName= String::createWithFormat("SONG_NAME_%d",i);
        string name = UserDefault::getInstance()->getStringForKey(songName->getCString());
        vecSongNames.pushBack(StringMake(name));
    }
}



bool GameData::saveSong(std::string songName, std::string data) {

    log("save SOng start");


    string path = getFilePath();
    path += songName;
    FILE *fp = fopen(path.c_str(), "w");


    if (! fp)
    {
        CCLOG("can not create file %s", path.c_str());
        MessageBox("can not save song","SAVE ERROR");
        return false;
    }

    fputs(data.c_str(), fp);
    fflush(fp);
    fclose(fp);


    int n = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
    for (int i = 0; i < n; i++) {
        String *sName= String::createWithFormat("SONG_NAME_%d",(i+1));
        std::string name = UserDefault::getInstance()->getStringForKey(sName->getCString());
        log("song name %s name %s",songName.c_str(),name.c_str());
        if(songName == name){
//            MessageBox("Save file updated",songName.c_str());
            return  true;
        }

    }




    int numOfSong = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);
    numOfSong++;
    UserDefault::getInstance()->setIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,numOfSong);
    String *songName1= String::createWithFormat("SONG_NAME_%d",numOfSong);
    UserDefault::getInstance()->setStringForKey(songName1->getCString(),songName);
    string name = UserDefault::getInstance()->getStringForKey(songName1->getCString());
    vecSongNames.pushBack(StringMake(name));

    UserDefault::getInstance()->setStringForKey("recent_song",songName);

    log("save SOng end");

//    MessageBox("SAVE SUCCESSFULLY",songName.c_str());

    return true;
}


std::string GameData::readSong(std::string songName) {

    log("read SOng start");


    PianoSong *pianoSong = new PianoSong();

    string path = getFilePath();

    path += songName;
    FILE *fp = fopen(path.c_str(), "r");
    char buf[500];

    if (! fp)
    {
        CCLOG("can not open file %s", path.c_str());
        return "";
    }

    int n = 100;

    fgets(buf, 500, fp);
    CCLOG("read content %s n %d", buf,n);
    pianoSong->vecSoundNode.pushBack(StringMake(buf));

    fclose(fp);
    log("read SOng end");

    return buf;
}

string GameData::getFilePath()
{
    string path("");

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    // In android, every programe has a director under /data/data.
    // The path is /data/data/ + start activity package name.
    // You can save application specific data here.
    path.append("/data/data/com.pianoPainting/");
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    // You can save file in anywhere if you have the permision.
	path.append("D:/tmpfile");
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WOPHONE)
    path = cocos2d::CCApplication::sharedApplication().getAppDataPath();

#ifdef _TRANZDA_VM_
	// If runs on WoPhone simulator, you should insert "D:/Work7" at the
	// begin. We will fix the bug in no far future.
	path = "D:/Work7" + path;
	path.append("tmpfile");
#endif

#endif

    return path;
}



void GameData::startRecording() {

    CCLog("calling google plus from common");
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "onStartRecording",
                                                "()V")) {

        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}


void GameData::stopRecording(std::string fileName) {
    CCLog("calling google plus from common");
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "onStopRecording",
                                                "(Ljava/lang/String;)Ljava/lang/String;")) {

        jstring StringArg = t.env->NewStringUTF(fileName.c_str());

       jstring  data = (jstring)t.env->CallStaticObjectMethod(t.classID, t.methodID,StringArg);


        const char*  recordPath =      t.env->GetStringUTFChars(data,NULL);
//        t.env->DeleteLocalRef(t.classID);
        UserDefault::getInstance()->setStringForKey(KEY_RECORD_PATH,recordPath);

    }
}

void GameData::playRecording(std::string fileName) {
    CCLog("playRecording--");

    std::string path = UserDefault::getInstance()->getStringForKey(KEY_RECORD_PATH);


    path = path+"/"+fileName+".ogg";

    log("play audio name %s",path.c_str());
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "onPlayRecording",
                                                "(Ljava/lang/String;)V")) {

        jstring StringArg = t.env->NewStringUTF(path.c_str());
        t.env->CallStaticVoidMethod(t.classID, t.methodID,StringArg);
        t.env->DeleteLocalRef(t.classID);
    }
}

void GameData::pauseRecording() {
    CCLog("calling google plus from common");
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "onPauseRecording",
                                                "()V")) {

        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}


void GameData::resumeRecording() {
    CCLog("calling google plus from common");
    cocos2d::JniMethodInfo t;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "onResumeRecording",
                                                "()V")) {

        t.env->CallStaticVoidMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);
    }
}


bool GameData::isRecordingAudio() {

    CCLog("calling google plus from common");
    cocos2d::JniMethodInfo t;
    int result = -1;
    if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "isRecordingAudio",
                                                "()B")) {

        result =   t.env->CallStaticIntMethod(t.classID, t.methodID);
        t.env->DeleteLocalRef(t.classID);

    }
    if(result == 1){
        return  true;
    } else{
        return false;
    }



}


extern "C"
{
//JNIEXPORT void Java_org_cocos2dx_cpp_AppActivity_FunctionName(JNIEnv* env, jobject obj)
//{
//    log(“Function Called”);
//}
JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_AppActivity_saveaudioSuccessfully(JNIEnv * env, jobject obj, jstring fileName);

//
}

JNIEXPORT void JNICALL
Java_org_cocos2dx_cpp_AppActivity_saveaudioSuccessfully(JNIEnv *env, jobject thiz, jstring fileName) {

    log("C++ Function called");
    const char *name;
    name = env->GetStringUTFChars(fileName, 0);
    std::string audioName = StringMake(name)->getCString();

    int numOfRecording = UserDefault::getInstance()->getIntegerForKey(KEY_NUM_OF_RECORDING,0);

    for (int i = 0; i < numOfRecording; ++i) {
        std::string key = KEY_RECORDING_NAME;
        key += String::createWithFormat("%d",numOfRecording)->getCString();

        if(audioName==key){
            return;
        }
    }
    numOfRecording++;


    UserDefault::getInstance()->setIntegerForKey(KEY_NUM_OF_RECORDING,numOfRecording);


    std::string key = KEY_RECORDING_NAME;
                      key += String::createWithFormat("%d",numOfRecording)->getCString();

    log("save recording key  %s",key.c_str());

    UserDefault::getInstance()->setStringForKey(key.c_str(),audioName.c_str());


}


