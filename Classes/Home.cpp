#include "Home.h"
#include "Modes.h"
#include "SimpleAudioEngine.h"
#include "GameScene.h"
#include "UILoadSong.h"
#include "UIStore.h"
#include "Achievement.h"
#include "CustomMenuItemImage.h"

#include "SoundController.h"

#include "GameData.h"

USING_NS_CC;

Scene* Home::createScene()
{
    return Home::create();
}

// on "init" you need to initialize your instance
bool Home::init() {
    //////////////////////////////
    // 1. super init first
    if (!Scene::init()) {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();


    Size size = Director::getInstance()->getWinSize();

    Sprite *imgBg = Sprite::create("home/home bg.jpg");

    imgBg->setPosition(Vec2(size.width/2,size.height/2));
    this->addChild(imgBg);


    Sprite *btnBg = Sprite::create("home/button bg.png");

    btnBg->setPosition(Vec2(size.width/2,size.height/2));
    this->addChild(btnBg);



    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto btn1 = CustomMenuItemImage::create(
            "home/blank button.png",
            "home/blank button.png",
            CC_CALLBACK_0(Home::fun1, this));

    btn1->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.42));

    auto btn2 = CustomMenuItemImage::create(
            "home/blank button.png",
            "home/blank button.png",
            CC_CALLBACK_0(Home::fun2, this));

    btn2->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.3));


    auto btn3 = CustomMenuItemImage::create(
            "home/blank button.png",
            "home/blank button.png",
            CC_CALLBACK_0(Home::fun3, this));

    btn3->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.18));


    auto btn4 = CustomMenuItemImage::create(
            "home/blank button.png",
            "home/blank button.png",
            CC_CALLBACK_0(Home::fun4, this));

    btn4->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.13));


    auto menu = Menu::create(btn1,btn2,btn3,btn4, NULL);
    menu->setPosition(Vec2::ZERO);
    btnBg->addChild(menu);



    auto label1 = Label::createWithTTF("PLAY", "fonts/arial.ttf", 40);
    label1->setPosition(Vec2(btn1->getContentSize().width/2,  btn1->getContentSize().height/2));
    btn1->addChild(label1);

    auto label2 = Label::createWithTTF("LOAD GAME", "fonts/arial.ttf", 40);
    label2->setPosition(Vec2(btn2->getContentSize().width/2,  btn2->getContentSize().height/2));
    btn2->addChild(label2);

    auto label3 = Label::createWithTTF("ACHIEVEMENT", "fonts/arial.ttf", 40);
    label3->setPosition(Vec2(btn3->getContentSize().width/2,  btn3->getContentSize().height/2));
    btn3->addChild(label3);

    auto label4 = Label::createWithTTF("STORE", "fonts/arial.ttf", 40);
    label4->setPosition(Vec2(btn4->getContentSize().width/2,  btn4->getContentSize().height/2));
    btn4->addChild(label4);


//    btn1->setScale(0);
//    btn2->setScale(0);
//    btn3->setScale(0);
//    btn4->setScale(0);
    btn1->setOpacity(0);
    btn2->setOpacity(0);
    btn3->setOpacity(0);
    btn4->setOpacity(0);





    label1->enableGlow(Color4B(10,10,10,100));
    label2->enableGlow(Color4B(10,10,10,100));
    label3->enableGlow(Color4B(10,10,10,100));
    label4->enableGlow(Color4B(10,10,10,100));

    label1->enableOutline(Color4B(10,10,10,100),2);
    label2->enableOutline(Color4B(10,10,10,100),2);
    label3->enableOutline(Color4B(10,10,10,100),2);
    label4->enableOutline(Color4B(10,10,10,100),2);



    ScaleTo *scale1 = ScaleTo::create(0.5,1);
    EaseElasticOut *ease1 = EaseElasticOut::create(scale1,0.8);

    ScaleTo *scale2 = ScaleTo::create(0.8,1);
    EaseElasticOut *ease2 = EaseElasticOut::create(scale2,0.8);

    ScaleTo *scale3 = ScaleTo::create(1.2,1);
    EaseElasticOut *ease3 = EaseElasticOut::create(scale3,0.8);

    ScaleTo *scale4 = ScaleTo::create(1.6,1);
    EaseElasticOut *ease4 = EaseElasticOut::create(scale4,0.8);


    FadeIn *fade1 = FadeIn::create(0.2);
    FadeIn *fade2 = FadeIn::create(0.6);
    FadeIn *fade3 = FadeIn::create(0.9);
    FadeIn *fade4 = FadeIn::create(1.2);





    btn1->runAction(fade1);
    btn2->runAction(fade2);
    btn3->runAction(fade3);
//    btn4->runAction(fade4);



    btn1->runAction(ease1);
    btn2->runAction(ease2);
    btn3->runAction(ease3);
//    btn4->runAction(ease4);


    return true;
}




void Home::fun1()
{

    log("func 1 call----");
//GameScene *gameScene = GameScene::create();
//    this->addChild(gameScene);
    SoundController::playEffectSound(SOUND_BTN_CLICK);

    GameData::getInstance()->game_type = 0;
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5, Modes::createScene());
    Director::getInstance()->replaceScene(effect);

}
void Home::fun2()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    TransitionFadeTR *effect = TransitionFadeTR::create(0.5,UILoadSong::createScene());
    Director::getInstance()->pushScene(effect);
//    GameData::getInstance()->game_type = 1;
//    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,GameScene::createScene());
//    Director::getInstance()->replaceScene(effect);

}
void Home::fun3()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
//    Achievement *achievement  = Achievement::create();
//    this->addChild(achievement);
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,Achievement::createScene());
    Director::getInstance()->pushScene(effect);
}


void Home::fun4()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,UIStore::createScene());
    Director::getInstance()->pushScene(effect);
}
