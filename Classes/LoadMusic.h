//
//

#ifndef PROJ_ANDROID_STUDIO_LOADMUSIC_H
#define PROJ_ANDROID_STUDIO_LOADMUSIC_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CustomMenuItemImage.h"
USING_NS_CC;
USING_NS_CC_EXT;
class LoadMusic : public :: cocos2d::Layer{

public:
    Size winSize = Director::getInstance()->getWinSize();


    Vector<String *> vecSongName;
    Vector<CustomMenuItemImage*> vecListenBtn;
    std::vector<std::string> vecLock;

    int listenAudioID = -1;
    LoadMusic();
    ~LoadMusic();

    virtual bool init();
    static Scene* createScene();

    CREATE_FUNC(LoadMusic);

    void addHeaderPanel();

    void createComponents();

    void addCenterComponents();

    void backCall();

    void loadSongCall(cocos2d::Ref *sender);
    void listenSongCall(cocos2d::Ref *sender);

    void restListenBtn();


    void shopCall();


};


#endif //PROJ_ANDROID_STUDIO_LOADMUSIC_H
