//
//

#include "UILoadSong.h"
#include "GameData.h"
#include "Home.h"
#include "GameScene.h"
#include "CustomMenuItemImage.h"
#include "UISetting.h"


UILoadSong::UILoadSong() {

}

UILoadSong::~UILoadSong() {

}


Scene* UILoadSong::createScene() {

    Scene *scene = Scene::create();
    auto layer = UILoadSong::create();
    scene->addChild(layer);
    return  scene;

}

bool UILoadSong::init() {
    if(!Layer::init()){
        return false;
    }
    createComponents();

    return  true;
}





void UILoadSong::createComponents() {



    Sprite *imgBg   =   Sprite::create("menu screen/main BG.png");
    imgBg->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgBg);


    addCenterComponents();
    addHeaderPanel();


}


void UILoadSong::addHeaderPanel() {


    isSongPlay = false;
    LayerColor *layerHeader = LayerColor::create(Color4B(0,0,0,0));
    this->addChild(layerHeader);
    Sprite *imgHeader = Sprite::create("game screen/headder.png");
//    imgHeader->setOpacity(0);
    layerHeader->setContentSize(Size(winSize.width,imgHeader->getContentSize().height));
    layerHeader->setPosition(Vec2(winSize.width*0.0,winSize.height*0.82));
    imgHeader->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height/2));
//    layerHeader->addChild(imgHeader);

    Label* lblTitle = Label::createWithTTF("LOAD PAINTING","fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(layerHeader->getContentSize().width/2,layerHeader->getContentSize().height*0.5));
    layerHeader->addChild(lblTitle);
    lblTitle->setColor(Color3B(80,0,0));

    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("Back-button.png","Back-button.png",CC_CALLBACK_0(UILoadSong::backCall,this));
    btnBack->setPosition(Vec2(layerHeader->getContentSize().width*0.12,layerHeader->getContentSize().height*0.45));

    Menu * menu = Menu::create();
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnBack);

    layerHeader->addChild(menu);




}


void UILoadSong::addCenterComponents() {

    Sprite *imgPage   =   Sprite::create("load song/BG frame.png");
    imgPage->setPosition(Vec2(winSize.width/2,winSize.height/2));
    this->addChild(imgPage);


    LayerColor *scrollLayer = LayerColor::create(Color4B(10,10,10,0));

    ScrollView *scrollView = ScrollView::create(Size(imgPage->getContentSize().width*0.88,imgPage->getContentSize().height*0.71),scrollLayer);
    imgPage->addChild(scrollView);
    scrollView->setPosition(Vec2(imgPage->getContentSize().width*0.075,imgPage->getContentSize().height*0.1));
    scrollView->setDirection(ScrollView::Direction::VERTICAL);

    int numOfSong = UserDefault::getInstance()->getIntegerForKey(KEY_NUMBER_OF_SAVE_SONG,0);


    log("num of song %d",numOfSong);
    int counter = 0;
    float topMargin = 0;
    for (int i = 0; i < numOfSong; i++) {
        String *songName= String::createWithFormat("SONG_NAME_%d",(i+1));
        std::string name = UserDefault::getInstance()->getStringForKey(songName->getCString());

        log("song name : %s",name.c_str());

        if(name!=""){
            LayerColor *layerBox = LayerColor::create(Color4B(255,255,255,0));

            Sprite *imgBottomLine = Sprite::create("menu screen/menu btn selected.png");

            layerBox->setContentSize(Size(scrollLayer->getContentSize().width,imgPage->getContentSize().height*0.12));

            vecSongName.pushBack(StringMake(name));
            Label *lblName = Label::createWithTTF(name,"fonts/arial.ttf",40,Size(imgPage->getContentSize().width*0.3,imgPage->getContentSize().height*0.15),TextHAlignment::LEFT,TextVAlignment::CENTER);
            lblName->setPosition(Vec2(layerBox->getContentSize().width*0.3,layerBox->getContentSize().height*0.5));

            lblName->setColor(Color3B(50,0,0));

            CustomMenuItemImage *btnListen = CustomMenuItemImage::create("load song/button 02.png","load song/button 02.png",CC_CALLBACK_1(UILoadSong::listenSongCall,this));
            btnListen->setPosition(Vec2(layerBox->getContentSize().width*0.75,layerBox->getContentSize().height*0.72));
            btnListen->setTag(counter);
            vecListenBtn.pushBack(btnListen);

            Label *lblListen = Label::createWithTTF("LISTEN","fonts/arial.ttf",35);
            lblListen->setPosition(Vec2(btnListen->getContentSize().width*0.5,btnListen->getContentSize().height*0.5));
            btnListen->addChild(lblListen);
            lblListen->setTag(101);
            lblListen->setColor(Color3B(100,0,0));

            CustomMenuItemImage *btnLoad = CustomMenuItemImage::create("load song/button 03.png","load song/button 03.png",CC_CALLBACK_1(UILoadSong::loadSongCall,this));
            btnLoad->setPosition(Vec2(layerBox->getContentSize().width*0.75,layerBox->getContentSize().height*0.3));
            btnLoad->setTag(counter);

            Label *lbLoad = Label::createWithTTF("LOAD","fonts/arial.ttf",35);
            lbLoad->setPosition(Vec2(btnLoad->getContentSize().width*0.5,btnLoad->getContentSize().height*0.5));
            btnLoad->addChild(lbLoad);
            lbLoad->setColor(Color3B(100,0,0));

            Menu *menu = Menu::create();
            menu->setPosition(Vec2::ZERO);
            menu->addChild(btnListen);
            menu->addChild(btnLoad);


            counter++;

            imgBottomLine->setScaleX(1.1);
            imgBottomLine->setPosition(Vec2(layerBox->getContentSize().width*0.46,layerBox->getContentSize().height*0.5));
            layerBox->addChild(imgBottomLine);


            Sprite *imgSideLine = Sprite::create("load song/line 02.png");
            imgSideLine->setPosition(Vec2(scrollLayer->getContentSize().width*0.55,layerBox->getContentSize().height*0.5));
//            layerBox->addChild(imgSideLine);



            layerBox->setPosition(Vec2(0,topMargin));
            topMargin = topMargin + layerBox->getContentSize().height;
            scrollLayer->addChild(layerBox);


            layerBox->addChild(lblName);
            layerBox->addChild(menu);

        }


        scrollLayer->setContentSize(Size(scrollView->getContentSize().width,topMargin));

        scrollView->setContentOffset(Vec2(0,-(scrollLayer->getContentSize().height-scrollView->getViewSize().height)),true);

    }



}

void UILoadSong::backCall() {
    this->stopAllActions();
Director::getInstance()->popScene();
}

void UILoadSong::listenSongCall(cocos2d::Ref *sender) {



    pianoSong = new PianoSong();
    Label *lblTitle =  (Label*)((CustomMenuItemImage*)sender)->getChildByTag(101);
    if(lblTitle->getString() == "LISTEN"){
        restListenBtn();
        isSongPlay = true;
        lblTitle->setString("STOP");

        int tag = ((CustomMenuItemImage*)sender)->getTag();

        std::string fileName = vecSongName.at(tag)->getCString();

        std::string data =  GameData::getInstance()->readSong(fileName);

        __Array *arrData = StringMake(data)->componentsSeparatedByString("$");

        String *songName = ((String*)arrData->getObjectAtIndex(0));
        String *soundNods = ((String*)arrData->getObjectAtIndex(1));
        String *colorNods = ((String*)arrData->getObjectAtIndex(2));


        log("marge cel size--- %s",colorNods->getCString());
        __Array *arrSoundNods = soundNods->componentsSeparatedByString(",");
        for (int i = 0; i < arrSoundNods->count(); i++) {
            pianoSong->vecSoundNode.pushBack(((String*)arrSoundNods->getObjectAtIndex(i)));
        }

        __Array *arrColorNods = colorNods->componentsSeparatedByString(",");


        Vector<FiniteTimeAction*> vecAction;
        for (int i = 0; i < pianoSong->vecSoundNode.size(); i++) {
            String *colorCell = ((String*)arrColorNods->getObjectAtIndex(i));

            __Array *arrCellProperty = colorCell->componentsSeparatedByString(":");

            int noteLangth =   atoi(((String*)arrCellProperty->getObjectAtIndex(0))->getCString());
            int isEmpty =  atoi(((String*)arrCellProperty->getObjectAtIndex(1))->getCString());


            float animationTime = 0.1*(noteLangth*2);


            log("sound note name %s",pianoSong->vecSoundNode.at(i)->getCString());
            if(isEmpty==1){
                CallFunc *callFunc = CallFunc::create(CC_CALLBACK_0(SoundController::playSoundNode,pianoSong->vecSoundNode.at(i)->getCString()));
                vecAction.pushBack(callFunc);
            }
            DelayTime *delayTime = DelayTime::create(animationTime);
            DelayTime *delayTime2 = DelayTime::create(0.1);
            vecAction.pushBack(delayTime);
            vecAction.pushBack(delayTime2);


//
        }


        CallFunc *callFunc = CallFunc::create([lblTitle](){
            lblTitle->setString("LISTEN");
        });
        vecAction.pushBack(callFunc);

        Sequence *sequence = Sequence::create(vecAction);
        this->runAction(sequence);


}else {
        restListenBtn();
        isSongPlay = false;
        lblTitle->setString("LISTEN");
        this->stopAllActions();
    }


}



void UILoadSong::restListenBtn() {
    this->stopAllActions();
    for (int i = 0; i < vecListenBtn.size();i++) {
        CustomMenuItemImage *btn = vecListenBtn.at(i);
        Label *lblName  = (Label*) btn->getChildByTag(101);
        lblName->setString("LISTEN");
    }
}




void UILoadSong::loadSongCall(cocos2d::Ref *sender) {

    this->stopAllActions();
    int tag = ((CustomMenuItemImage*)sender)->getTag();

    std::string songName = vecSongName.at(tag)->getCString();
    log("song name %s",songName.c_str());

    GameData::getInstance()->game_type = GAME_TYPE_LOADGAME;
    GameData::getInstance()->load_song_name = songName;


    std::string fileName    =   GameData::getInstance()->load_song_name;//UserDefault::getInstance()->getStringForKey("recent_song","");
    std::string data =  GameData::getInstance()->readSong(fileName);
    __Array *arrData = StringMake(data)->componentsSeparatedByString("$");


    String *soundNods = ((String*)arrData->getObjectAtIndex(1));
    String *colorNods = ((String*)arrData->getObjectAtIndex(2));
    String *keyBoard = ((String*)arrData->getObjectAtIndex(3));

    log("keyboard type %s",keyBoard->getCString());
    int keyBoardSelect = Value(keyBoard->getCString()).asInt();
    KeyboardSelection *keyboardSelection = KeyboardSelection::create();

    keyBoardSelect++;
    keyboardSelection->setKeyboardType(keyBoardSelect);
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5,GameScene::createScene());

    Director::getInstance()->replaceScene(effect);

}