//

//

#ifndef PROJ_ANDROID_STUDIO_SELECTKEYBORD_H
#define PROJ_ANDROID_STUDIO_SELECTKEYBORD_H






#include "cocos2d.h"
#include "cocos-ext.h"
#include "CustomMenuItemImage.h"
USING_NS_CC_EXT;
USING_NS_CC;
class SelectKeybord : public cocos2d::Layer
{
public:
    //Sprite  level[20];
    //Sprite *sprite;
    CustomMenuItemImage *button;
    Label *level;
    int count=1;
    int j;
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // a selector callback
    void start();
    void back();
    // implement the "static create()" method manually
    CREATE_FUNC(SelectKeybord);

};


#endif //PROJ_ANDROID_STUDIO_SELECTKEYBORD_H
