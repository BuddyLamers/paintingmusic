
#include "UIKeyboardLayer.h"
#include "GameData.h"
#include "CustomMenuItemImage.h"


#include "SoundController.h"

UIKeyboardLayer::UIKeyboardLayer() {

}

UIKeyboardLayer::~UIKeyboardLayer() {

}

bool UIKeyboardLayer::init() {
    if(!Layer::init()){
        return  false;
    }
    initComponents();
    return true;
}


void UIKeyboardLayer::initComponents() {

     this->setContentSize(Size(winSize.width,winSize.height*0.22));

//    LayerColor * layerColor = LayerColor::create(Color4B::BLUE);
//    layerColor->setContentSize(Size(winSize.width,winSize.height*0.3));
//    layerColor->setPosition(Vec2(this->getContentSize().width*0.0,this->getContentSize().height-layerColor->getContentSize().height));
//    this->addChild(layerColor);


    imgkeyBoard = Sprite::create("game screen/full  keyboard.png");

    imgkeyBoard->setPosition(Vec2(this->getContentSize().width*0.5,this->getContentSize().height-imgkeyBoard->getContentSize().height/2));
    this->addChild(imgkeyBoard);

//    MenuItemImage *btn = MenuItemImage::create()



    LayerColor *maskLayer = LayerColor::create(Color4B(0,0,0,100));
    maskLayer->setContentSize(imgkeyBoard->getContentSize());
    maskLayer->setAnchorPoint(Vec2(0.5,0.5));
    maskLayer->setIgnoreAnchorPointForPosition(false);
    maskLayer->setPosition(Vec2(imgkeyBoard->getPosition()));
//

//    stencil = DrawNode::create();
//    stencil->setAnchorPoint(Vec2(0.5,0.5));
//    stencil->drawSolidCircle(imgkeyBoard->getPosition(), 50.0f, 90.0f, 4.0f, Color4F::GREEN);


    stencil = Sprite::create("game screen/clipframe.png");


    // create the clipping node and set the stencil
    auto clipper = ClippingNode::create();
    clipper->setStencil(stencil);
    clipper->setInverted(true);

    clipper->addChild(maskLayer);
    this->addChild(clipper);


    layer_SmallKeyboard = Layer::create();
    layer_SmallKeyboard->setContentSize(imgkeyBoard->getContentSize());
    layer_SmallKeyboard->setPosition(Vec2(imgkeyBoard->getPositionX()-imgkeyBoard->getContentSize().width/2,imgkeyBoard->getPositionY()-imgkeyBoard->getContentSize().height/2));
    this->addChild(layer_SmallKeyboard);


    auto Bg_sprite = Sprite::create("game screen/sliderbg.png");
    Bg_sprite->setOpacity(0);
    auto Prog_sprite = Sprite::create("game screen/sliderbg.png");
    Prog_sprite->setOpacity(0);
    Thumb_sprite = Sprite::create("game screen/key viewer 02.png");
    slider1 = ControlSlider::create(Bg_sprite, Prog_sprite, Thumb_sprite);

    slider1->setMinimumValue(0);
    slider1->setMaximumValue(100);
    slider1->addTargetWithActionForControlEvents(this, cccontrol_selector(UIKeyboardLayer::valueChangedCallback), cocos2d::extension::Control::EventType::VALUE_CHANGED);
    addChild(slider1);
    slider1->setPosition(imgkeyBoard->getPosition());



    stencil->setPosition(Thumb_sprite->getParent()->convertToWorldSpace(Thumb_sprite->getPosition()));





    layerScroll	=	LayerColor::create(Color4B(0,0,0,150));

    scrollView	=	ScrollView::create(Size(winSize.width, this->getContentSize().height-imgkeyBoard->getContentSize().height),layerScroll);
    scrollView->setDirection(ScrollView::Direction::HORIZONTAL);

    scrollView->setContentOffset(Vec2(0,0),true);
    this->addChild(scrollView);
    scrollView->setPosition(Vec2(0,0));

//    scrollView->setTouchEnabled(false);

    //scrollView->setContentSize(Size(scrollView->getContentSize().width,scrollView->getContentSize().height*scrollSize));
    scrollView->setContentSize(Size(winSize.width*2, this->getContentSize().height-imgkeyBoard->getContentSize().height/2));

//
    float posX  = 0;
    float posY  =  0;
//    int a[11] = {2,3,4,8,9,10,13,14,18,19,20};
    int temp = -1;
    float scrollWidth = 0;
    for (int i = 0; i < NUMBER_OF_KEYS; i++) {
        log(" before in loop");
        KeyButton *key = KeyButton::create();


        key->setKeyNumber(i);

        if(temp==0 || temp==2 || temp==5 || temp==7 || temp==9 ){
            key->setKeyType(2);
            key->setPosition(Vec2(posX-40,posY));
            key->setLocalZOrder(5);

            if(temp==9){
                temp = -3;
    }
}else{
key->setKeyType(1);
key->setPosition(Vec2(posX,posY));
posX += 80;
}

        temp++;

        layerScroll->addChild(key);

        log("in loop");
        vecKeyButton.pushBack(key);


    }

    log("loop end");






    setKeyboardKeysColor();

    layerScroll->setContentSize(Size(posX+100,scrollView->getContentSize().height));

    // add pause button panel

    Sprite *imgPauseBg =  Sprite::create("game screen/blue yellow banner.png");
    imgPauseBg->setTag(110);
    imgPauseBg->setPosition(Vec2(winSize.width/2,-imgPauseBg->getContentSize().height/2));
    this->addChild(imgPauseBg);



    Sprite *btnBox1 = Sprite::create("game screen/btn box small.png");
    btnBox1->setPosition(Vec2(imgPauseBg->getContentSize().width*0.215,imgPauseBg->getContentSize().height*0.5));
imgPauseBg->addChild(btnBox1);
    btnBox1->setScaleX(0.75);


    Sprite *btnBox2 = Sprite::create("game screen/btn box small.png");
    btnBox2->setPosition(Vec2(imgPauseBg->getContentSize().width*0.51,imgPauseBg->getContentSize().height*0.5));
    imgPauseBg->addChild(btnBox2);
    btnBox2->setScaleX(0.75);


    Sprite *btnBox3 = Sprite::create("game screen/btn box small.png");
    btnBox3->setPosition(Vec2(imgPauseBg->getContentSize().width*0.81,imgPauseBg->getContentSize().height*0.5));
    imgPauseBg->addChild(btnBox3);
    btnBox3->setScaleX(0.75);

    CustomMenuItemImage *btnRest = CustomMenuItemImage::create("game screen/erase.png","game screen/erase press.png",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnRest->setPosition(Vec2(imgPauseBg->getContentSize().width*0.15,imgPauseBg->getContentSize().height*0.5));
    btnRest->setTag(101);

    CustomMenuItemImage *btnPlay = CustomMenuItemImage::create("game screen/play button lower.png","game screen/play button lower.png",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnPlay->setPosition(Vec2(imgPauseBg->getContentSize().width*0.45,imgPauseBg->getContentSize().height*0.5));
    btnPlay->setTag(102);

    CustomMenuItemImage *btnPause = CustomMenuItemImage::create("","",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnPause->setPosition(Vec2(imgPauseBg->getContentSize().width*0.45,imgPauseBg->getContentSize().height*0.5));
    btnPause->setTag(103);

    CustomMenuItemImage *btnStop = CustomMenuItemImage::create("game screen/stop button lower.png","game screen/stop button lower press.png",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnStop->setPosition(Vec2(imgPauseBg->getContentSize().width*0.58,imgPauseBg->getContentSize().height*0.5));
    btnStop->setTag(104);

    CustomMenuItemImage *btnSave = CustomMenuItemImage::create("game screen/save  button lower.png","game screen/save  button lower press.png",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnSave->setPosition(Vec2(imgPauseBg->getContentSize().width*0.75,imgPauseBg->getContentSize().height*0.5));
    btnSave->setTag(105);

    CustomMenuItemImage *btnSetting = CustomMenuItemImage::create("game screen/control panel.png","game screen/control panel press.png",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnSetting->setPosition(Vec2(imgPauseBg->getContentSize().width*0.88,imgPauseBg->getContentSize().height*0.5));
    btnSetting->setTag(106);

    CustomMenuItemImage *btnErase = CustomMenuItemImage::create("game screen/delet icon.png","game screen/delet icon press.png",CC_CALLBACK_0(UIKeyboardLayer::addPianoKey,this));
    btnErase->setPosition(Vec2(imgPauseBg->getContentSize().width*0.28,imgPauseBg->getContentSize().height*0.5));
    btnErase->setTag(107);

    Menu *menuPause = Menu::create();
    menuPause->setPosition(Vec2::ZERO);
    menuPause->addChild(btnPause);
    menuPause->addChild(btnRest);
    menuPause->addChild(btnPlay);
    menuPause->addChild(btnSave);
    menuPause->addChild(btnSetting);
    menuPause->addChild(btnStop);
    menuPause->addChild(btnErase);


    menuPause->setTag(111);
    imgPauseBg->addChild(menuPause);





    log("in loop end");

}




void UIKeyboardLayer::setKeyboardKeysColor(){

    int count = 0;
    for(int i = 0; i<vecKeyButton.size();i++){


        KeyButton *keyButton = vecKeyButton.at(i);
        int keyNum = keyButton->keyNumber % 12;
        int octiveNum = keyButton->keyNumber / 12;

        keyButton->keyOctal = octiveNum;
        keyButton->keyPosition = count;

        if(keyButton->keyType==1) {
            std::string keyCode = KEY_COLOR_CODE;
            keyCode = keyCode + String::createWithFormat("%d", count)->getCString();
            int colorCode = UserDefault::getInstance()->getIntegerForKey(keyCode.c_str(), 0);
            keyButton->setKeyColor(keyNum, octiveNum, colorCode);

            count++;

            if(count==7){
                count=0;
            }
        }

    }


    int keyCounter = 38;
    float leftMargin = imgkeyBoard->getContentSize().width*0.055;
    for(int i = 0;i<vecKeyButton.size();i++){
        KeyButton *keyButton = vecKeyButton.at(i);
        if(keyButton->keyType==1){
            keyButton->imgKeyCenterColor->setColor(Color3B(keyButton->colorPanel->r,keyButton->colorPanel->g,keyButton->colorPanel->b));


            Sprite *imgColorBox = Sprite::create("game screen/center key color.png");
            imgColorBox->setScale(0.4);
            imgColorBox->setTag(100+keyCounter);
            keyCounter++;
            imgColorBox->setPosition(Vec2(leftMargin,layer_SmallKeyboard->getContentSize().height*0.3));
            layer_SmallKeyboard->addChild(imgColorBox);
            leftMargin+=layer_SmallKeyboard->getContentSize().width*0.0445;
            imgColorBox->setColor(Color3B(keyButton->colorPanel->r,keyButton->colorPanel->g,keyButton->colorPanel->b));




        }else if(keyButton->keyType==2){
            KeyButton *keyLeftButton = vecKeyButton.at(i-1);
            keyButton->colorPanel->r1 = keyLeftButton->colorPanel->r;
            keyButton->colorPanel->g1 = keyLeftButton->colorPanel->g;
            keyButton->colorPanel->b1 = keyLeftButton->colorPanel->b;
            keyButton->imgKeyLeftColor->setColor(Color3B(keyLeftButton->colorPanel->r,keyLeftButton->colorPanel->g,keyLeftButton->colorPanel->b));
            KeyButton *keyRightButton = vecKeyButton.at(i+1);

            keyButton->colorPanel->r2 = keyRightButton->colorPanel->r;
            keyButton->colorPanel->g2 = keyRightButton->colorPanel->g;
            keyButton->colorPanel->b2 = keyRightButton->colorPanel->b;

            keyButton->imgKeyRightColor->setColor(Color3B(keyRightButton->colorPanel->r,keyRightButton->colorPanel->g,keyRightButton->colorPanel->b));
        }
    }
}


void UIKeyboardLayer::addPianoKey() {


}

void UIKeyboardLayer::valueChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt)
{
//    log("valuse change");
//    ControlSlider *slider = static_cast<cocos2d::extension::ControlSlider*>(sender);
//    int tag = slider->getTag();
//
//
//    float value = static_cast<cocos2d::extension::ControlSlider*>(sender)->getValue();
//
//    float valuebetween = value/100;
//
//
//
//   float x1 =  slider->getBoundingBox().getMinX()+slider->getContentSize().width*valuebetween;
//
//
//
//
//
//
//
//    stencil->setPosition(Vec2(x1,stencil->getPosition().y));
}


bool KeyButton::init() {
    if(!LayerColor::initWithColor(Color4B(0,0,0,0))){
        return false;
    }
    createPianoKey();
    return true;
}

void KeyButton::createPianoKey() {



    isRestKey = false;

    colorPanel = new ColorPanel();

//    btnKey = MenuItemImage::create("game screen/white key normal.png","game screen/white key press.png",CC_CALLBACK_0(KeyButton::keyClick,this));
  btnKey    =   Sprite::create("game screen/white key normal.png");

    addSpriteTouchEvents(btnKey,0);
    this->setContentSize(Size(winSize.width*0.1,btnKey->getContentSize().height));
    btnKey->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height/2));

    this->addChild(btnKey);

    layerAnimation = LayerColor::create(Color4B::RED);
    layerAnimation->setContentSize(this->getContentSize());
    this->addChild(layerAnimation,20);
    layerAnimation->setOpacity(0);

//    Menu *menu = Menu::create();
//    menu->setPosition(Vec2::ZERO);
//    menu->addChild(btnKey);
//
//    this->addChild(menu);





}

void KeyButton::setKeyColor(int keyNum,int octiveNum,int color) {

    log("setKeyColor");
    this->keyColor = color;


    std::string colorId = colorPanel->arrColor[color][octiveNum];


    log(" color id %s",colorId.c_str());
    String *strColorId = String::create(colorId);

    __Array *arrColor =  strColorId->componentsSeparatedByString(",");



        colorPanel->r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
        colorPanel->g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
        colorPanel->b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
        log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);

//    if(keyType==1){
//        imgKeyCenterColor->setColor(Color3B(colorPanel->r,colorPanel->b,colorPanel->g));
//    }else if(keyType==2){
//        imgKeyLeftColor->setColor(Color3B::RED);
//    }


}


void KeyButton::setKeyNumber(int number) {
    this->keyNumber = number;
}

void KeyButton::setKeyType(int type) {
    if(type==2){
        btnKey->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height*0.7));
        btnKey->setTexture("game screen/black key.png");
//        btnKey->setSelectedImage(Sprite::create("game screen/black key press.png"));

        imgKeyLeftColor = Sprite::create("game screen/left key color.png");
        imgKeyLeftColor->setAnchorPoint(Vec2(1,0.5));
        imgKeyLeftColor->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height*0.6));
        this->addChild(imgKeyLeftColor);

        imgKeyRightColor = Sprite::create("game screen/right key color.png");
        imgKeyRightColor->setAnchorPoint(Vec2(0,0.5));
        imgKeyRightColor->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height*0.6));
        this->addChild(imgKeyRightColor);


        log("if call---------------");
    }else{
        log("else call---------------");
        imgKeyCenterColor = Sprite::create("game screen/center key color.png");
        imgKeyCenterColor->setScale(0.8);
        imgKeyCenterColor->setPosition(Vec2(this->getContentSize().width/2,this->getContentSize().height*0.25));
        this->addChild(imgKeyCenterColor);
    }
    this->keyType = type;
}


void KeyButton::keyClick() {}



void KeyButton::rightKeyAnimation() {
    layerAnimation->stopAllActions();
    layerAnimation->setColor(Color3B::GREEN);
    FadeIn * fadeIn = FadeIn::create(0.3);
    FadeOut* fadeOut = FadeOut::create(0.3);

    Sequence *sequence = Sequence::create(fadeIn,fadeOut,NULL);
    layerAnimation->runAction(sequence);

}


void KeyButton::wrongKeyAnimation() {
    layerAnimation->stopAllActions();
    layerAnimation->setColor(Color3B::RED);
    FadeIn * fadeIn = FadeIn::create(0.3);
    FadeOut* fadeOut = FadeOut::create(0.3);

    Sequence *sequence = Sequence::create(fadeIn,fadeOut,NULL);
    layerAnimation->runAction(sequence);

}

void KeyButton::addSpriteTouchEvents(Sprite *touchKey, int itemType){
//    auto listener = EventListenerTouchOneByOne::create();
//
//    listener->onTouchBegan= [&](cocos2d::Touch* touch, cocos2d::Event* event)
//    {
//        Vec2         p              =       touch->getLocation();
//        Sprite *touchSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
//
//        Vec2         localPoint     =       touchSprite->getParent()->convertToNodeSpace(p);
//        cocos2d::Rect rect          =       touchSprite->getBoundingBox();
//
//        if(!rect.containsPoint(localPoint))
//        {
//            return false; // we did not consume this event, pass thru.
//        }
//
//
//
//        SoundController::playPianoKey(keyType,keyPosition,keyOctal);
//
//
//        if(keyType==2){
//            touchSprite->setTexture("game screen/black key press.png");
//        }else{
//            touchSprite->setTexture("game screen/white key press.png");
//        }
//
//
//
//        return true;// to indicate that we have consumed it.
//    };
//
//
//    listener->onTouchMoved= [=](cocos2d::Touch* touch, cocos2d::Event* event)
//    {
//        Vec2         p              =       touch->getLocation();
//        Sprite *DSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
//        Vec2		startPoint	=			touch->getStartLocation();
//
//    };
//
//    listener->onTouchEnded= [=](cocos2d::Touch* touch, cocos2d::Event* event)
//    {
//        Sprite *touchSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
//
//        if(keyType==2){
//            touchSprite->setTexture("game screen/black key.png");
//        }else{
//            touchSprite->setTexture("game screen/white key normal.png");
//        }
//    };
//    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, touchKey);
//    listener->setSwallowTouches(true);
}