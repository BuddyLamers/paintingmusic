#ifndef __SPLASHSCREEN_SCENE_H__
#define __SPLASHSCREEN_SCENE_H__

#include "cocos2d.h"

class SplashScreen: public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);

    void change();
    void goHome();

    void firstTimeLoad();
    // implement the "static create()" method manually
    CREATE_FUNC(SplashScreen);
};

#endif // __HELLOWORLD_SCENE_H__
