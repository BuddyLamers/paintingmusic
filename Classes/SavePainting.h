//

//

#ifndef PROJ_ANDROID_STUDIO_SAVEPAINTING_H
#define PROJ_ANDROID_STUDIO_SAVEPAINTING_H

#include "UISavePainting.h"
#include "CustomMenuItemImage.h"

#include "TouchableLayer.h"
class SavePainting : public UISavePainting{
public:



    TouchableLayer *layerRewrite;
    CustomMenuItemImage *btnRewriteCancel;
    CustomMenuItemImage *btnRewriteYes;


    SavePainting();

    ~SavePainting();

    virtual bool init();
    CREATE_FUNC(SavePainting);

    virtual void backCall();
    virtual void savePaintingCall();
    void startLoading(float dt);


    void showRewritePopup();

    void okayCall();

    void rewriteCancel();
    void rewriteYes();


};


#endif //PROJ_ANDROID_STUDIO_SAVEPAINTING_H
