/*
 * CustomMenuItemImage.cpp
 *
 *  Created on: 09-Aug-2014
 *      Author: Assert InfoTech
 */

#include "CustomMenuItemImage.h"

CustomMenuItemImage::CustomMenuItemImage() {
	// TODO Auto-generated constructor stub

}

CustomMenuItemImage::~CustomMenuItemImage() {
	// TODO Auto-generated destructor stub

}

CustomMenuItemImage * CustomMenuItemImage::create(const char *normalImage, const char *selectedImage,
		const ccMenuCallback& callback)
{

	CustomMenuItemImage *pRet = new CustomMenuItemImage();
	if (pRet && pRet->initWithNormalImage(normalImage, selectedImage,callback))
	{
		pRet->autorelease();
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return NULL;

}


bool CustomMenuItemImage::initWithNormalImage(const char *normalImage, const char *selectedImage, const ccMenuCallback& callback)
{
	this->setScale(1.0f);
	genericLabel			=		NULL;
	isAutoSelectionEnabled	=		true;
	isPressAnimationEnabled	=		true;
	return MenuItemImage::initWithNormalImage(normalImage,selectedImage,"",callback);
}

void CustomMenuItemImage::setAutoSelectionEnabled(bool selection){
	isAutoSelectionEnabled		=		selection;
}

//add label on menu item image
void CustomMenuItemImage::addLabel(const char *text,const char *fontName,float fontSize,const Color4B &color4){

	genericLabel       =       Label::createWithTTF(text, fontName, fontSize);
	genericLabel->setTextColor(color4);
	genericLabel->setPosition(Vec2(this->getContentSize().width*0.50,this->getContentSize().height*0.50));
	this->addChild(genericLabel);
}


//add label stroke on menu item image
void CustomMenuItemImage::addLabelStroke(const Color4B color){
    genericLabel->enableOutline(Color4B(color));
}


void CustomMenuItemImage::setPressAnimationEnabled(bool isEnabled){
	isPressAnimationEnabled			=		isEnabled;
}

//set scale value
void CustomMenuItemImage::setScale(float val){

	CCNode::setScale(val);
	scaleVal	=	this->getScale();

}

//super method
void CustomMenuItemImage::selected(){

	if(isAutoSelectionEnabled){
		CCMenuItemImage::selected();
	}

	if(isPressAnimationEnabled){
    ActionInterval  *scaleDown	=	ScaleTo::create(0.12f,scaleVal*0.9f);
		this->runAction(scaleDown);
	}
}


void CustomMenuItemImage::unselected(){

	if(isAutoSelectionEnabled){
		CCMenuItemImage::unselected();
	}

	if(isPressAnimationEnabled){
		ActionInterval  *scaleUp		=	ScaleTo::create(0.12f,scaleVal);
		this->runAction(scaleUp);
	}
}

void CustomMenuItemImage::setSelected(bool selection){
	if(selection){
		CCMenuItemImage::selected();
	}else{
		CCMenuItemImage::unselected();
	}
}

void CustomMenuItemImage::activate(){
	if(isPressAnimationEnabled){
        this->runAction(Sequence::create(DelayTime::create(0.1f),CallFunc::create(CC_CALLBACK_0(CustomMenuItemImage::onActivate, this)),NULL));
	}else{
		onActivate();
	}
}

void CustomMenuItemImage::onActivate(){
	CCMenuItemImage::activate();
}
