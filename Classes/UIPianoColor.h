

//720X1152
#ifndef PROJ_ANDROID_STUDIO_UIPIANOCOLOR_H
#define PROJ_ANDROID_STUDIO_UIPIANOCOLOR_H


#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC_EXT;
USING_NS_CC;

enum COLOR_TYPE{
    COLOR_WRONG = 0,
    COLOR_RIGHT,
    COLOR_NORMAL

};
//class ColorBox;
class UIPianoColor : public  cocos2d::LayerColor{

public:


    int h,w;
    int boxNumber;
    float animationTime;
    LayerColor *layerAnim;
    float extraSize;  // using for marge Cell

    std::string node;
    // for right or wrong mode
    Sprite *imgType = nullptr;


    // right or wrong
    int  colorType = false;

    int  isNoFillColor = false;

    int requireLength  = 0;// for playanimation note langth
    int requireNotePosition = 0; // for playanimation note pos



    Sprite *imgCenter;
    Sprite *imgLeft;
    Sprite *imgRight;
    int r1,g1,b1; // left side
    int r2,g2,b2; // right side
    int r,g,b ;// center

    int noteLength = 1;


    Sprite *arrowLeft;
    Sprite *arrowRight;
    Sprite *arrowTop;
    Sprite *arrowBottom;





    bool isEmptyCell;

    LayerColor *leftLayer;
    LayerColor *rightLayer;



    LayerColor *selectLayer;

    Scale9Sprite *outerBox;

    Sprite *imgArrow;
//    ColorBox *box;
    UIPianoColor();
    ~UIPianoColor();

    virtual bool init();
    CREATE_FUNC(UIPianoColor);

    void startAnimation();

    void resetAnimation();
    void setMargeCell(float width);

    void selectBox(bool isSelect);


    void startSelectAnimation();
    void stopSelectAnimation();

    void createLoopAnim();


    void setCellHeight(float height);
    void setCellWidth(float width);

    void setCellData();


    void animationOver();


    void setEmptyCell();

    float getSingleCellWidth();



};


//class ColorBox{
//    int h,w;
//    LayerColor *box;
//
//
//
//    void setBoxColor(Color3B);
//    void startAnimation();
//
//};

#endif //PROJ_ANDROID_STUDIO_UIPIANOCOLOR_H
