//
//

#ifndef PROJ_ANDROID_STUDIO_UIRECORDSONG_H
#define PROJ_ANDROID_STUDIO_UIRECORDSONG_H


#include "cocos2d.h"
USING_NS_CC;
//class RecordingPanel;
class UIRecordSong : public  cocos2d::Layer{
public:
    Size winSize = Director::getInstance()->getWinSize();
    UIRecordSong();
    ~UIRecordSong();
    static Scene *createScene();
    virtual  bool init();
    CREATE_FUNC(UIRecordSong);
    void initComponents();
    void addHeaderPanel();
    void btnClick(Ref *sender);
    void backCall();


};
//
//class RecordingPanel{
//    RecordingPanel();
//    ~RecordingPanel();
//    virtual  bool init();
//    CREATE_FUNC(RecordingPanel);
//
//};


#endif //PROJ_ANDROID_STUDIO_UIRECORDSONG_H
