#include "Modes.h"
#include "MenuScreen.h"
#include "SimpleAudioEngine.h"
#include "GameData.h"
#include "GameScene.h"
#include "CustomMenuItemImage.h"
#include "SoundController.h"
#include "UISetting.h"


#include "Home.h"

USING_NS_CC;

Scene* Modes::createScene()
{
    Scene *scene = Scene::create();
    auto layer = Modes::create();
    scene->addChild(layer);
    return scene;
}

// on "init" you need to initialize your instance
bool Modes::init() {
    //////////////////////////////
    // 1. super init first
    if (!Layer::init()) {
        return false;
    }



#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    this->setKeypadEnabled(true);
#endif




    KeyboardSelection *keyboardSelection = KeyboardSelection::create();
    keyboardSelection->setKeyboardType(1);
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object


    Size size = Director::getInstance()->getWinSize();

    Sprite *imgBg = Sprite::create("game mods/game mode bg.jpg");

    imgBg->setPosition(Vec2(size.width/2,size.height/2));
    this->addChild(imgBg);


    Sprite *btnBg = Sprite::create("game mods/Button BG.png");

    btnBg->setPosition(Vec2(size.width/2,size.height*0.6));
    this->addChild(btnBg);

    Sprite *boy = Sprite::create("game mods/boy.png");

    boy->setPosition(Vec2(size.width/2,size.height*0.0+boy->getContentSize().height/2));
    this->addChild(boy);









    auto btn1 = CustomMenuItemImage::create(
            "game mods/replicate song.png",
            "game mods/replicate song.png",
            CC_CALLBACK_0(Modes::fun1, this));

    btn1->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.8));

    auto btn2 = CustomMenuItemImage::create(
            "game mods/match audio.png",
            "game mods/match audio.png",
            CC_CALLBACK_0(Modes::fun2, this));

    btn2->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.6));


    auto btn3 = CustomMenuItemImage::create(
            "game mods/experimental play.png",
            "game mods/experimental play.png",
            CC_CALLBACK_0(Modes::fun3, this));

    btn3->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.4));



    auto lblInfo = Label::createWithTTF("USER TO SING INTO A MICROPHONE AND CREATE A SONG EXPERIMENT WITH SONGWRITING", "fonts/Marker Felt.ttf", 30,Size(btnBg->getContentSize().width*0.8,btnBg->getContentSize().height*0.3),TextHAlignment::CENTER,TextVAlignment::CENTER);
    lblInfo->setPosition(Vec2(btnBg->getContentSize().width/2 ,btnBg->getContentSize().height*0.35));
//    btnBg->addChild(lblInfo, 1);

    lblInfo->setColor(Color3B::WHITE);




    CustomMenuItemImage *btnBack = CustomMenuItemImage::create("achievement/back button.png","achievement/back button.png",CC_CALLBACK_0(Modes::backCall,this));
    btnBack->setPosition(Vec2(btnBg->getContentSize().width*0.11,btnBg->getContentSize().height*0.95));





    auto menu = Menu::create(btn1,btn2,btn3,btnBack, NULL);
    menu->setPosition(Vec2::ZERO);
    btnBg->addChild(menu, 1);






//    auto label1 = Label::createWithTTF("1", "fonts/Marker Felt.ttf", 15);
//    label1->setPosition(Vec2(btn1->getContentSize().width/2,  btn1->getContentSize().height/2));
//    btn1->addChild(label1, 1);
//
//    auto label2 = Label::createWithTTF("2", "fonts/Marker Felt.ttf", 15);
//    label2->setPosition(Vec2(btn2->getContentSize().width/2,  btn2->getContentSize().height/2));
//    btn2->addChild(label2, 1);
//
//    auto label3 = Label::createWithTTF("3", "fonts/Marker Felt.ttf", 15);
//    label3->setPosition(Vec2(btn3->getContentSize().width/2,  btn3->getContentSize().height/2));
//    btn3->addChild(label3, 1);



    return true;
}




void Modes::fun1()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    GameData::getInstance()->game_type = GAME_TYPE_REPLICATE_SONG;
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5, GameScene::createScene());
    Director::getInstance()->replaceScene(effect);
}

void Modes::fun2()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    GameData::getInstance()->game_type = GAME_TYPE_FILL_IN_COLOR;
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5, GameScene::createScene());
    Director::getInstance()->replaceScene(effect);
}

void Modes::fun3()
{
    SoundController::playEffectSound(SOUND_BTN_CLICK);
    GameData::getInstance()->game_type = GAME_TYPE_EXPERIMENTAL_PLAY;
    TransitionCrossFade *effect = TransitionCrossFade::create(0.5, GameScene::createScene());
    Director::getInstance()->replaceScene(effect);
}

void Modes::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) {
    log("back Button");
    if (keyCode == EventKeyboard::KeyCode::KEY_BACK) {
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#endif
        log("back Button");
        backCall();
    }

}


void Modes::backCall() {

    SoundController::playEffectSound(SOUND_BTN_CLICK);
    Director::getInstance()->replaceScene(Home::create());

}
