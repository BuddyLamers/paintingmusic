//
// Created by assertinfotech on 24/11/17.
//

#ifndef PROJ_ANDROID_STUDIO_UISETTING_H
#define PROJ_ANDROID_STUDIO_UISETTING_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "TouchableLayer.h"

using namespace cocos2d::extension;
USING_NS_CC;

class KeyboardSelection;
class UISetting : public cocos2d::Layer{


public:



    float scaleX,scaleY;
    Size winSize = Director::getInstance()->getWinSize();


    Sprite *imgBG;// for setting popup bg;

    Sprite *imgPaintingSetting;
    Sprite *imgKeyboardSetting;


    Sprite *imgPaintingGrid;

    Label *lblHeight;
    Label *lblWidth;

    UISetting();
    ~UISetting();
    virtual bool init();
    CREATE_FUNC(UISetting);

    void createComponents();
    void paintingSettingCall();
    void keyboardSettingCall();
    void backCall();

    void createPaintingSetting();
    void paintingBackCall();
    void heightChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt);
    void widthChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt);

};



class KeyboardSelection  : public cocos2d::Layer{
public:




    Size winSize = Director::getInstance()->getWinSize();

    TouchableLayer *layer_customKeyboard;

    Vector<MenuItemImage*> vecCustomKeys;

    Vector<Sprite*> vecColorBox;

    // if color box not scroll set true
    bool isTouchable;

    int arrKeyValue[7] = {0,0,0,0,0,0,0};
    int restSelected = 0;


    Vector<MenuItemImage*> vecRectColor;


    Sprite *imgRecentColor;
    Sprite *imgSwipeColor;
    Sprite *imgRestColor;


    // for color grid
    int selectedBoxColor;
    // for custom piano key
    int selectedKeyColor;


    KeyboardSelection();
    ~KeyboardSelection();

    virtual  bool init();

    CREATE_FUNC(KeyboardSelection);

    void initKeyboardSelection();

    void shopCall();
    Sprite* getKeyboard(int type);

    void addHeaderPanel();

    void swipeColorCall(cocos2d::Ref *sender);

    void backCall();


    // set keyboard color boardNumber 1 for classic
    void setKeyboardType(int boardNumber);


    void customKeyboardCreate();

    void customKeySelectCall(cocos2d::Ref* sender);

    void resetCustomKeys();


    void customSwipeCall();

    void addColorBoxTouchEvents(Sprite *item);

    void  doneCall();

    void restColorCall(cocos2d::Ref* sender);





};

#endif //PROJ_ANDROID_STUDIO_UISETTING_H
