//

//

#include "LoadPaintingScene.h"
#include "CustomMenuItemImage.h"
#include "SoundController.h"
USING_NS_CC;

Scene* LoadPaintingScene::createScene()
{
    auto scene = Scene::create();
    auto layer = LoadPaintingScene::create();
    scene->addChild(layer);
    return  scene;
}

// on "init" you need to initialize your instance
bool LoadPaintingScene::init() {
    //////////////////////////////
    // 1. super init first
    if (!Layer::init()) {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    LayerColor *layer1 = LayerColor::create(Color4B(90,90,90, 100));
    layer1->setContentSize(Size(visibleSize.width , +visibleSize.height*.1));
    layer1->setPosition(Vec2(origin.x + 0, origin.y + visibleSize.height*.9));
    this->addChild(layer1);


    auto label = Label::createWithTTF("Load Panting", "fonts/Marker Felt.ttf", 15);
    label->setPosition(Vec2(layer1->getContentSize().width/2, layer1->getContentSize().height*.5));
    layer1->addChild(label);

    auto backButton = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(LoadPaintingScene::back, this));

    backButton->setPosition(Vec2(layer1->getContentSize().width*.08, layer1->getContentSize().height*.5));

    auto btnMenu = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(LoadPaintingScene::back, this));

    btnMenu->setPosition(Vec2(layer1->getContentSize().width*.92, layer1->getContentSize().height*.5));


    auto btn1 = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(LoadPaintingScene::fun1, this));

    btn1->setPosition(Vec2(origin.x+visibleSize.width*.2, origin.y+visibleSize.height*.5));

    auto btn2 = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(LoadPaintingScene::fun2, this));

    btn2->setPosition(Vec2(origin.x+visibleSize.width*.5, origin.y+visibleSize.height*.5));


    auto btn3 = CustomMenuItemImage::create(
            "Back-button.png",
            "Back-button.png",
            CC_CALLBACK_0(LoadPaintingScene::fun3, this));

    btn3->setPosition(Vec2(origin.x+visibleSize.width*.8, origin.y+visibleSize.height*.5));


    auto btn5 = CustomMenuItemImage::create(
            "go2.png",
            "go2.png",
            CC_CALLBACK_0(LoadPaintingScene::fun5, this));

    btn5->setPosition(Vec2(origin.x+visibleSize.width / 2, origin.y+visibleSize.height * .42));

    auto btn6 = CustomMenuItemImage::create(
            "go2.png",
            "go2.png",
            CC_CALLBACK_0(LoadPaintingScene::fun6, this));

    btn6->setPosition(Vec2(origin.x+visibleSize.width / 2, origin.y+visibleSize.height* .3));


    auto btn7 = CustomMenuItemImage::create(
            "go2.png",
            "go2.png",
            CC_CALLBACK_0(LoadPaintingScene::fun7, this));

    btn7->setPosition(Vec2(origin.x+visibleSize.width / 2, origin.y+visibleSize.height * .18));

    auto label5 = Label::createWithTTF("Open as Painting", "fonts/Marker Felt.ttf", 15);
    label5->setPosition(Vec2(btn5->getContentSize().width/2, btn5->getContentSize().height*.5));
    btn5->addChild(label5, 1);

    auto label6 = Label::createWithTTF("Save Song", "fonts/Marker Felt.ttf", 15);
    label6->setPosition(Vec2(btn6->getContentSize().width/2, btn6->getContentSize().height*.5));
    btn6->addChild(label6, 1);

    auto label7 = Label::createWithTTF("Load Song", "fonts/Marker Felt.ttf", 15);
    label7->setPosition(Vec2(btn7->getContentSize().width/2, btn7->getContentSize().height*.5));
    btn7->addChild(label7, 1);





    auto menu = Menu::create(backButton,btnMenu, NULL);
    menu->setPosition(Vec2::ZERO);
    layer1->addChild(menu, 1);

    auto menu2 = Menu::create(btn1,btn2,btn3,btn5,btn6,btn7, NULL);
    menu2->setPosition(Vec2::ZERO);
    this->addChild(menu2, 1);


auto soundSpr =Sprite::create("HelloWorld.png");
soundSpr->setPosition(Vec2(origin.x+visibleSize.width*.5,origin.y+visibleSize.height*.7));
  this->addChild(soundSpr);
    return true;





}

void LoadPaintingScene::back()
{

}

void  LoadPaintingScene::fun1()
{

}

void LoadPaintingScene::fun2()
{

}

void  LoadPaintingScene::fun3()
{

}


void LoadPaintingScene::fun5()
{

}

void  LoadPaintingScene::fun6()
{

}

void  LoadPaintingScene::fun7()
{

}
