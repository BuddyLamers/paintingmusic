//
//
//

#ifndef PROJ_ANDROID_STUDIO_UIKEYBOARDLAYER_H
#define PROJ_ANDROID_STUDIO_UIKEYBOARDLAYER_H

#include "cocos2d.h"
#include "cocos-ext.h"


#include "ColorPanel.h"
#include "SoundController.h"

using namespace cocos2d::extension;
 USING_NS_CC;

#define  NUMBER_OF_KEYS  36
class KeyButton;
class UIKeyboardLayer : public cocos2d::Layer{

public:

    Vector<KeyButton*> vecKeyButton;
    Size winSize = Director::getInstance()->getWinSize();

    Sprite *imgkeyBoard; // for fix keyboard

    Layer *layer_SmallKeyboard;
    Sprite* Thumb_sprite;
    Sprite *stencil;
    Layer	*layerScroll;
    ScrollView *scrollView;

    ControlSlider *slider1;
    UIKeyboardLayer();
    ~UIKeyboardLayer();
    virtual  bool init();
    CREATE_FUNC(UIKeyboardLayer);

    void initComponents();

    void valueChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt);

    void setKeyboardKeysColor();


//    void set



    void addPianoKey();
};


class KeyButton : public  cocos2d::LayerColor{
public:
    Size winSize = Director::getInstance()->getWinSize();
    int keyType;
    int keyNumber;
    int keyColor;
    LayerColor *layerAnimation;
    bool isRestKey;
    int keyOctal    =   0;
    int keyPosition =   0;

    int soundId =   0;

    Sprite *imgKeyCenterColor;
    Sprite *imgKeyLeftColor;
    Sprite *imgKeyRightColor;


    ColorPanel *colorPanel;
    Sprite *btnKey;
    Sprite *imgKeyColor;

    virtual  bool init();
    CREATE_FUNC(KeyButton);

    void createPianoKey();
    void setKeyType(int type);
    void setKeyNumber(int number);
    void setKeyColor(int keyNum,int octiveNum,int color);
    void keyClick();

    void rightKeyAnimation();
    void wrongKeyAnimation();

    void addSpriteTouchEvents(Sprite *touchKey, int itemType);






};




#endif //PROJ_ANDROID_STUDIO_UIKEYBOARDLAYER_H
