//
//

#include "GameScene.h"
#include "GameData.h"
#include "Home.h"
#include "SavePainting.h"

#include "UIRecordSong.h"

#include "UISetting.h"
#include "LoadMusic.h"


bool GameScene::isSongPause = false;
GameScene::GameScene() {

}


GameScene::~GameScene() {

}

Scene* GameScene::createScene() {

    Scene *scene = Scene::create();
    auto layer = GameScene::create();
    scene->addChild(layer);
    return scene;

}
bool GameScene::init() {

    if(!UIGameScene::init()){
        return false;
    }

    if(CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID){
        setKeypadEnabled(true);
    }

    initComponents();
    createComponents();



    return true;



}


void GameScene::initComponents() {


    musicTimer  = 0;
    startDragColor = nullptr;
    isBackGroundPlay = false;
    isSongPause = false;
    isSongPlaying   =   false;
    pianoSong = new PianoSong();
    log("before load game");

    GameData::getInstance()->currentSongName = "";
//   keyboardLayer->setKeyboardKeysColor();
//    resetGridItem();
//   resetGridColor();

    GameData::getInstance()->currentSongName = "";
    if(GameData::getInstance()->game_type==GAME_TYPE_REPLICATE_SONG){
        log("before load game1");
//        loadGameData();
        loadMatchAudio();
    }else if(GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR){
        log("before load game2");
        loadMatchAudio();
    }else if(GameData::getInstance()->game_type==GAME_TYPE_EXPERIMENTAL_PLAY){
//        loadGameData();
        isReplicateAudioEnd = false;
    } else if(GameData::getInstance()->game_type==GAME_TYPE_LOADGAME){
                loadGameData();
        isReplicateAudioEnd = false;
    }

    log("before load game3");
}
void GameScene::loadGameData() {
    std::string fileName    =   GameData::getInstance()->load_song_name;//UserDefault::getInstance()->getStringForKey("recent_song","");
   std::string data =  GameData::getInstance()->readSong(fileName);
    lblTitle->setString(fileName);
    GameData::getInstance()->currentSongName = fileName;
    __Array *arrData = StringMake(data)->componentsSeparatedByString("$");

    String *songName = ((String*)arrData->getObjectAtIndex(0));
    String *soundNods = ((String*)arrData->getObjectAtIndex(1));
    String *colorNods = ((String*)arrData->getObjectAtIndex(2));
    String *keyBoard = ((String*)arrData->getObjectAtIndex(3));

//    int keyBoardSelect = Value(keyBoard).asInt();
//    KeyboardSelection *keyboardSelection = KeyboardSelection::create();
//    keyboardSelection->setKeyboardType(keyBoardSelect);

    log("marge cel size--- %s",colorNods->getCString());
    __Array *arrSoundNods = soundNods->componentsSeparatedByString(",");
    for (int i = 0; i < arrSoundNods->count(); i++) {
        pianoSong->vecSoundNode.pushBack(((String*)arrSoundNods->getObjectAtIndex(i)));
    }

    __Array *arrColorNods = colorNods->componentsSeparatedByString(",");

    setColorGrid(arrColorNods);

}




void GameScene::loadMatchAudio() {

    LoadMusic *loadMusic = LoadMusic::create();
    this->addChild(loadMusic);

    musicNodes = new MusicNodes();

    if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG){
        isReplicateAudioEnd = false;
    }else if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
     isReplicateAudioEnd = false;
        loadFillInBlanks();
    }

//    TransitionFadeTR *effect = TransitionFadeTR::create(0.5,LoadMusic::createScene());
//    Director::getInstance()->pushScene(LoadMusic::createScene());



}

void GameScene::loadFillInBlanks() {



    for (int i = 0; i < musicNodes->vecNode.size(); ++i) {
        KeyButton *keyButton = KeyButton::create();
        ColorPanel *colorPanel = new ColorPanel();
        keyButton->keyType = 0;

        __String *node = musicNodes->vecNode.at(i);

//        node->isEqual(__String::create("00"))
        if(node->isEqual(__String::create("00"))) {

            std::string keyCode = KEY_COLOR_CODE_REST;
            int color = UserDefault::getInstance()->getIntegerForKey(keyCode.c_str());
            keyButton->keyColor = color;
            keyButton->isRestKey = true;
            std::string colorId = colorPanel->arrRestColor[color];

            log(" color id %s", colorId.c_str());
            String *strColorId = String::create(colorId);

            __Array *arrColor = strColorId->componentsSeparatedByString(",");
            colorPanel->r = atoi(((__String *) arrColor->getObjectAtIndex(0))->_string.c_str());
            colorPanel->g = atoi(((__String *) arrColor->getObjectAtIndex(1))->_string.c_str());
            colorPanel->b = atoi(((__String *) arrColor->getObjectAtIndex(2))->_string.c_str());
            log("r value %d g%d b%d", colorPanel->r, colorPanel->g, colorPanel->b);
        }else{
            colorPanel->r = 220;
            colorPanel->g = 220;
            colorPanel->b = 220;
        }
        keyButton->colorPanel->r1 = colorPanel->r;
        keyButton->colorPanel->g1= colorPanel->g;
        keyButton->colorPanel->b1 = colorPanel->b;

        keyButton->colorPanel->r2 = colorPanel->r;
        keyButton->colorPanel->g2= colorPanel->g;
        keyButton->colorPanel->b2 = colorPanel->b;

        keyButton->colorPanel->r = colorPanel->r;
        keyButton->colorPanel->g= colorPanel->g;
        keyButton->colorPanel->b = colorPanel->b;

        addColorBox(keyButton);


    }


    for (int i = 0; i < musicNodes->vecNode.size(); ++i) {
        __String *strNode = musicNodes->vecNode.at(i);
        log("node name %s",strNode->getCString());

        __Array *arrNode = strNode->componentsSeparatedByString("#");
        for (int j = 1; j < arrNode->count(); ++j) {
//            log("compair value %s",((String*)arrNode->getObjectAtIndex(i))->getCString());
            vecpianoColor.at(i)->setMargeCell(vecpianoColor.at(i)->noteLength);
        }
        log("compair string %d",arrNode->count());

//        if(arrNode->count()==2){
//            vecpianoColor.at(i)->setMargeCell(vecpianoColor.at(i)->w);
//        }

    }

resetGridItem();

    Vector<FiniteTimeAction* > vecAnimation;
    float delatTime = 0;
    for (int i = 0; i < vecpianoColor.size(); ++i) {

        UIPianoColor *color = vecpianoColor.at(i);

        color->isNoFillColor = true;
//        color->startAnimation();
        CallFunc *callFunc = CallFunc::create(CC_CALLBACK_0(UIPianoColor::startAnimation,color));

        if(i>0){
            color->animationTime = musicNodes->vecTime.at(i)->getValue()-musicNodes->vecTime.at(i-1)->getValue();
        }else{
            color->animationTime = musicNodes->vecTime.at(i)->getValue();
        }



        CallFunc *callFunc1 = CallFunc::create([&]{
            fillInColorCounter++;
        });
        vecAnimation.pushBack(callFunc);
        vecAnimation.pushBack(DelayTime::create(color->animationTime));
        vecAnimation.pushBack(callFunc1);

    }
    CallFunc *callFunc2 = CallFunc::create([&]{
        isReplicateAudioEnd = true;
        SoundController::stopAllEffectSound();
        int rightNode = 0;
        for (int i = 0; i < vecReplicateNodesClick.size(); ++i) {
            int n = vecReplicateNodesClick.at(i)->getValue();
            if(n==1){
                rightNode++;
            }
        }

        String  *result = String::createWithFormat("%d of %d notes correctly matched",rightNode,musicNodes->vecTime.size());
//            MessageBox("Result",result->getCString());

        if(rightNode==musicNodes->vecTime.size()){
            result = String::create("You win!");
        }
        showMessageDialog("result",result->getCString());

    });

    vecAnimation.pushBack(callFunc2);
    Sequence * sequence = Sequence::create(vecAnimation);
//    this->runAction(sequence);

}


void GameScene::setColorGrid(__Array *arrColorNods) {
    int pWidth  =  0;
    int pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    log("Number of notes :: %d",pianoSong->vecSoundNode.size());
    for (int i = 0; i < pianoSong->vecSoundNode.size(); i++) {

        std::string strNode = pianoSong->vecSoundNode.at(i)->getCString();

        int counter =   getKeyBtnIndex(strNode);
        KeyButton *keyBtn = keyboardLayer->vecKeyButton.at(counter);

        log("note name %s",strNode.c_str());
//        addColorBox(keyBtn);
        UIPianoColor *color = UIPianoColor::create();
        color->node = strNode;
        addColorBoxTouchEvents(color);
        color->boxNumber = i;

        String *colorCell = ((String*)arrColorNods->getObjectAtIndex(i));

        __Array *arrCellProperty = colorCell->componentsSeparatedByString(":");



        color->noteLength =   atoi(((String*)arrCellProperty->getObjectAtIndex(0))->getCString());

        int isEmpty =  atoi(((String*)arrCellProperty->getObjectAtIndex(1))->getCString());

        if(isEmpty==0){
            color->isEmptyCell = true;
        }else{
            color->isEmptyCell = false;
        }

        if( color->isEmptyCell){

            ColorPanel *colorPanel = new ColorPanel();
            std::string keyCode = KEY_COLOR_CODE_REST;
            int colorIndex = UserDefault::getInstance()->getIntegerForKey(keyCode.c_str());
            std::string colorId = colorPanel->arrRestColor[colorIndex];


            log(" color id %s",colorId.c_str());
            String *strColorId = String::create(colorId);

            __Array *arrColor =  strColorId->componentsSeparatedByString(",");



            colorPanel->r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
            colorPanel->g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
            colorPanel->b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
            log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);




            color->imgLeft->setColor(Color3B(colorPanel->r,colorPanel->g,colorPanel->b));
            color->imgRight->setColor(Color3B(colorPanel->r,colorPanel->g,colorPanel->b));

//            color->leftLayer->setColor(Color3B::WHITE);
//            color->rightLayer->setColor(Color3B::WHITE);
//            color->imgCenter->setColor(Color3B::WHITE);
            color->isEmptyCell = true;
        }else if(keyBtn->keyType == 2){
            color->r1  =keyBtn->colorPanel->r1;
            color->g1  =keyBtn->colorPanel->g1;
            color->b1  =keyBtn->colorPanel->b1;

            color->r2  =keyBtn->colorPanel->r2;
            color->g2  =keyBtn->colorPanel->g2;
            color->b2  =keyBtn->colorPanel->b2;

            color->imgLeft->setColor(Color3B(keyBtn->colorPanel->r1,keyBtn->colorPanel->g1,keyBtn->colorPanel->b1));
            color->imgRight->setColor(Color3B(keyBtn->colorPanel->r2,keyBtn->colorPanel->g2,keyBtn->colorPanel->b2));

        }else{
            color->r  =keyBtn->colorPanel->r;
            color->g  =keyBtn->colorPanel->g;
            color->b  =keyBtn->colorPanel->b;

            color->imgLeft->setColor(Color3B(keyBtn->colorPanel->r,keyBtn->colorPanel->g,keyBtn->colorPanel->b));
            color->imgRight->setColor(Color3B(keyBtn->colorPanel->r,keyBtn->colorPanel->g,keyBtn->colorPanel->b));
        }


        vecpianoColor.pushBack(color);
//        if(i==0){
//            pWidth  = 0;
//        }else
//        if(vecpianoColor.at(i-1)->extraSize>1.000000){
//            pWidth  +=  vecpianoColor.at(i-1)->w*1.025;
//        }else{
//            pWidth  +=  vecpianoColor.at(i-1)->w*1.05;
//        }
//
//        if(pWidth>scrollView->getViewSize().width-color->w) {
//            pHeight -= color->h * 1.05;
//            pWidth = 0;
//        }
//        log("inside loop color2");
//        scrollView->getChildByTag(101)->addChild(color);
//        color->setPosition(Vec2(pWidth,pHeight-color->getContentSize().height*1.05));
    }


    resetGridItem();
}



void GameScene::createComponents() {



    int t1 = 101;


    for (int i = 0; i < keyboardLayer->vecKeyButton.size(); i++) {
        KeyButton *key = keyboardLayer->vecKeyButton.at(i);
        addSpriteTouchEvents(key,0);
    }

    if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG || GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR) {
        btnRecord->setEnabled(false);
        btnRecord->setDisabledImage(Sprite::create("game screen/mike btn disable.png"));
    }

        for (int i = 0; i < 7; i++) {

        MenuItemImage *btn = static_cast<MenuItemImage*>(keyboardLayer->getChildByTag(110)->getChildByTag(111)->getChildByTag(t1));
        if(GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR){
            int tag = btn->getTag();
            if(tag==107){
                btn->setEnabled(false);
                btn->setDisabledImage(Sprite::create("game screen/delet icon disable.png"));
                btn->setSelectedImage(Sprite::create("game screen/delet icon disable.png"));
            }else if(tag==101){
                btn->setEnabled(false);
                btn->setSelectedImage(Sprite::create("game screen/erase disable.png"));
                btn->setDisabledImage(Sprite::create("game screen/erase disable.png"));
            }else if(tag==105){
                btn->setEnabled(false);
                btn->setSelectedImage(Sprite::create("game screen/save  button lower disable.png"));
                btn->setDisabledImage(Sprite::create("game screen/save  button lower disable.png"));
            }
        }
            if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG ){
                int tag = btn->getTag();
                if(tag==105){
                    btn->setEnabled(false);
                    btn->setSelectedImage(Sprite::create("game screen/save  button lower disable.png"));
                    btn->setDisabledImage(Sprite::create("game screen/save  button lower disable.png"));
                }
            }



            btn->setCallback([&](cocos2d::Ref* sender){
            int  tag    =   ((MenuItemImage*)sender)->getTag();

            log("button click %d",tag);
                SoundController::playEffectSound(SOUND_BTN_CLICK);
            switch(tag){
                case 101:{//rest call



                    restCall();
//                    createEmptyCell();
//                    eraseCall();
                }break;
                case 102:{// play call


                    playSong();
                }break;
                case 103:{// pause call

                    pauseCall();
                }break;
                case 104:{// stop call
                    startDragColor = nullptr;
                    stopCall();

                }break;
                case 105:{// save call

                    saveCall();
                }break;
                case 106:{// setting call

                    UISetting *uiSetting = UISetting::create();
                    this->addChild(uiSetting);

                }break;
                case 107:{// erase call

                   eraseCall();

//                    deleteCall();
                }break;

            }

        });

        t1++;
    }
}


void GameScene::valueChangedCallback(Ref* sender, cocos2d::extension::Control::EventType evnt)
{
    log("valuse change");
    ControlSlider *slider = static_cast<cocos2d::extension::ControlSlider*>(sender);
    int tag = slider->getTag();


    float value = static_cast<cocos2d::extension::ControlSlider*>(sender)->getValue();

    float valuebetween = value/100;



    float x1 =  slider->getBoundingBox().getMinX()+slider->getContentSize().width*valuebetween;



    keyboardLayer->stencil->setPosition(Vec2(x1,keyboardLayer->stencil->getPosition().y));

    float x2 = (winSize.width-keyboardLayer->scrollView->getContentSize().width+100)*valuebetween;

    keyboardLayer->scrollView->setContentOffset(Vec2(x2,0),false);
}

void GameScene::recordCall(cocos2d::Ref *sender){

    SoundController::playEffectSound(SOUND_BTN_CLICK);
if(GameData::getInstance()->game_type ==  GAME_TYPE_EXPERIMENTAL_PLAY){
    Director::getInstance()->pushScene(UIRecordSong::createScene());

}
}
void GameScene::pauseCall(){


//    if(isSongPlaying){

        isSongPause = !isSongPause;
//    }

}

void GameScene::playMusicCall(cocos2d::Ref * sender){
    log("pause call");
    SoundController::playEffectSound(SOUND_BTN_CLICK);
//    if(!isReplicateAudioEnd){
//        return;
//    }
//    if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG){
//        return;
//    }
//    GameData::getInstance()->listAllSong
    bool flag = false;

    log("load recording name %s",GameData::getInstance()->load_recording_name.c_str());
    if(!isBackGroundPlay){
        isBackGroundPlay = true;
        if( GameData::getInstance()->load_recording_name == "#NULL"){
            log("---11");
            if(!isReplicateAudioStart){
                log("---11.1");
                isReplicateAudioStart = true;
                replicateAudioId =  SoundController::playMusicNode("jingle bell song.mp3",0.1);
            }else{
                log("---11.2");
                SoundController::resumeSound(replicateAudioId);
            }
        } else {
            log("---11.3");
            GameData::resumeRecording();
        }
        ((MenuItemImage*)sender)->setNormalImage(Sprite::create("game screen/pause button.png"));
        ((MenuItemImage*)sender)->setSelectedImage(Sprite::create("game screen/pause button.png"));
    } else{
        log("---22");
        isBackGroundPlay  = false;
        if(GameData::getInstance()->load_recording_name == "#NULL"){
            SoundController::pauseSound(replicateAudioId);

        } else {
            GameData::pauseRecording();
        }
        ((MenuItemImage*)sender)->setNormalImage(Sprite::create("game screen/play button lower.png"));
        ((MenuItemImage*)sender)->setSelectedImage(Sprite::create("game screen/play button lower.png"));
    }
}


void GameScene::stopMusicCall(cocos2d::Ref *sender) {

    SoundController::playEffectSound(SOUND_BTN_CLICK);
    if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR || GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG)
    {
        isReplicateAudioStart = false;
    }if( GameData::getInstance()->load_recording_name == "#NULL"){
        isReplicateAudioStart = false;
    }else{
        log("stop music call");
        GameData::pauseRecording();
    }

    SoundController::stopAllEffectSound();
    GameData::getInstance()->gameMusicID = -1;
    log("stop music call success");
    btnPlay->setNormalImage(Sprite::create("game screen/play button lower.png"));
    btnPlay->setSelectedImage(Sprite::create("game screen/play button lower.png"));

}

void GameScene::stopCall() {
    soungNodeCounter = 0;
    isSongPlaying = false;
    isSongPause = false;



    unschedule(schedule_selector(GameScene::playUpdate));
    stopAllBoxAnim();
    for (int i = 0; i < vecpianoColor.size(); ++i) {
        vecpianoColor.at(i)->resetAnimation();
    }

    if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG || GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
//        String  *result = String::createWithFormat("you tapped %d correct nots out of %d",rightNodeCounter,musicNodes->vecTime.size());

        String  *result = String::createWithFormat("%d of %d notes correctly matched",rightNodeCounter,musicNodes->vecTime.size());
//            MessageBox("Result",result->getCString());

        if(rightNodeCounter==musicNodes->vecTime.size()){
            result = String::create("You win!");
        }

        showMessageDialog("Result",result->getCString());
    }

    MenuItemImage *btn = static_cast<MenuItemImage*>(keyboardLayer->getChildByTag(110)->getChildByTag(111)->getChildByTag(102));


            btn->setSelectedImage(Sprite::create("game screen/play button lower.png"));
            btn->setNormalImage(Sprite::create("game screen/play button lower.png"));




}


void GameScene::saveCall(){
    if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR || GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG){
        return;
    }
    log("func2 call");
    SavePainting *savePainting = SavePainting::create();
//    savePainting->setPosition(Vec)
    this->addChild(savePainting);
}


void GameScene::autoMoveTile(){
    currentTileCount++;
}
void GameScene::backCall() {
    MenuScreen *menuScreen  =   MenuScreen::create();
    this->addChild(menuScreen);
}

void GameScene::restCall() {
    if( GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR) {

//        if (isReplicateAudioEnd){
            return;
//        }
    }
        KeyButton *keyButton = KeyButton::create();
    ColorPanel *colorPanel = new ColorPanel();
    keyButton->keyType = 0;

    std::string keyCode = KEY_COLOR_CODE_REST;
    int color = UserDefault::getInstance()->getIntegerForKey(keyCode.c_str());
    keyButton->keyColor = color;
    keyButton->isRestKey = true;
    std::string colorId = colorPanel->arrRestColor[color];

    log(" color id %s",colorId.c_str());
    String *strColorId = String::create(colorId);

    __Array *arrColor =  strColorId->componentsSeparatedByString(",");
    colorPanel->r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
    colorPanel->g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
    colorPanel->b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
    log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);

    keyButton->colorPanel->r1 = colorPanel->r;
    keyButton->colorPanel->g1= colorPanel->g;
    keyButton->colorPanel->b1 = colorPanel->b;

    keyButton->colorPanel->r2 = colorPanel->r;
    keyButton->colorPanel->g2= colorPanel->g;
    keyButton->colorPanel->b2 = colorPanel->b;

    keyButton->colorPanel->r = colorPanel->r;
    keyButton->colorPanel->g= colorPanel->g;
    keyButton->colorPanel->b = colorPanel->b;

    addColorBox(keyButton);
}



void GameScene::createEmptyCell() {
    KeyButton *selectedBtn = keyboardLayer->vecKeyButton.at(vecpianoColor.getIndex(startDragColor));
    selectedBtn->keyType = 0;
    startDragColor->imgLeft->setColor(Color3B::WHITE);
    startDragColor->imgRight->setColor(Color3B::WHITE);
    startDragColor->isEmptyCell = true;
//    startDragColor->imgCenter->setColor(Color3B::WHITE);


}





void GameScene::deleteCall() {

    log("delete call");
    pianoSong->vecSoundNode.eraseObject(pianoSong->vecSoundNode.at(vecpianoColor.getIndex(startDragColor)));
    if(startDragColor!= nullptr){
        vecpianoColor.eraseObject(startDragColor,true);
        startDragColor = nullptr;
    }

    scrollView->getChildByTag(101)->removeAllChildrenWithCleanup(true);





    int pWidth  =   5;
    int pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    log("Number of notes :: %d",pianoSong->vecSoundNode.size());
    for (int i = 0; i < pianoSong->vecSoundNode.size(); i++) {

        std::string strNode = pianoSong->vecSoundNode.at(i)->getCString();
        int counter =   getKeyBtnIndex(strNode);
        KeyButton *keyBtn = keyboardLayer->vecKeyButton.at(counter);

//        addColorBox(keyBtn);
        UIPianoColor *color = vecpianoColor.at(i);


        if(i==0){
            pWidth  -=  color->w*1.05;
        }
        pWidth  +=  color->w*1.05;
        if(pWidth>imgColorPanel->getContentSize().width*0.9) {
            pHeight -= color->h * 1.05;
            pWidth = 5;
        }
        log("inside loop color2");
        scrollView->getChildByTag(101)->addChild(color);
        color->setPosition(Vec2(pWidth,pHeight-color->getContentSize().height*1.05));
    }



}



void GameScene::eraseCall() {

    log("erase call");

    if(!vecpianoColor.size()>0 || GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
        return;
    }
    pianoSong->vecSoundNode.eraseObject(pianoSong->vecSoundNode.at(vecpianoColor.getIndex(startDragColor)));
    if(startDragColor!= nullptr){
        vecpianoColor.eraseObject(startDragColor,true);
        startDragColor = nullptr;
    }

    scrollView->getChildByTag(101)->removeAllChildrenWithCleanup(true);

    float pWidth  =   0;
    float pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    float scrollHeight = 0;
    log("Number of notes :: %d",pianoSong->vecSoundNode.size());
    for (int i = 0; i < pianoSong->vecSoundNode.size(); i++) {

        std::string strNode = pianoSong->vecSoundNode.at(i)->getCString();
        int counter =   getKeyBtnIndex(strNode);
        KeyButton *keyBtn = keyboardLayer->vecKeyButton.at(counter);

//        addColorBox(keyBtn);
        UIPianoColor *color = vecpianoColor.at(i);


        if(i==0){
            pWidth  = 0;
        }else
        if(vecpianoColor.at(i-1)->extraSize>1){
            pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
        }else{
            pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
        }

        if(pWidth>scrollView->getViewSize().width-color->w) {
            pHeight -= color->h * 1.05;
            pWidth =0 ;
            scrollHeight += color->h * 1.05;
        }
        log("inside loop color2");
        scrollView->getChildByTag(101)->addChild(color);
        color->setPosition(Vec2(pWidth,pHeight-color->getContentSize().height*1.05));
    }

    scrollHeight+=200;

    scrollView->setContentSize(Size(scrollView->getViewSize().width,MAX(scrollView->getViewSize().height,scrollHeight)));


    pWidth  =   0;
    pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    for (int i = 0; i < vecpianoColor.size(); i++) {
        UIPianoColor *color = vecpianoColor.at(i);
        if(i==0){
            pWidth  = 0;
        }else
        if(vecpianoColor.at(i-1)->extraSize>1){
            pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
        }else{
            pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
        }

        if(pWidth>scrollView->getViewSize().width-color->w) {
            pHeight -= color->h * 1.05;
            pWidth = 0;
        }
        log("inside loop color2");
        color->setPosition(Vec2(pWidth,pHeight-color->getContentSize().height*1.05));
    }


    if(startDragColor==nullptr){
        scrollView->setContentOffset(Vec2::ZERO);

    }else{
        scrollView->setContentOffset(Vec2(0,MAX(scrollView->getViewSize().height-scrollView->getContentSize().height,-startDragColor->getPositionY()+200)));

    }

}


void GameScene::resetGridItem() {
    scrollView->getChildByTag(101)->removeAllChildrenWithCleanup(true);
    float pWidth  =   0;
    float pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    float scrollHeight = 0;
    log("Number of notes :: %d",pianoSong->vecSoundNode.size());
    for (int i = 0; i < vecpianoColor.size(); i++) {

        std::string strNode = pianoSong->vecSoundNode.at(i)->getCString();
        int counter =   getKeyBtnIndex(strNode);
        KeyButton *keyBtn = keyboardLayer->vecKeyButton.at(counter);

//        addColorBox(keyBtn);
        UIPianoColor *color = vecpianoColor.at(i);
        color->setCellData();

        log("extra size %d-----",color->noteLength);
        if(i==0){
            pWidth  = 0;
        }else
            if(vecpianoColor.at(i-1)->extraSize>1){
                pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
            }else{
                pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
            }

        if(pWidth>scrollView->getViewSize().width-color->w) {
            pHeight -= color->h * 1.05;
            pWidth = 0;
            scrollHeight+= color->h * 1.05;
        }
        log("inside loop color2");
        scrollView->getChildByTag(101)->addChild(color);
        color->setPosition(Vec2(pWidth,pHeight-color->getContentSize().height*1.05));

    }

    scrollHeight+=200;


    scrollView->setContentSize(Size(scrollView->getViewSize().width,MAX(scrollView->getViewSize().height,scrollHeight)));

     pWidth  =   0;
     pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    for (int i = 0; i < vecpianoColor.size(); i++) {
        UIPianoColor *color = vecpianoColor.at(i);
        if(i==0){
            pWidth  = 0;
        }else
        if(vecpianoColor.at(i-1)->extraSize>1){
            pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
        }else{
            pWidth  +=  (vecpianoColor.at(i-1)->w+vecpianoColor.at(i-1)->getSingleCellWidth()*0.05);
        }

        if(pWidth>scrollView->getViewSize().width-color->w) {
            pHeight -= color->h * 1.05;
            pWidth = 0;
        }
        log("inside loop color2");
        color->setPosition(Vec2(pWidth,pHeight-color->getContentSize().height*1.05));
    }

    if(startDragColor==nullptr){
        scrollView->setContentOffset(Vec2::ZERO);

    }else{
        scrollView->setContentOffset(Vec2(0,MAX(scrollView->getViewSize().height-scrollView->getContentSize().height,-startDragColor->getPositionY()+200)));


    }
}



// keyboard key tap
void GameScene::addSpriteTouchEvents(KeyButton *touchKey2, int itemType){
    auto listener = EventListenerTouchOneByOne::create();

    log("addSpriteTouchEvents %d",touchKey2->keyNumber);
    listener->onTouchBegan= [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {


        if(isSongPlaying){
            return  false;
        }

        if( isReplicateAudioEnd){
            return  false;
        }



        Vec2         p              =       touch->getLocation();
        Sprite *touchSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
        Vec2         localPoint     =       touchSprite->getParent()->convertToNodeSpace(p);
        cocos2d::Rect rect          =       touchSprite->getBoundingBox();
        KeyButton *touchKey = static_cast<KeyButton*>(touchSprite->getParent());

        if(!rect.containsPoint(localPoint))
        {
            return false; // we did not consume this event, pass thru.
        }
//        log("on touch call3 -- %d--%d--%d",touchKey->keyType,touchKey->keyPosition,touchKey->keyOctal);

        int currentPianoNote = SoundController::playPianoKey(touchKey->keyType,touchKey->keyPosition,touchKey->keyOctal,touchKey->keyNumber);



            CallFunc *callFunc = CallFunc::create(CC_CALLBACK_0(SoundController::stopEffectSound,currentPianoNote));
            Sequence *sequence = Sequence::create(DelayTime::create(1),callFunc,NULL);
            this->runAction(sequence);





        if(touchKey->keyType==2){
            touchSprite->setTexture("game screen/black key press.png");
        }else{
            touchSprite->setTexture("game screen/white key press.png");
        }
//        stopAllBoxAnim();


        return true;// to indicate that we have consumed it.
    };


    listener->onTouchMoved= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        Vec2         p              =       touch->getLocation();
        Sprite *DSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
        Vec2		startPoint	=			touch->getStartLocation();

    };
    listener->onTouchEnded= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        Sprite *touchSprite  =       static_cast<Sprite*>(event->getCurrentTarget());
        KeyButton *touchKey = static_cast<KeyButton*>(touchSprite->getParent());





        if(touchKey->keyType==2){
            touchSprite->setTexture("game screen/black key.png");
        }else{
            touchSprite->setTexture("game screen/white key normal.png");
        }

        if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR) {
            if (startDragColor == nullptr) {
                return;
            }
        }

        UIPianoColor *pianoColor    =   startDragColor;
        if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
            if(fillInColorCounter>=vecpianoColor.size()){
                isReplicateAudioEnd = true;
                SoundController::stopAllEffectSound();
                int rightNode = 0;
                for (int i = 0; i < vecReplicateNodesClick.size(); ++i) {
                    int n = vecReplicateNodesClick.at(i)->getValue();
                    if(n==1){
                        rightNode++;

                    }
                }

                String  *result = String::createWithFormat("%d of %d notes correctly matched",rightNode,musicNodes->vecTime.size());
//            MessageBox("Result",result->getCString());

                if(rightNode==musicNodes->vecTime.size()){
                    result = String::create("You win!");
                }
                return;
            }
            if(touchKey->keyType == 2){
                pianoColor->r1  =touchKey->colorPanel->r1;
                pianoColor->g1  =touchKey->colorPanel->g1;
                pianoColor->b1  =touchKey->colorPanel->b1;
                pianoColor->r2  =touchKey->colorPanel->r2;
                pianoColor->g2  =touchKey->colorPanel->g2;
                pianoColor->b2  =touchKey->colorPanel->b2;
                pianoColor->imgLeft->setColor(Color3B(touchKey->colorPanel->r1,touchKey->colorPanel->g1,touchKey->colorPanel->b1));
                pianoColor->imgRight->setColor(Color3B(touchKey->colorPanel->r2,touchKey->colorPanel->g2,touchKey->colorPanel->b2));

            }else{
                pianoColor->r  =touchKey->colorPanel->r;
                pianoColor->g  =touchKey->colorPanel->g;
                pianoColor->b  =touchKey->colorPanel->b;
                pianoColor->imgLeft->setColor(Color3B(touchKey->colorPanel->r,touchKey->colorPanel->g,touchKey->colorPanel->b));
                pianoColor->imgRight->setColor(Color3B(touchKey->colorPanel->r,touchKey->colorPanel->g,touchKey->colorPanel->b));
            }
            std::string strNode = "";
            if(touchKey->keyNumber==0){
                strNode = "a1";
            }
            switch(touchKey->keyNumber){
                case 0:
                    strNode   =   strNode+"a1";
                    break;
                case 1:
                    strNode   =   strNode+"#a1";
                    break;
                case 2:
                    strNode   =   strNode+"b1";
                    break;
                case 3:
                    strNode   =   strNode+"#b1";
                    break;
                case 4:
                    strNode   =   strNode+"c1";
                    break;
                case 5:
                    strNode   =   strNode+"d1";
                    break;
                case 6:
                    strNode   =   strNode+"#c1";
                    break;
                case 7:
                    strNode   =   strNode+"e1";
                    break;
                case 8:
                    strNode   =   strNode+"#d1";
                    break;
                case 9:
                    strNode   =   strNode+"f1";
                    break;
                case 10:
                    strNode   =   strNode+"#e1";
                    break;
                case 11:
                    strNode   =   strNode+"g1";
                    break;

                case 12:
                    strNode   =   strNode+"a2";
                    break;
                case 13:
                    strNode   =   strNode+"#a2";
                    break;
                case 14:
                    strNode   =   strNode+"b2";
                    break;
                case 15:
                    strNode   =   strNode+"#b2";
                    break;
                case 16:
                    strNode   =   strNode+"c2";
                    break;
                case 17:
                    strNode   =   strNode+"d2";
                    break;
                case 18:
                    strNode   =   strNode+"#c2";
                    break;
                case 19:
                    strNode   =   strNode+"e2";
                    break;
                case 20:
                    strNode   =   strNode+"#d2";
                    break;
                case 21:
                    strNode   =   strNode+"f2";
                    break;
                case 22:
                    strNode   =   strNode+"#e2";
                    break;
                case 23:
                    strNode   =   strNode+"g2";
                    break;

                case 24:
                    strNode   =   strNode+"a3";
                    break;
                case 25:
                    strNode   =   strNode+"#a3";
                    break;
                case 26:
                    strNode   =   strNode+"b3";
                    break;
                case 27:
                    strNode   =   strNode+"#b3";
                    break;
                case 28:
                    strNode   =   strNode+"c3";
                    break;
                case 29:
                    strNode   =   strNode+"d3";
                    break;
                case 30:
                    strNode   =   strNode+"#c3";
                    break;
                case 31:
                    strNode   =   strNode+"e3";
                    break;
                case 32:
                    strNode   =   strNode+"#d3";
                    break;
                case 33:
                    strNode   =   strNode+"f3";
                    break;
                case 34:
                    strNode   =   strNode+"#e3";
                    break;
                case 35:
                    strNode   =   strNode+"g3";
                    break;
            }

            bool nodeMatch = false;
            pianoColor->isEmptyCell = false;
            int index = vecpianoColor.getIndex(pianoColor);
            if(GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR){
                pianoSong->vecSoundNode.replace(index,StringMake(strNode));
            }else {
                pianoSong->vecSoundNode.insert((index), StringMake(strNode));
            }
            pianoColor->node = strNode;
            pianoColor->isNoFillColor = false;

            if(nodeMatch){
                log("node match");
//                touchKey->rightKeyAnimation();
                pianoColor->colorType = COLOR_RIGHT;
                vecReplicateNodesClick.pushBack(__Integer::create(1));
            } else{
//                touchKey->wrongKeyAnimation();
                pianoColor->colorType = COLOR_WRONG;
                vecReplicateNodesClick.pushBack(__Integer::create(0));
                log("node not match");
            }
        }else{
            this->addColorBox(touchKey);
        }
    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, touchKey2->btnKey);
    listener->setSwallowTouches(true);
}


void GameScene::addColorBox(KeyButton *keyBtn, int type) {
    UIPianoColor *pianoColor    =   UIPianoColor::create();

//    if(type==0){
//        pianoColor->colorType = COLOR_TYPE::COLOR_WRONG;
//    }else if(type == 1){
//        pianoColor->colorType = COLOR_TYPE::COLOR_RIGHT;
//    }else if(type == 2){
//        pianoColor->colorType = COLOR_TYPE::COLOR_NORMAL;
//    }
    if(keyBtn->keyType == 2){
        pianoColor->r1  =keyBtn->colorPanel->r1;
        pianoColor->g1  =keyBtn->colorPanel->g1;
        pianoColor->b1  =keyBtn->colorPanel->b1;

        pianoColor->r2  =keyBtn->colorPanel->r2;
        pianoColor->g2  =keyBtn->colorPanel->g2;
        pianoColor->b2  =keyBtn->colorPanel->b2;
        pianoColor->imgLeft->setColor(Color3B(keyBtn->colorPanel->r1,keyBtn->colorPanel->g1,keyBtn->colorPanel->b1));
        pianoColor->imgRight->setColor(Color3B(keyBtn->colorPanel->r2,keyBtn->colorPanel->g2,keyBtn->colorPanel->b2));
    }else{
        pianoColor->r  =keyBtn->colorPanel->r;
        pianoColor->g  =keyBtn->colorPanel->g;
        pianoColor->b  =keyBtn->colorPanel->b;
        pianoColor->imgLeft->setColor(Color3B(keyBtn->colorPanel->r,keyBtn->colorPanel->g,keyBtn->colorPanel->b));
        pianoColor->imgRight->setColor(Color3B(keyBtn->colorPanel->r,keyBtn->colorPanel->g,keyBtn->colorPanel->b));
    }
    addColorBoxTouchEvents(pianoColor);
    int pWidth  =   0;
    pWidth  -=  pianoColor->w*1.05;
    int pHeight =   scrollView->getChildByTag(101)->getContentSize().height;

    std::string strNode = "";

    if(keyBtn->keyNumber==0){
        strNode = "a1";
    }

    switch(keyBtn->keyNumber){
        case 0:
            strNode   =   strNode+"a1";
            break;
        case 1:
            strNode   =   strNode+"#a1";
            break;
        case 2:
            strNode   =   strNode+"b1";
            break;
        case 3:
            strNode   =   strNode+"#b1";
            break;
        case 4:
            strNode   =   strNode+"c1";
            break;
        case 5:
            strNode   =   strNode+"d1";
            break;
        case 6:
            strNode   =   strNode+"#c1";
            break;
        case 7:
            strNode   =   strNode+"e1";
            break;
        case 8:
            strNode   =   strNode+"#d1";
            break;
        case 9:
            strNode   =   strNode+"f1";
            break;
        case 10:
            strNode   =   strNode+"#e1";
            break;
        case 11:
            strNode   =   strNode+"g1";
            break;

        case 12:
            strNode   =   strNode+"a2";
            break;
        case 13:
            strNode   =   strNode+"#a2";
            break;
        case 14:
            strNode   =   strNode+"b2";
            break;
        case 15:
            strNode   =   strNode+"#b2";
            break;
        case 16:
            strNode   =   strNode+"c2";
            break;
        case 17:
            strNode   =   strNode+"d2";
            break;
        case 18:
            strNode   =   strNode+"#c2";
            break;
        case 19:
            strNode   =   strNode+"e2";
            break;
        case 20:
            strNode   =   strNode+"#d2";
            break;
        case 21:
            strNode   =   strNode+"f2";
            break;
        case 22:
            strNode   =   strNode+"#e2";
            break;
        case 23:
            strNode   =   strNode+"g2";
            break;

        case 24:
            strNode   =   strNode+"a3";
            break;
        case 25:
            strNode   =   strNode+"#a3";
            break;
        case 26:
            strNode   =   strNode+"b3";
            break;
        case 27:
            strNode   =   strNode+"#b3";
            break;
        case 28:
            strNode   =   strNode+"c3";
            break;
        case 29:
            strNode   =   strNode+"d3";
            break;
        case 30:
            strNode   =   strNode+"#c3";
            break;
        case 31:
            strNode   =   strNode+"e3";
            break;
        case 32:
            strNode   =   strNode+"#d3";
            break;
        case 33:
            strNode   =   strNode+"f3";
            break;
        case 34:
            strNode   =   strNode+"#e3";
            break;
        case 35:
            strNode   =   strNode+"g3";
            break;
    }


    pianoColor->node = strNode;


    log("key node %s",strNode.c_str());

    if(keyBtn->isRestKey){
        pianoColor->isEmptyCell = true;
        pianoColor->node = "00";
    }

    if(startDragColor!=nullptr){
        int index = vecpianoColor.getIndex(startDragColor);
        vecpianoColor.insert((index+1),pianoColor);
        pianoSong->vecSoundNode.insert((index+1),StringMake(strNode));
    }else{
        vecpianoColor.pushBack(pianoColor);
        pianoSong->vecSoundNode.pushBack(StringMake(strNode));
    }

    scrollView->getChildByTag(101)->addChild(pianoColor);



    resetGridItem();

    if(startDragColor!=nullptr){
       startDragColor->startSelectAnimation();
    }

  //
}




void GameScene::playSong() {
    MenuItemImage *btn = static_cast<MenuItemImage*>(keyboardLayer->getChildByTag(110)->getChildByTag(111)->getChildByTag(102));
    log("playSong");
  startDragColor = nullptr;
    stopAllBoxAnim();
    log("playSong2");
    if(isSongPlaying){
        log("playSong3");
        if(isSongPause){
            btn->setSelectedImage(Sprite::create("game screen/pause button.png"));
            btn->setNormalImage(Sprite::create("game screen/pause button.png"));
            isSongPause = false;
        }else{
            isSongPause = true;
            btn->setSelectedImage(Sprite::create("game screen/play button lower.png"));
            btn->setNormalImage(Sprite::create("game screen/play button lower.png"));
        }

    }else{
        isSongPlaying = true;
        btn->setSelectedImage(Sprite::create("game screen/pause button.png"));
        btn->setNormalImage(Sprite::create("game screen/pause button.png"));

//        log("soungNodeCounter value---- %s",musicNodes->vecNode.at(soungNodeCounter)->getCString());
//        log("soungNodeCounter value---- %s",this->vecpianoColor.at(soungNodeCounter)->node.c_str());
        float nodeTime = 0;

        rightNodeCounter = 0;
            for (int i = 0; i < this->vecpianoColor.size(); i++) {
                if(GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG || GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR) {
                nodeTime += vecpianoColor.at(i)->animationTime;
                if (i < musicNodes->vecNode.size()) {

                    log("value---- %s -- %s", musicNodes->vecNode.at(i)->getCString(),
                        this->vecpianoColor.at(i)->node.c_str());



                        __String *strNode = musicNodes->vecNode.at(i);
                        __Array *arrNode = strNode->componentsSeparatedByString("#");

                        log("compair string %d",arrNode->count());

                     __String *node = ((__String*)arrNode->getObjectAtIndex(0));


                    int noteLangth = Value(arrNode->getObjectAtIndex(1)).asInt();
                    noteLangth++; // beacuse notelangth 0 set default

                    log("note langth %d -- %d",noteLangth,this->vecpianoColor.at(i)->noteLength);
                    log("note Name %s -- %s",node->getCString(),this->vecpianoColor.at(i)->node.c_str());



                    if ((node->getCString() ==
                        this->vecpianoColor.at(i)->node) && (this->vecpianoColor.at(i)->noteLength==noteLangth)) {

                        float t = musicNodes->vecTime.at(i)->getValue();

                        log("animation time %f ---- %f", nodeTime, t);
//                        if (nodeTime <= t &&
//                            t >= nodeTime - this->vecpianoColor.at(i)->animationTime) {
                            this->vecpianoColor.at(i)->colorType = COLOR_RIGHT;
                            rightNodeCounter++;
//                        } else {
//                            this->vecpianoColor.at(i)->colorType = COLOR_WRONG;
//                        }


                    } else {
                        log("value wrong right ");
                        this->vecpianoColor.at(i)->colorType = COLOR_WRONG;
                        this->vecpianoColor.at(i)->requireLength = noteLangth-this->vecpianoColor.at(i)->noteLength;

                        this->vecpianoColor.at(i)->requireNotePosition = getRequireNotePosition(node->getCString(),this->vecpianoColor.at(i)->node);


                    }
                } else if (GameData::getInstance()->game_type == GAME_TYPE_REPLICATE_SONG) {

                    log("value else wrong ");
                    this->vecpianoColor.at(i)->colorType = COLOR_WRONG;
                    this->vecpianoColor.at(i)->requireLength = -100;
                }
            }
                else{
                    this->vecpianoColor.at(i)->colorType = COLOR_NORMAL;
                }

            }
        schedule(schedule_selector(GameScene::playUpdate),0.35);
    }
}

void GameScene::playUpdate(float dt) {
    if(soungNodeCounter==vecpianoColor.size()){
        unschedule(schedule_selector(GameScene::playUpdate));
        stopCall();
        return;
    }
    if(!isSongPause) {
        log("soungNodeCounter value %d",soungNodeCounter);
        log("vecpianoColor size %d",this->vecpianoColor.size());
        log("vecSoundNode size %d", pianoSong->vecSoundNode.size());
        std::string strNode = pianoSong->vecSoundNode.at(soungNodeCounter)->_string;
        if(!this->vecpianoColor.at(soungNodeCounter)->isEmptyCell) {
            SoundController::playSoundNode(strNode);
        }
        isSongPause = true;

        this->vecpianoColor.at(soungNodeCounter)->startAnimation();
        soungNodeCounter++;
    }
}

void GameScene::backClick() {
    SoundController::stopAllEffectSound();
    Director::getInstance()->replaceScene(Home::createScene());
}

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event){
    if(keyCode== EventKeyboard::KeyCode::KEY_BACK){
        backClick();
    }
}






void GameScene::addColorBoxTouchEvents(UIPianoColor *pianoColor){
    auto listener = EventListenerTouchOneByOne::create();

    listener->onTouchBegan= [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {


        if(GameData::getInstance()->game_type==GAME_TYPE_FILL_IN_COLOR){
            if (startDragColor != nullptr) {
               UIPianoColor *color =  static_cast<UIPianoColor*>(event->getCurrentTarget());
                if(color->node == "00"){
                    return  false;
                }
            }
        }



        Vec2         p              =       touch->getLocation();
       Layer *touchLayer  =       static_cast<Layer*>(event->getCurrentTarget());
        Vec2         localPoint     =       touchLayer->getParent()->convertToNodeSpace(p);
        cocos2d::Rect rect          =       touchLayer->getBoundingBox();

        if(!rect.containsPoint(localPoint))
        {
            return false; // we did not consume this event, pass thru.
        }

        log("touch begin start");

        UIPianoColor *color = static_cast<UIPianoColor*>(event->getCurrentTarget());
        log("touch begin start1 size %d ",vecpianoColor.size());
        log("touch pos %f ",color->getPositionX());
        int index = vecpianoColor.getIndex(color);
//        log("touch begin start2 %d index %d music size %d painting size ",index, musicNodes->vecNode.size(),vecpianoColor.size());
        __String *node = pianoSong->vecSoundNode.at(index);
        log("touch begin start3");


        if(node->isEqual(__String::create("00"))) {
            return false;
        }
        log("touch begin start4");


        startDragColor =  static_cast<UIPianoColor*>(event->getCurrentTarget());
        log("touch begin start5");

        if(!isSongPlaying) {
            stopAllBoxAnim();
            log("touch begin start6");
            startDragColor->startSelectAnimation();
            log("touch begin start7");
        }



        log("touch begin end");

//        log("on touch call3 -- %d--%d--%d",touchKey->keyType,touchKey->keyPosition,touchKey->keyOctal);


        return true;// to indicate that we have consumed it.
    };


    listener->onTouchMoved= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        Vec2         p              =       touch->getLocation();

        int startDragIndex = vecpianoColor.getIndex(startDragColor);

        log("touch move start");

        if(GameData::getInstance()->game_type == GAME_TYPE_FILL_IN_COLOR){
            return;;
        }

        int n = startDragIndex+1;
        if(n<vecpianoColor.size()) {
            UIPianoColor *DColor = vecpianoColor.at(startDragIndex + 1);

            Vec2 startPoint = touch->getStartLocation();


            Vec2 localPoint = DColor->getParent()->convertToNodeSpace(p);
            cocos2d::Rect rect = DColor->getBoundingBox();


            if (!rect.containsPoint(localPoint)) {
                if (startDragIndex - 1 >= 0) {
                DColor = vecpianoColor.at(startDragIndex - 1);
                startPoint = touch->getStartLocation();


                localPoint = DColor->getParent()->convertToNodeSpace(p);
                rect = DColor->getBoundingBox();
            }
            }



            log("touch move start 1");

        if(rect.containsPoint(localPoint))
        { log("touch inside");
            if(!isSongPlaying) {

                int currentIndex = vecpianoColor.getIndex(DColor);

//                if ((currentIndex + 1) == startDragIndex || (currentIndex - 1) == startDragIndex) {
                    pianoSong->vecSoundNode.replace(currentIndex,
                                                    pianoSong->vecSoundNode.at(startDragIndex));


                DColor->isEmptyCell = startDragColor->isEmptyCell;
                    DColor->r = startDragColor->r;
                    DColor->g = startDragColor->g;
                    DColor->b = startDragColor->b;

                    DColor->r1 = startDragColor->r1;
                    DColor->g1 = startDragColor->g1;
                    DColor->b1 = startDragColor->b1;

                    DColor->r2 = startDragColor->r2;
                    DColor->g2 = startDragColor->g2;
                    DColor->b2 = startDragColor->b2;


                   KeyButton *keyBtn =  keyboardLayer->vecKeyButton.at(getKeyBtnIndex(pianoSong->vecSoundNode.at(startDragIndex)->getCString()));


                if(DColor->isEmptyCell){
//                    DColor->leftLayer->setColor(Color3B::WHITE);
//                    DColor->rightLayer->setColor(Color3B::WHITE);
//                    DColor->imgCenter->setColor(Color3B::WHITE);
                    ColorPanel*colorPanel = new ColorPanel();

                    std::string keyCode = KEY_COLOR_CODE_REST;
                    int color = UserDefault::getInstance()->getIntegerForKey(keyCode.c_str());
                    std::string colorId = colorPanel->arrRestColor[color];


                    log(" color id %s",colorId.c_str());
                    String *strColorId = String::create(colorId);

                    __Array *arrColor =  strColorId->componentsSeparatedByString(",");



                    colorPanel->r = atoi(((__String*)arrColor->getObjectAtIndex(0))->_string.c_str());
                    colorPanel->g = atoi(((__String*)arrColor->getObjectAtIndex(1))->_string.c_str());
                    colorPanel->b = atoi(((__String*)arrColor->getObjectAtIndex(2))->_string.c_str());
                    log("r value %d g%d b%d",colorPanel->r,colorPanel->g,colorPanel->b);




                    DColor->imgLeft->setColor(Color3B(colorPanel->r,colorPanel->g,colorPanel->b));
                    DColor->imgRight->setColor(Color3B(colorPanel->r,colorPanel->g,colorPanel->b));

                    DColor->isEmptyCell = true;
                }else if(keyBtn->keyType == 2){
                        DColor->imgLeft->setColor(Color3B(keyBtn->colorPanel->r1,keyBtn->colorPanel->g1,keyBtn->colorPanel->b1));
                        DColor->imgRight->setColor(Color3B(keyBtn->colorPanel->r2,keyBtn->colorPanel->g2,keyBtn->colorPanel->b2));


                    }else{
                        DColor->imgLeft->setColor(Color3B(keyBtn->colorPanel->r,keyBtn->colorPanel->g,keyBtn->colorPanel->b));
                        DColor->imgRight->setColor(Color3B(keyBtn->colorPanel->r,keyBtn->colorPanel->g,keyBtn->colorPanel->b));
                    }




//                startDragColor->w = startDragColor->w*2;
//
//                startDragColor->leftLayer->setScaleX(2);
//
//
//                startDragColor->rightLayer->setScaleX(2);
//                startDragColor->leftLayer->setPositionX(0);
//                startDragColor->rightLayer->setPositionX(startDragColor->leftLayer->getContentSize().width);
//

                log("touch move start 2");

                    DColor->boxNumber = startDragColor->boxNumber;

                DColor->setMargeCell(startDragColor->noteLength);
                log("touch move start 3");
                eraseCall();
                startDragColor = DColor;
                resetGridItem();
                log("touch move start 4");
                stopAllBoxAnim();
                DColor->startSelectAnimation();


                log("touch move start 5");
//                }
            }
        }

        }

    };
    listener->onTouchEnded= [=](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        UIPianoColor *touchLayer  =       static_cast<UIPianoColor*>(event->getCurrentTarget());


        log("touch end start");
//        startDragColor = nullptr;

        if(isSongPlaying){
            for (int i = 0; i < vecpianoColor.size(); ++i) {
                vecpianoColor.at(i)->resetAnimation();
            }
            soungNodeCounter = vecpianoColor.getIndex(touchLayer);
            log(" soungNodeCounter count-- %d",soungNodeCounter);
        }




    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, pianoColor);
    listener->setSwallowTouches(false);
}




int GameScene::getKeyBtnIndex(std::string strNode) {
    int counter = 0;


    if (strNode == "a1") {
        counter = 0;
    } else if (strNode == "#a1") {
        counter = 1;
    } else if (strNode == "b1") {
        counter = 2;
    } else if (strNode == "#b1") {
        counter = 3;
    } else if (strNode == "c1") {
        counter = 4;
    } else if (strNode == "d1") {
        counter = 5;
    } else if (strNode == "#c1") {
        counter = 6;
    } else if (strNode == "e1") {
        counter = 7;
    } else if (strNode == "#d1") {
        counter = 8;
    } else if (strNode == "f1") {
        counter = 9;
    } else if (strNode == "#e1") {
        counter = 10;
    } else if (strNode == "g1") {
        counter = 11;
    }
   else if (strNode == "a2") {
        counter = 12;
    } else if (strNode == "#a2") {
        counter = 13;
    } else if (strNode == "b2") {
        counter = 14;
    } else if (strNode == "#b2") {
        counter = 15;
    } else if (strNode == "c2") {
        counter = 16;
    } else if (strNode == "d2") {
        counter = 17;
    } else if (strNode == "#c2") {
        counter = 18;
    } else if (strNode == "e2") {
        counter = 19;
    } else if (strNode == "#d2") {
        counter = 20;
    } else if (strNode == "f2") {
        counter = 21;
    } else if (strNode == "#e2") {
        counter = 22;
    } else if (strNode == "g2") {
        counter = 23;
    }
   else  if (strNode == "a3") {
        counter = 24;
    } else if (strNode == "#a3") {
        counter = 25;
    } else if (strNode == "b3") {
        counter = 26;
    } else if (strNode == "#b3") {
        counter = 27;
    } else if (strNode == "c3") {
        counter = 28;
    } else if (strNode == "d3") {
        counter = 29;
    } else if (strNode == "#c3") {
        counter = 30;
    } else if (strNode == "e3") {
        counter = 31;
    } else if (strNode == "#d3") {
        counter = 32;
    } else if (strNode == "f3") {
        counter = 33;
    } else if (strNode == "#e3") {
        counter = 34;
    } else if (strNode == "g3") {
        counter = 35;
    }









    return counter;
}


void GameScene::stopAllBoxAnim() {

    for (int i = 0; i < vecpianoColor.size(); i++) {
        vecpianoColor.at(i)->stopSelectAnimation();
    }

}


void GameScene::time_update(float dt) {

    if(isBackGroundPlay){
//
        musicTimer += dt;
//        log("time update call inside %f",musicTimer);
    }

}

void GameScene::showMessageDialog(std::string title, std::string msg) {


    TouchableLayer *touchLayer = TouchableLayer::createLayer(Color4B(0,0,0,150));
    Sprite *imgPopup = Sprite::create("store/page_21_BG.png");
    imgPopup->setPosition(Vec2(touchLayer->getContentSize().width/2,touchLayer->getContentSize().height/2));
    touchLayer->addChild(imgPopup);


    Label *lblTitle = Label::createWithTTF(title,"fonts/arial.ttf",45);
    lblTitle->setPosition(Vec2(imgPopup->getContentSize().width/2,imgPopup->getContentSize().height*0.8));
    imgPopup->addChild(lblTitle);
    lblTitle->setColor(Color3B::BLACK);



    Label *lblMsg = Label::createWithTTF(msg,"fonts/arial.ttf",45,Size(imgPopup->getContentSize().width*0.7,imgPopup->getContentSize().height*0.4),TextHAlignment::CENTER,TextVAlignment::CENTER);
    lblMsg->setPosition(Vec2(imgPopup->getContentSize().width/2,imgPopup->getContentSize().height*0.6));
    imgPopup->addChild(lblMsg);

    lblMsg->setColor(Color3B::BLACK);
    MenuItemImage *btnOk    =   MenuItemImage::create("popup/yes.png","popup/yes.png",CC_CALLBACK_0(Node::removeFromParent,touchLayer));
    btnOk->setPosition(Vec2(imgPopup->getContentSize().width/2,imgPopup->getContentSize().height*0.3));

    Label *lblOk = Label::createWithTTF("OK","fonts/arial.ttf",30);
    lblOk->setPosition(Vec2(btnOk->getContentSize().width/2,btnOk->getContentSize().height*0.5));
    btnOk->addChild(lblOk);



    Menu *menu = Menu::create(btnOk,NULL);
    menu->setPosition(Vec2::ZERO);
    menu->addChild(btnOk);
    imgPopup->addChild(menu);

    this->addChild(touchLayer);


}


void GameScene::restartLevel() {
    Director::getInstance()->replaceScene(GameScene::createScene());
}


void GameScene::resetGridColor() {
    for (int i = 0; i < vecpianoColor.size(); i++) {
        UIPianoColor *color = vecpianoColor.at(i);

        log("no fill call %d -- %d ",color->isNoFillColor, color->isEmptyCell);
        if(color->isNoFillColor || color->isEmptyCell){


        }else {
            for (int j = 0; j < keyboardLayer->vecKeyButton.size(); j++) {
                KeyButton *key = keyboardLayer->vecKeyButton.at(j);

                log("color note %s",color->node.c_str());
                int index = getKeyBtnIndex(color->node);
                if (key->keyNumber == index) {
                    color->r = key->colorPanel->r;
                    color->g = key->colorPanel->g;
                    color->b = key->colorPanel->b;

                    color->r1 = key->colorPanel->r1;
                    color->g1 = key->colorPanel->g1;
                    color->b1 = key->colorPanel->b1;

                    color->r2 = key->colorPanel->r2;
                    color->g2 = key->colorPanel->g2;
                    color->b2 = key->colorPanel->b2;

                    if (key->keyType == 2) {
                        color->imgLeft->setColor(Color3B(key->colorPanel->r1, key->colorPanel->g1,
                                                         key->colorPanel->b1));
                        color->imgRight->setColor(Color3B(key->colorPanel->r2, key->colorPanel->g2,
                                                          key->colorPanel->b2));


                    } else {
                        color->imgLeft->setColor(Color3B(key->colorPanel->r, key->colorPanel->g,
                                                         key->colorPanel->b));
                        color->imgRight->setColor(Color3B(key->colorPanel->r, key->colorPanel->g,
                                                          key->colorPanel->b));
                    }
                }
            }
        }
    }

}


int GameScene::getRequireNotePosition(std::string requireNote, std::string currentNote) {

    int requireIndex = getKeyBtnIndex(requireNote);
    int currentIndex = getKeyBtnIndex(currentNote);

    return  requireIndex-currentIndex;


}